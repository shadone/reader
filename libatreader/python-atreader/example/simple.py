#!/usr/bin/python
#
# Simple example that imports atreader module and creates
# GtkReaderArea widget with some text in it.
#
# Denis Dzyubenko <shad@novoross.ru> 2004

import pygtk
pygtk.require('2.0')
import gtk
from gtk import TRUE, FALSE
import gobject
import pango
import atreader

class MainWindow(gtk.Window):
	def __init__(self):
		gtk.Window.__init__(self)
		self.connect('destroy', self.quit)
		self.set_default_size(600, 400)

		vbox = gtk.VBox(FALSE, 2)
		vbox.set_border_width(5)
		self.add(vbox)

		hbox = gtk.HBox(FALSE, 2)
		vbox.pack_start(hbox)

		## creating default text
		## tip: don't forget to set buffer name
		buffer = atreader.ReaderBuffer("Main")

		## first paragraph with default font
		p = atreader.ReaderParagraph()
		p.append_text("FictionBook is based on the XML standard from W3C, and FictionBook documents are well-formed XML documents, conforming to the FictionBook schema. Since the adoption of hyperlinks in FictionBook version 2.0, XML namespaces are now mandatory for all FictionBook documents. The main FictionBook namespace is ?http://www.gribuser.ru/xml/fictionbook/2.0?, the version number in the namespace URL will change as new versions of the standard are developed. The other required namespace is ?http://www.w3.org/1999/xlink? for XLink. It should be included if any hyperlink elements are used in the document.")
		p.set_indent(atreader.VALUE_PIXELS, 50)
		p.set_align(pango.ALIGN_LEFT)
		p.set_justify(TRUE)
		buffer.append_paragraph(p)

		## second paragraph
		p = atreader.ReaderParagraph()
		p.append_text("Mike Matsnev 6th august 2002")
		p.set_align(pango.ALIGN_RIGHT)
		p.set_justify(FALSE)
		attr = atreader.ReaderAttr("small italic text")
		attr.set_style(pango.STYLE_ITALIC)
		attr.set_size(9)
		## or
		#attr.set_font_family("Arial 9")
		chunk = atreader.ReaderChunk()
		## note that for now, indexes are *byte* offsets, not character.
		## This will be changed in the future.
		chunk.set_position(0, len(p.get_text()))
		chunk.set_text_value(attr)
		p.add_chunk(chunk)
		buffer.append_paragraph(p)

		## third paragraph - empty line
		p = atreader.ReaderParagraph()
		p.set_text("")
		buffer.append_paragraph(p)

		# fourth paragraph
		p = atreader.ReaderParagraph()
		p.append_text("The overall document structure is straightforward and resembles that of HTML. However, the main goal of this work is to design an XML based document that focuses on the document?s logical structure, and not on the presentation features. An essential characteristic of structured markup is that it explicitly distinguishes the structure and semantic content of a document. It does not mark up the way in which the document will appear to the reader, in print or otherwise. In this specification we deliberately didn?t include any direct means for specifying presentation features. The only way to influence the rendering process is by specifying styles for paragraphs and strings of characters. Instead some unique elements are provided to separate logical parts of book, like poems, citations, and epigraphs.")
		p.set_indent(atreader.VALUE_PERCENTS, 10)
		p.set_align(pango.ALIGN_LEFT)
		p.set_justify(TRUE)
		buffer.append_paragraph(p)
		
		self.reader = atreader.ReaderArea()
		
		## default font family and size
		self.reader.set_default_font_from_string("Arial 14")
		## height between lines
		self.reader.set_spacing(3)
		## column count
		self.reader.set_column_count(1)
		## width between columns
		self.reader.set_column_spacing(10)
		## whether to display partial lines (i.e. lines that do not
		## fully fit on the screen)
		self.reader.set_display_partial(TRUE)
		## default height of empty line (pixels)
		self.reader.set_empty_line_height(20)
		## text wrapping
		self.reader.set_wrap(pango.WRAP_WORD_CHAR)
		
		self.reader.add_buffer(buffer)
		self.reader.set_current_buffer_by_name(buffer.get_name())

		hbox.pack_start(self.reader, expand=TRUE)
		
		vscrollbar = gtk.VScrollbar()
		self.reader.set_adjustment(vscrollbar.get_adjustment())
		hbox.pack_start(vscrollbar, expand=FALSE)

		self.quit_btn = gtk.Button(label="Quit")
		self.quit_btn.connect('clicked', self.quit)
		vbox.pack_start(self.quit_btn, expand=FALSE)
		
	def quit(self, *args):
		gtk.main_quit()


mainwnd = MainWindow()
mainwnd.show_all()

gtk.main()


# Local Variables: ***
# mode: python ***
# tab-width: 4 ***
# End: ***
