#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

/* include this first, before NO_IMPORT_PYGOBJECT is defined */
#include <pygobject.h>

void pyatreader_register_classes (PyObject *d);
void pyatreader_add_constants(PyObject *module, const gchar *strip_prefix);

extern PyMethodDef pyatreader_functions[];

DL_EXPORT(void) initatreader(void)
{
  PyObject *m, *d;
  
  init_pygobject ();
  
  m = Py_InitModule ("atreader", pyatreader_functions);
  d = PyModule_GetDict (m);
  
  pyatreader_register_classes (d);
  pyatreader_add_constants(m, "GTK_READER_PARAGRAPH_");
}

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
