#include "gtkreaderattr.h"

/* Forward declarations */

static void gtk_reader_attr_class_init               (GtkReaderAttrClass *klass);
static void gtk_reader_attr_init                     (GtkReaderAttr      *attr);
static void gtk_reader_attr_finalize                 (GObject *object);

/* Local data */

static gpointer parent_class = NULL;

GType gtk_reader_attr_get_type()
{
  static GType attr_type = 0;
  
  if (!attr_type)
  {
    static const GTypeInfo attr_info =
      {
        sizeof (GtkReaderAttrClass),
        NULL,
        NULL,
        (GClassInitFunc)gtk_reader_attr_class_init,
        NULL,
        NULL,
        sizeof (GtkReaderAttr),
        0,
        (GInstanceInitFunc)gtk_reader_attr_init,
      };
    
    attr_type = g_type_register_static(G_TYPE_OBJECT, "GtkReaderAttr", &attr_info, 0);
  }
  
  return attr_type;
}

static void gtk_reader_attr_class_init(GtkReaderAttrClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));

  gobject_class->finalize = gtk_reader_attr_finalize;
}

static void gtk_reader_attr_init(GtkReaderAttr *attr)
{
  attr->name                  = NULL;
  attr->is_relative_font_size = FALSE;
  attr->font_size.absolute    = 0;
  attr->font_family           = NULL;
  attr->style                 = PANGO_STYLE_NORMAL;
  attr->weight                = PANGO_WEIGHT_NORMAL;
}

/**
 * Free memory allocated to #GtkReaderAttr when ref count == 0.
 * @param object a #GtkReaderAttr.
 */
static void gtk_reader_attr_finalize(GObject *object)
{
  GtkReaderAttr *attr;

  attr = GTK_READER_ATTR(object);
  g_assert(attr != NULL);

  if (attr->name)
    g_free(attr->name);

  if (attr->font_family)
    g_free(attr->font_family);
  
  G_OBJECT_CLASS(parent_class)->finalize(object);
}

/**
 * Creates a new #GtkReaderAttr.
 * @param name a name to give to the attribute.
 * @return a  #GtkReaderAttr.
 */
GtkReaderAttr* gtk_reader_attr_new(const gchar *name)
{
  GtkReaderAttr *attr;

  attr = g_object_new(gtk_reader_attr_get_type(), NULL);
  g_assert(attr != NULL);
  
  attr->name = g_strdup(name);
    
  return attr;
}

/**
 * Checks if any of attribute properties is set.
 * @param attr a #GtkReaderAttr.
 * @return TRUE if any attribute is set, FALSE otherwise.
 */
gboolean gtk_reader_attr_is_set(GtkReaderAttr *attr)
{
  g_return_val_if_fail(attr != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_READER_ATTR(attr), FALSE);

  return (attr->type != 0);
}

/**
 * Set font size attribute to specified size.
 * @param attr a #GtkReaderAttr.
 * @param size a absolute size of font.
 */
void gtk_reader_attr_set_size(GtkReaderAttr *attr,
                              guint          size)
{
  g_return_if_fail(attr != NULL);
  g_return_if_fail(GTK_IS_READER_ATTR(attr));

  attr->type |= ATTR_FONT_SIZE;
  
  attr->is_relative_font_size = FALSE;
  attr->font_size.absolute = size;
}

/**
 * Sets font size relative to basic font of paragraph.
 * @param attr a #GtkReaderAttr.
 * @param size a relative font size.
 */
void gtk_reader_attr_set_size_relative(GtkReaderAttr *attr,
                                       gint           size)
{
  g_return_if_fail(attr != NULL);
  g_return_if_fail(GTK_IS_READER_ATTR(attr));

  attr->type |= ATTR_FONT_SIZE;
  
  attr->is_relative_font_size = TRUE;
  attr->font_size.relative    = size;
}

void gtk_reader_attr_set_font_family(GtkReaderAttr *attr,
                                     const gchar   *family)
{ 
  g_return_if_fail(attr != NULL);
  g_return_if_fail(GTK_IS_READER_ATTR(attr));

  attr->type |= ATTR_FONT_FAMILY;

  if (attr->font_family)
    g_free(attr->font_family);

  attr->font_family = g_strdup(family);
}


void gtk_reader_attr_set_style(GtkReaderAttr *attr,
                               PangoStyle     style)
{
  g_return_if_fail(attr != NULL);
  g_return_if_fail(GTK_IS_READER_ATTR(attr));

  attr->type |= ATTR_STYLE;

  attr->style = style;
}

void gtk_reader_attr_set_weight(GtkReaderAttr *attr,
                                PangoWeight    weight)
{
  g_return_if_fail(attr != NULL);
  g_return_if_fail(GTK_IS_READER_ATTR(attr));

  attr->type |= ATTR_WEIGHT;

  attr->weight = weight;
}


/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
