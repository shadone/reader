#include <string.h> /*memset*/
#include "gtkreaderchunk.h"

const gunichar SYMBOL_IMAGE = 0xE000;

/* Forward declarations */

static void gtk_reader_chunk_class_init               (GtkReaderChunkClass *klass);
static void gtk_reader_chunk_init                     (GtkReaderChunk      *reader_chunk);
static void gtk_reader_chunk_finalize                 (GObject *object);
void        gtk_reader_chunk_free_value               (GtkReaderChunk *chunk);

/* Local data */

static gpointer parent_class = NULL;

GType gtk_reader_chunk_get_type()
{
  static GType reader_chunk_type = 0;
  
  if (!reader_chunk_type)
  {
    static const GTypeInfo reader_chunk_info =
      {
        sizeof (GtkReaderChunkClass),
        NULL,
        NULL,
        (GClassInitFunc) gtk_reader_chunk_class_init,
        NULL,
        NULL,
        sizeof (GtkReaderChunk),
        0,
        (GInstanceInitFunc) gtk_reader_chunk_init,
      };
    
    reader_chunk_type = g_type_register_static(G_TYPE_OBJECT, "GtkReaderChunk", &reader_chunk_info, 0);
  }
  
  return reader_chunk_type;
}

static void gtk_reader_chunk_class_init(GtkReaderChunkClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));

  gobject_class->finalize = gtk_reader_chunk_finalize;
}

static void gtk_reader_chunk_init(GtkReaderChunk *chunk)
{
  chunk->chunkid = 0x0;
  chunk->type = GTK_READER_CHUNK_IMAGE;
#ifdef HAVE_MEMSET
  memset(&(chunk->value), 0, sizeof(chunk->value));
#endif
  chunk->start_index = 0;
  chunk->end_index = 0;
}

GtkReaderChunk* gtk_reader_chunk_new()
{
  GtkReaderChunk *chunk;

  chunk = g_object_new(gtk_reader_chunk_get_type(), NULL);

  return chunk;
}

/**
 * Destroys a chunk when ref count == 0.
 * @param object a #GtkReaderChunk.
 */
static void gtk_reader_chunk_finalize(GObject *object)
{
  GtkReaderChunk *chunk;

  chunk = GTK_READER_CHUNK(object);
  g_assert(chunk != NULL);

  gtk_reader_chunk_free_value(chunk);
  
  G_OBJECT_CLASS(parent_class)->finalize(object);
}

/**
 * Analyzes chunk type and frees memory allocated for chunk.
 * @param chunk a #GtkReaderChunk.
 */
void gtk_reader_chunk_free_value(GtkReaderChunk *chunk)
{
  if (chunk->type == GTK_READER_CHUNK_TEXT)
  {
    if (chunk->value.textattr)
    {
      g_object_unref(G_OBJECT(chunk->value.textattr));
      chunk->value.textattr = NULL;
    }
  }
  if (chunk->type == GTK_READER_CHUNK_IMAGE)
  {
    //TODO:
  }
  else if (chunk->type == GTK_READER_CHUNK_TABLE)
  {
    //TODO:
  }
}

/**
 * Sets position of the chunk.
 * Note that positions are *byte* offsets, but it will be changed
 * sometime.
 * @param chunk a #GtkReaderChunk.
 * @param start a start index of the chunk.
 * @param end a end index of the chunk.
 * @todo replace byte indexes with character indexes.
 */
void gtk_reader_chunk_set_position(GtkReaderChunk *chunk,
                                   guint           start,
                                   guint           end)
{
  g_return_if_fail(chunk != NULL);
  g_return_if_fail(GTK_IS_READER_CHUNK(chunk));

  if (start == end)
  {
    g_warning("Start index should not equal end index.\n");
    return;
  }
  if (start > end)
  {
    g_warning("Start index should be less that end index.\n");
    return;
  }

  chunk->start_index = start;
  chunk->end_index   = end;
}

void gtk_reader_chunk_set_text_value(GtkReaderChunk *chunk,
                                     GtkReaderAttr  *attr)
{
  g_return_if_fail(chunk != NULL);
  g_return_if_fail(GTK_IS_READER_CHUNK(chunk));
  g_return_if_fail(attr != NULL);

  gtk_reader_chunk_free_value(chunk);

  g_object_ref(G_OBJECT(attr));
  
  chunk->type = GTK_READER_CHUNK_TEXT;
  chunk->value.textattr = attr;
}


void gtk_reader_chunk_set_image_value(GtkReaderChunk *chunk,
                                      GtkReaderChunkImage Image)
{
  g_return_if_fail(chunk != NULL);
  g_return_if_fail(GTK_IS_READER_CHUNK(chunk));

  gtk_reader_chunk_free_value(chunk);

  chunk->type = GTK_READER_CHUNK_IMAGE;
  chunk->value.image = Image;
  //TODO:
}

void gtk_reader_chunk_set_table_value(GtkReaderChunk *chunk,
                                      GtkReaderChunkTable Table)
{
  g_return_if_fail(chunk != NULL);
  g_return_if_fail(GTK_IS_READER_CHUNK(chunk));

  gtk_reader_chunk_free_value(chunk);

  chunk->type = GTK_READER_CHUNK_TABLE;
  chunk->value.table = Table;
  //TODO:
}


/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
