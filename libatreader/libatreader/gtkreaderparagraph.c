#include "gtkreaderparagraph.h"
#include "gtkreaderattr.h"

#define CHUNKID_START 0xE000
#define CHUNKID_END   0xF8FF

/* Forward declarations */

static void gtk_reader_paragraph_class_init               (GtkReaderParagraphClass *klass);
static void gtk_reader_paragraph_init                     (GtkReaderParagraph      *reader_paragraph);
static void gtk_reader_paragraph_finalize                 (GObject *object);

/* Local data */

static gpointer parent_class = NULL;

GType gtk_reader_paragraph_get_type()
{
  static GType reader_paragraph_type = 0;
  
  if (!reader_paragraph_type)
  {
    static const GTypeInfo reader_paragraph_info =
      {
        sizeof (GtkReaderParagraphClass),
        NULL,
        NULL,
        (GClassInitFunc) gtk_reader_paragraph_class_init,
        NULL,
        NULL,
        sizeof (GtkReaderParagraph),
        0,
        (GInstanceInitFunc) gtk_reader_paragraph_init,
      };
    
    reader_paragraph_type = g_type_register_static(G_TYPE_OBJECT, "GtkReaderParagraph", &reader_paragraph_info, 0);
  }
  
  return reader_paragraph_type;
}

static void gtk_reader_paragraph_class_init(GtkReaderParagraphClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));

  gobject_class->finalize = gtk_reader_paragraph_finalize;
}

/**
 * Destroys a paragraph when ref count == 0.
 * @param object a #GtkReaderParagraph.
 */
static void gtk_reader_paragraph_finalize(GObject *object)
{
  GtkReaderParagraph *paragraph;
  GSList *it;

  paragraph = GTK_READER_PARAGRAPH(object);
  g_assert(paragraph != NULL);

  if (paragraph->text)
    g_string_free(paragraph->text, TRUE);

  for (it = paragraph->chunks; it != NULL; it = g_slist_next(it))
  {
    GtkReaderChunk *chunk;
    
    g_assert(it != NULL && it->data != NULL);
    chunk = GTK_READER_CHUNK(it->data);
    g_object_unref(G_OBJECT(chunk));
  }
  g_slist_free(paragraph->chunks);
  
  G_OBJECT_CLASS(parent_class)->finalize(object);
}

static void gtk_reader_paragraph_init(GtkReaderParagraph *paragraph)
{
  paragraph->indent.type   = GTK_READER_PARAGRAPH_VALUE_PIXELS;
  paragraph->indent.value  = 0;
  paragraph->align   = PANGO_ALIGN_LEFT;
  paragraph->justify = FALSE;
  paragraph->text    = NULL;
  paragraph->length  = 0;
  paragraph->chunks  = NULL;
  paragraph->max_chunkid = CHUNKID_START;
}

/**
 * Creates new #GtkReaderParagraph.
 * @return a #GtkReaderParagraph.
 */
GtkReaderParagraph* gtk_reader_paragraph_new()
{
  GtkReaderParagraph *reader_paragraph;

  reader_paragraph = g_object_new(gtk_reader_paragraph_get_type(), NULL);

  return reader_paragraph;
}

/**
 * Sets text of paragraph.
 * @param paragraph a #GtkReaderParagraph.
 * @param text a text to insert into paragraph.
 */
void gtk_reader_paragraph_set_text(GtkReaderParagraph *paragraph,
                                   const gchar        *text)
{
  g_return_if_fail(paragraph != NULL);
  g_return_if_fail(GTK_IS_READER_PARAGRAPH(paragraph));

  if (paragraph->text)
  {
    g_string_free(paragraph->text, TRUE);
    paragraph->text = NULL;
  }

  gtk_reader_paragraph_append_text(paragraph, text);  
}

/**
 * Returns text stored in paragraph.
 * @param paragraph a #GtkReaderParagraph.
 * @return stored text.
 */
const gchar* gtk_reader_paragraph_get_text(GtkReaderParagraph *paragraph)
{
  g_return_val_if_fail(paragraph != NULL, NULL);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), NULL);

  g_return_val_if_fail(paragraph->text != NULL, NULL);

  return paragraph->text->str;
}

/**
 * Returns text stored in paragraph as GString.
 * @param paragraph a #GtkReaderParagraph.
 * @return stored text. Returned value should not be freed.
 */
GString* gtk_reader_paragraph_get_string(GtkReaderParagraph *paragraph)
{
  g_return_val_if_fail(paragraph != NULL, NULL);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), NULL);

  return paragraph->text;
}


/**
 * Appends text to the end of paragraph.
 * @param paragraph a #GtkReaderParagraph.
 * @param text a text to append.
 */
void gtk_reader_paragraph_append_text(GtkReaderParagraph *paragraph,
                                      const gchar        *text)
{
  g_return_if_fail(paragraph != NULL);
  g_return_if_fail(GTK_IS_READER_PARAGRAPH(paragraph));

  if (paragraph->text)
    paragraph->text = g_string_append(paragraph->text, text);
  else
    paragraph->text = g_string_new(text);
  g_assert(paragraph->text != NULL);
  
  paragraph->length = g_utf8_strlen(paragraph->text->str, -1);
}

/**
 * Set horizontal offset of first line of paragraph.
 * @param paragraph a #GtkReaderParagraph.
 * @param type a type of measure - percents of pixels.
 * @param value an indent.
 */
void gtk_reader_paragraph_set_indent(GtkReaderParagraph *paragraph,
                                     GtkReaderParagraphValue type,
                                     guint value)
{
  g_return_if_fail(paragraph != NULL);
  g_return_if_fail(GTK_IS_READER_PARAGRAPH(paragraph));

  paragraph->indent.type  = type;
  paragraph->indent.value = value;
}

/**
 * Returns the type of indent value - percents or pixels.
 * @param paragraph a #GtkReaderParagraph.
 * @return a type of measure.
 */
GtkReaderParagraphValue gtk_reader_paragraph_get_indent_type(GtkReaderParagraph *paragraph)
{
  g_return_val_if_fail(paragraph != NULL, GTK_READER_PARAGRAPH_VALUE_PIXELS);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), GTK_READER_PARAGRAPH_VALUE_PIXELS);

  return paragraph->indent.type;
}

/**
 * Returns horizontal indent of first line of paragraph.
 * @param paragraph a #GtkReaderParagraph.
 * @return an indent in percents or pixels.
 */
guint gtk_reader_paragraph_get_indent(GtkReaderParagraph *paragraph)
{
  g_return_val_if_fail(paragraph != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), 0);

  return paragraph->indent.value;
}

/**
 * Set horizontal offset of paragraph.
 * @param paragraph a #GtkReaderParagraph.
 * @param type a type of measure - percents of pixels.
 * @param offset an offset.
 */
void gtk_reader_paragraph_set_offset(GtkReaderParagraph     *paragraph,
                                     GtkReaderParagraphValue type,
                                     guint                   offset)
{
  g_return_if_fail(paragraph != NULL);
  g_return_if_fail(GTK_IS_READER_PARAGRAPH(paragraph));

  paragraph->offset.type  = type;
  paragraph->offset.value = offset;
}

/**
 * Returns the type of offset value - percents or pixels.
 * @param paragraph a #GtkReaderParagraph.
 * @return a type of measure.
 */
GtkReaderParagraphValue gtk_reader_paragraph_get_offset_type(GtkReaderParagraph *paragraph)
{
  g_return_val_if_fail(paragraph != NULL, GTK_READER_PARAGRAPH_VALUE_PIXELS);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), GTK_READER_PARAGRAPH_VALUE_PIXELS);

  return paragraph->offset.type;
}

/**
 * Returns horizontal offset of paragraph.
 * @param paragraph a #GtkReaderParagraph.
 * @return an offset in percents or pixels.
 */
guint gtk_reader_paragraph_get_offset(GtkReaderParagraph *paragraph)
{
  g_return_val_if_fail(paragraph != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), 0);

  return paragraph->offset.value;
}

/**
 * Returns align of paragraph.
 * @param paragraph a #GtkReaderParagraph.
 * @return alignment as PangoAlignment structure (see Pango docs).
 */
PangoAlignment gtk_reader_paragraph_get_align(GtkReaderParagraph *paragraph)
{
  g_return_val_if_fail(paragraph != NULL, PANGO_ALIGN_LEFT);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), PANGO_ALIGN_LEFT);

  return paragraph->align;
}

/**
 * Set align of paragraph (left, right or center).
 * @param paragraph a #GtkReaderParagraph.
 * @param align an alignment as PangoAlignment structure (see Pango docs).
 */
void gtk_reader_paragraph_set_align(GtkReaderParagraph *paragraph,
                                    PangoAlignment align)
{
  g_return_if_fail(paragraph != NULL);
  g_return_if_fail(GTK_IS_READER_PARAGRAPH(paragraph));

  paragraph->align = align;
}

/**
 * Returns whether paragraph is justifyed (i.e. aligned to both edges).
 * @param paragraph a #GtkReaderParagraph.
 * @return TRUE if justification is turned on, FALSE othewise.
 */
gboolean gtk_reader_paragraph_get_justify(GtkReaderParagraph *paragraph)
{
  g_return_val_if_fail(paragraph != NULL, PANGO_ALIGN_LEFT);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), PANGO_ALIGN_LEFT);

  return paragraph->justify;
}

/**
 * Sets justification (align of text to both edges) of paragraph.
 * @param paragraph a #GtkReaderParagraph.
 * @param justify a TRUE if paragraph should be justified.
 */
void gtk_reader_paragraph_set_justify(GtkReaderParagraph *paragraph,
                                      gboolean            justify)
{
  g_return_if_fail(paragraph != NULL);
  g_return_if_fail(GTK_IS_READER_PARAGRAPH(paragraph));

  paragraph->justify = justify;
}

GtkReaderParagraphVAlign gtk_reader_paragraph_get_valign(GtkReaderParagraph *paragraph)
{
  g_return_val_if_fail(paragraph != NULL, GTK_READER_PARAGRAPH_VALIGN_TOP);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), GTK_READER_PARAGRAPH_VALIGN_TOP);

  return paragraph->valign;
}

void gtk_reader_paragraph_set_valign(GtkReaderParagraph *paragraph,
                                     GtkReaderParagraphVAlign valign)
{
  g_return_if_fail(paragraph != NULL);
  g_return_if_fail(GTK_IS_READER_PARAGRAPH(paragraph));

  paragraph->valign = valign;
}

void gtk_reader_paragraph_fill_layout(GtkReaderParagraph *paragraph,
                                      PangoLayout        *layout)
{
  GSList *it;
  PangoAttrList *attr_list;
  
  g_return_if_fail(paragraph != NULL);
  g_return_if_fail(GTK_IS_READER_PARAGRAPH(paragraph));
  g_return_if_fail(layout != NULL);
  g_return_if_fail(PANGO_IS_LAYOUT(layout));

  if (paragraph->text)
    pango_layout_set_text(layout, paragraph->text->str, paragraph->text->len);
  else
    pango_layout_set_text(layout, NULL, 0);

  attr_list = pango_attr_list_new();

  for (it = paragraph->chunks; it != NULL; it = g_slist_next(it))
  {
    GtkReaderChunk *chunk;
    PangoAttribute *attr = NULL;

    g_assert(it != NULL && it->data != NULL);
    chunk = (GtkReaderChunk*)it->data;
    if (chunk->type == GTK_READER_CHUNK_TEXT)
    {
      GtkReaderAttr *rattr = chunk->value.textattr;
      g_assert(rattr != NULL);

      if (rattr->type & ATTR_FONT_SIZE)
      {
        if (rattr->is_relative_font_size)
        {
          /* TODO: */
        }
        else
        {
          /* absolute font size */
          attr = pango_attr_size_new(rattr->font_size.absolute * PANGO_SCALE);
          attr->start_index = chunk->start_index;
          attr->end_index   = chunk->end_index;
          pango_attr_list_insert(attr_list, attr);
        }
      }
      if (rattr->type & ATTR_FONT_FAMILY)
      {
        attr = pango_attr_family_new(rattr->font_family);
        attr->start_index = chunk->start_index;
        attr->end_index   = chunk->end_index;
        pango_attr_list_insert(attr_list, attr);
      }
      if (rattr->type & ATTR_STYLE)
      {
        attr = pango_attr_style_new(rattr->style);
        attr->start_index = chunk->start_index;
        attr->end_index   = chunk->end_index;
        pango_attr_list_insert(attr_list, attr);
      }
      if (rattr->type & ATTR_WEIGHT)
      {
        attr = pango_attr_weight_new(rattr->weight);
        attr->start_index = chunk->start_index;
        attr->end_index   = chunk->end_index;
        pango_attr_list_insert(attr_list, attr);
      }
    }
    else if (chunk->type == GTK_READER_CHUNK_IMAGE)
    {
      PangoRectangle log_rect;

      log_rect.x = 0;
      log_rect.y = 0;
      if (paragraph->valign == GTK_READER_PARAGRAPH_VALIGN_BOTTOM)
        log_rect.y = -chunk->value.image.height;
      log_rect.width  = chunk->value.image.width;
      log_rect.height = chunk->value.image.height;
        
      attr = pango_attr_shape_new(&log_rect, &log_rect);
      attr->start_index = chunk->start_index;
      attr->end_index   = chunk->end_index;
      pango_attr_list_insert(attr_list, attr);
    }
  }

  pango_layout_set_attributes(layout, attr_list);
  pango_attr_list_unref(attr_list);
}

gunichar gtk_reader_paragraph_add_chunk(GtkReaderParagraph *paragraph,
                                        GtkReaderChunk *chunk)
{
  g_return_val_if_fail(paragraph != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), 0);
  g_return_val_if_fail(chunk != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_CHUNK(chunk), 0);

  if (chunk->type == GTK_READER_CHUNK_TEXT && paragraph->text &&
      paragraph->text->len < chunk->start_index)
    g_assert_not_reached();
  
  chunk->chunkid = paragraph->max_chunkid++;

  g_object_ref(G_OBJECT(chunk));
  
  paragraph->chunks = g_slist_append(paragraph->chunks, chunk);

  return chunk->chunkid;
}

GtkReaderChunk* gtk_reader_paragraph_get_chunk(GtkReaderParagraph *paragraph,
                                               gunichar chunkid)
{
  GSList *it;

  g_return_val_if_fail(paragraph != NULL, NULL);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), NULL);

  for (it = paragraph->chunks; it != NULL; it = g_slist_next(it))
  {
    GtkReaderChunk *chunk;

    g_assert(it != NULL && it->data != NULL);
    chunk = (GtkReaderChunk*)it->data;
    if (chunk->chunkid == chunkid)
      return chunk;
  }
  
  return NULL;
}

gboolean gtk_reader_paragraph_is_chunkid(GtkReaderParagraph *paragraph,
                                         gunichar chunkid)
{
  g_return_val_if_fail(paragraph != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), FALSE);

  return (chunkid >= CHUNKID_START && chunkid <= CHUNKID_END);
}



/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
