#ifndef __GTK_READER_BUFFER_H__
#define __GTK_READER_BUFFER_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>
#include "gtkreaderparagraph.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_TYPE_READER_BUFFER          (gtk_reader_buffer_get_type() )
#define GTK_READER_BUFFER(obj)          GTK_CHECK_CAST(obj, gtk_reader_buffer_get_type(), GtkReaderBuffer)
#define GTK_READER_BUFFER_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, gtk_reader_buffer_get_type(), GtkReaderBufferClas)
#define GTK_IS_READER_BUFFER(obj)       GTK_CHECK_TYPE(obj, gtk_reader_buffer_get_type())


typedef struct _GtkReaderBuffer        GtkReaderBuffer;
typedef struct _GtkReaderBufferClass   GtkReaderBufferClass;

struct _GtkReaderBuffer
{
  GObject parent_instance;

  GSList *paragraphs;           /**< a list of paragraphs (#GtkReaderParagraph). */
  gchar  *name;                 /**< a name of the buffer. */
  gulong  length;               /**< length of text in all paragraphs (in characters). */
};

struct _GtkReaderBufferClass
{
  GObjectClass parent_class;
};


GtkReaderBuffer* gtk_reader_buffer_new       ();
GtkReaderBuffer* gtk_reader_buffer_new_with_name(const gchar *name);
GtkType          gtk_reader_buffer_get_type  (void);

void  gtk_reader_buffer_set_name         (GtkReaderBuffer    *buffer,
                                          const gchar        *name);
const gchar* gtk_reader_buffer_get_name  (GtkReaderBuffer    *buffer);
void  gtk_reader_buffer_append_paragraph (GtkReaderBuffer    *buffer,
                                          GtkReaderParagraph *paragraph);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTK_READER_BUFFER_H__ */

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
