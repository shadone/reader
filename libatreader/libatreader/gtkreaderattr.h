#ifndef __GTK_READER_ATTR_H__
#define __GTK_READER_ATTR_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>
#include <glib.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define ATTR_FONT_SIZE               0x1
#define ATTR_FONT_FAMILY             0x2
#define ATTR_STYLE                   0x4
#define ATTR_WEIGHT                  0x8

#define GTK_TYPE_READER_ATTR          (gtk_reader_attr_get_type() )
#define GTK_READER_ATTR(obj)          GTK_CHECK_CAST(obj, gtk_reader_attr_get_type(), GtkReaderAttr)
#define GTK_READER_ATTR_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, gtk_reader_attr_get_type(), GtkReaderAttrClass)
#define GTK_IS_READER_ATTR(obj)       GTK_CHECK_TYPE(obj, gtk_reader_attr_get_type())
  
typedef struct _GtkReaderAttr       GtkReaderAttr;
typedef struct _GtkReaderAttrClass  GtkReaderAttrClass;

/**
 * Named text attribute.
 */
struct _GtkReaderAttr
{
  GObject parent_instance;

  gchar *name;                  /**< attribute name */

  guint    type;                /**< what attribute properties should be used */

  /* Font size */
  gboolean is_relative_font_size;
  union
  {
    guint absolute;             /**< absolute font size */
    gint  relative;             /**< relative font size */
  } font_size;

  /* Font family */
  gchar *font_family;           /**< font family name */
  PangoStyle  style;            /**< font style (normal, obliq, italic) */
  PangoWeight weight;           /**< font weight (normal, bold, and shades) */
};

struct _GtkReaderAttrClass
{
  GObjectClass parent_class;
};


GtkReaderAttr*  gtk_reader_attr_new       (const gchar *name);
GtkType         gtk_reader_attr_get_type  (void);
gboolean        gtk_reader_attr_is_set    (GtkReaderAttr *attr);
void            gtk_reader_attr_set_size  (GtkReaderAttr *attr,
                                           guint          size);
void            gtk_reader_attr_set_size_relative(GtkReaderAttr *attr,
                                                  gint           size);
void            gtk_reader_attr_set_font_family  (GtkReaderAttr *attr,
                                                  const gchar   *family);
void            gtk_reader_attr_set_style        (GtkReaderAttr *attr,
                                                  PangoStyle     style);
void            gtk_reader_attr_set_weight       (GtkReaderAttr *attr,
                                                  PangoWeight    weight);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTK_READER_ATTR_H__ */

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
