#ifndef __GTK_READER_PARAGRAPH_H__
#define __GTK_READER_PARAGRAPH_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>
#include <glib.h>
#include "gtkreaderchunk.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_TYPE_READER_PARAGRAPH          (gtk_reader_paragraph_get_type() )
#define GTK_READER_PARAGRAPH(obj)          GTK_CHECK_CAST(obj, gtk_reader_paragraph_get_type(), GtkReaderParagraph)
#define GTK_READER_PARAGRAPH_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, gtk_reader_paragraph_get_type(), GtkReaderParagraphClas)
#define GTK_IS_READER_PARAGRAPH(obj)       GTK_CHECK_TYPE(obj, gtk_reader_paragraph_get_type())

typedef struct _GtkReaderParagraph       GtkReaderParagraph;
typedef struct _GtkReaderParagraphClass  GtkReaderParagraphClass;

// тип отступа: пиксели; проценты
typedef enum
{
  GTK_READER_PARAGRAPH_VALUE_PIXELS=0,
  GTK_READER_PARAGRAPH_VALUE_PERCENTS
} GtkReaderParagraphValue;
 
typedef enum
{
  GTK_READER_PARAGRAPH_VALIGN_TOP=0,
  GTK_READER_PARAGRAPH_VALIGN_BOTTOM
} GtkReaderParagraphVAlign;
 
struct _GtkReaderParagraph
{
  GObject parent_instance;

  gunichar max_chunkid;

  GString *text;
  guint    length;              /**< length of text in characters */
  GSList  *chunks; // список chunk'ов которые нужно вставлять в Layout который создается из text
  struct
  {
    GtkReaderParagraphValue type;
    guint value;
  } indent;
  struct
  {
    GtkReaderParagraphValue type;
    guint value;
  } offset;
  PangoAlignment align;
  GtkReaderParagraphVAlign valign;
  gboolean justify;
};
  
struct _GtkReaderParagraphClass
{
  GObjectClass parent_class;
};


GtkReaderParagraph* gtk_reader_paragraph_new       ();
GtkType             gtk_reader_paragraph_get_type  (void);

void                     gtk_reader_paragraph_set_text   (GtkReaderParagraph *paragraph,
                                                          const gchar        *text);
const gchar*             gtk_reader_paragraph_get_text   (GtkReaderParagraph *paragraph);
GString*                 gtk_reader_paragraph_get_string (GtkReaderParagraph *paragraph);
void                     gtk_reader_paragraph_append_text(GtkReaderParagraph *paragraph,
                                                          const gchar        *text);
void                     gtk_reader_paragraph_set_indent (GtkReaderParagraph     *paragraph,
                                                          GtkReaderParagraphValue type,
                                                          guint                   indent);
GtkReaderParagraphValue  gtk_reader_paragraph_get_indent_type(GtkReaderParagraph *paragraph);
guint                    gtk_reader_paragraph_get_indent     (GtkReaderParagraph *paragraph);
void                     gtk_reader_paragraph_set_offset (GtkReaderParagraph     *paragraph,
                                                          GtkReaderParagraphValue type,
                                                          guint                   offset);
GtkReaderParagraphValue  gtk_reader_paragraph_get_offset_type(GtkReaderParagraph *paragraph);
guint                    gtk_reader_paragraph_get_offset     (GtkReaderParagraph *paragraph);
PangoAlignment           gtk_reader_paragraph_get_align      (GtkReaderParagraph *paragraph);
void                     gtk_reader_paragraph_set_align      (GtkReaderParagraph *paragraph,
                                                              PangoAlignment      align);
gboolean                 gtk_reader_paragraph_get_justify    (GtkReaderParagraph *paragraph);
void                     gtk_reader_paragraph_set_justify    (GtkReaderParagraph *paragraph,
                                                              gboolean            justify);
GtkReaderParagraphVAlign gtk_reader_paragraph_get_valign     (GtkReaderParagraph *paragraph);
void                     gtk_reader_paragraph_set_valign     (GtkReaderParagraph *paragraph,
                                                              GtkReaderParagraphVAlign valign);
void                     gtk_reader_paragraph_fill_layout    (GtkReaderParagraph *paragraph,
                                                              PangoLayout *layout);
gunichar                 gtk_reader_paragraph_add_chunk      (GtkReaderParagraph *paragraph,
                                                              GtkReaderChunk *chunk);
GtkReaderChunk*          gtk_reader_paragraph_get_chunk      (GtkReaderParagraph *paragraph,
                                                              gunichar chunkid);
gboolean                 gtk_reader_paragraph_is_chunkid     (GtkReaderParagraph *paragraph,
                                                              gunichar chunkid);

  
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTK_READER_PARAGRAPH_H__ */

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
