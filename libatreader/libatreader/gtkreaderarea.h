#ifndef __GTK_READER_AREA_H__
#define __GTK_READER_AREA_H__

#include <gdk/gdk.h>
#include <gtk/gtkadjustment.h>
#include <gtk/gtkwidget.h>
#include "gtkreaderbuffer.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GTK_TYPE_READER_AREA          (gtk_reader_area_get_type() )
#define GTK_READER_AREA(obj)          GTK_CHECK_CAST(obj, gtk_reader_area_get_type(), GtkReaderArea)
#define GTK_READER_AREA_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, gtk_reader_area_get_type(), GtkReaderAreaClass)
#define GTK_IS_READER_AREA(obj)       GTK_CHECK_TYPE(obj, gtk_reader_area_get_type())


typedef struct _GtkReaderIndexer     GtkReaderIndexer;
typedef struct _GtkReaderArea        GtkReaderArea;
typedef struct _GtkReaderAreaClass   GtkReaderAreaClass;
  
struct _GtkReaderIndexer
{
  guint32 height;               /* height of all paragraphs */
  GSList *paragraphs;           /* list of paragraphs heights  */
};

struct _GtkReaderArea
{
  GtkWidget widget;

  /* The adjustment object */
  GtkAdjustment *adjustment;

  PangoFontDescription *font;
  PangoLayout *pango_layout;

  /* Backing pixmap for drawing area */
  GdkPixmap *pixmap;

  gchar    *current_buffer;     /* name of currently selected buffer */
  GHashTable      *buffers;     /* a named buffers */
  GtkReaderIndexer indexer;
  guint pos_paragraph;          /* current paragraph number */
  guint pos_offset;             /* current offset in paragraph in pixels */

  gboolean is_modified;         /* whether need to redraw pixmap */
  
  PangoWrapMode wrap_mode;
  guint column_count;
  guint column_spacing;
  gint  spacing;
  gboolean display_partial;
  guint empty_line_height;      /**< height of empty line */
};

struct _GtkReaderAreaClass
{
  GtkWidgetClass parent_class;
};


GtkReaderArea* gtk_reader_area_new                    (GtkAdjustment *adjustment);
GtkType        gtk_reader_area_get_type               (void);

GtkAdjustment* gtk_reader_area_get_adjustment         (GtkReaderArea *readerarea);
void           gtk_reader_area_set_adjustment         (GtkReaderArea *reader_area,
                                                       GtkAdjustment *adjustment);
void           gtk_reader_area_set_buffers            (GtkReaderArea *reader_area,
                                                       GSList        *buffers);
void           gtk_reader_area_add_buffer             (GtkReaderArea *reader_area,
                                                       GtkReaderBuffer *buffer);
void           gtk_reader_area_set_current_buffer     (GtkReaderArea    *area,
                                                       GtkReaderBuffer  *buffer);
void           gtk_reader_area_set_current_buffer_by_name(GtkReaderArea *area,
                                                          const gchar   *name);
const gchar*   gtk_reader_area_get_current_buffer     (GtkReaderArea *area);
GSList*        gtk_reader_area_get_buffer_names       (GtkReaderArea *area);
void           gtk_reader_area_set_column_count       (GtkReaderArea *reader_area,
                                                       guint count);
void           gtk_reader_area_set_column_spacing     (GtkReaderArea *reader_area,
                                                       guint spacing);
guint          gtk_reader_area_get_column_spacing     (GtkReaderArea *reader_area);
guint          gtk_reader_area_get_column_count       (GtkReaderArea *reader_area);
void           gtk_reader_area_set_wrap               (GtkReaderArea *reader_area,
                                                       PangoWrapMode  wrap);
PangoWrapMode  gtk_reader_area_get_wrap               (GtkReaderArea *reader_area);
void           gtk_reader_area_set_default_font_from_string (GtkReaderArea *reader_area,
                                                             gchar *font_description);
void           gtk_reader_area_set_spacing            (GtkReaderArea *reader_area,
                                                       gint spacing);
gint           gtk_reader_area_get_spacing            (GtkReaderArea *reader_area);
void           gtk_reader_area_set_display_partial    (GtkReaderArea *reader_area,
                                                       gboolean is_display);
gboolean       gtk_reader_area_get_display_partial    (GtkReaderArea *reader_area);
void           gtk_reader_area_set_empty_line_height  (GtkReaderArea *area,
                                                       guint height);
void           gtk_reader_area_scroll_pixels          (GtkReaderArea *reader_area,
                                                       gint value);
void           gtk_reader_area_set_position           (GtkReaderArea *reader_area,
                                                       guint paragraph_num,
                                                       gint  paragraph_offset);
void           gtk_reader_area_set_position_absolute  (GtkReaderArea *area,
                                                       guint offset);
guint          gtk_reader_area_get_paragraph_height   (GtkReaderArea *area,
                                                       GtkReaderParagraph *paragraph,
                                                       guint width);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTK_READER_AREA_H__ */

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
