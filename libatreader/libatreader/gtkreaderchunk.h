#ifndef __GTK_READER_CHUNK_H__
#define __GTK_READER_CHUNK_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>
#include <glib.h>
#include "gtkreaderattr.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GTK_TYPE_READER_CHUNK          (gtk_reader_chunk_get_type() )
#define GTK_READER_CHUNK(obj)          GTK_CHECK_CAST(obj, gtk_reader_chunk_get_type(), GtkReaderChunk)
#define GTK_READER_CHUNK_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, gtk_reader_chunk_get_type(), GtkReaderChunkClass)
#define GTK_IS_READER_CHUNK(obj)       GTK_CHECK_TYPE(obj, gtk_reader_chunk_get_type())

extern const gunichar SYMBOL_IMAGE;
  
typedef struct _GtkReaderChunkImage  GtkReaderChunkImage;
typedef struct _GtkReaderChunkTable  GtkReaderChunkTable;
typedef struct _GtkReaderChunk       GtkReaderChunk;
typedef struct _GtkReaderChunkClass  GtkReaderChunkClass;

// Type of "chunk" of type 'text'
typedef enum /*< skip >*/
{
  GTK_READER_CHUNK_TEXT=0,
  GTK_READER_CHUNK_IMAGE,
  GTK_READER_CHUNK_TABLE
} GtkReaderChunkType;

struct _GtkReaderChunkImage
{
  guint width;
  guint height;
  //TODO: Image *image;
};

struct _GtkReaderChunkTable
{
  guint cols;
  guint rows;
  //TODO: table
};

struct _GtkReaderChunk
{
  GObject parent_instance;

  gunichar chunkid;

  guint start_index;            /**< start byte index of the range. */
  guint end_index;              /**< end byte index of the range. The character at this index is not included in the range. */
  
  GtkReaderChunkType  type;
  union {
    GtkReaderAttr      *textattr;
    GtkReaderChunkImage image;
    GtkReaderChunkTable table;
  } value;
};
  
struct _GtkReaderChunkClass
{
  GObjectClass parent_class;
};


GtkReaderChunk* gtk_reader_chunk_new       ();
GtkType         gtk_reader_chunk_get_type  (void);
void            gtk_reader_chunk_set_position    (GtkReaderChunk *chunk,
                                                  guint           start,
                                                  guint           end);
void            gtk_reader_chunk_set_text_value  (GtkReaderChunk *chunk,
                                                  GtkReaderAttr  *attr);
void            gtk_reader_chunk_set_image_value (GtkReaderChunk *chunk,
                                                  GtkReaderChunkImage Image);
void            gtk_reader_chunk_set_table_value (GtkReaderChunk *chunk,
                                                  GtkReaderChunkTable Table);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTK_READER_CHUNK_H__ */

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
