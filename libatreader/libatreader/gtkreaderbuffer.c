#include <string.h>
#include "gtkreaderbuffer.h"

/* Forward declarations */

static void gtk_reader_buffer_class_init               (GtkReaderBufferClass *klass);
static void gtk_reader_buffer_init                     (GtkReaderBuffer      *reader_buffer);
static void gtk_reader_buffer_finalize                 (GObject *object);

/* Local data */

static gpointer parent_class = NULL;

GType gtk_reader_buffer_get_type()
{
  static GType reader_buffer_type = 0;
  
  if (!reader_buffer_type)
  {
    static const GTypeInfo reader_buffer_info =
      {
        sizeof (GtkReaderBufferClass),
        NULL,
        NULL,
        (GClassInitFunc) gtk_reader_buffer_class_init,
        NULL,
        NULL,
        sizeof (GtkReaderBuffer),
        0,
        (GInstanceInitFunc) gtk_reader_buffer_init,
      };
    
    reader_buffer_type = g_type_register_static(G_TYPE_OBJECT, "GtkReaderBuffer", &reader_buffer_info, 0);
  }
  
  return reader_buffer_type;
}

static void gtk_reader_buffer_class_init(GtkReaderBufferClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));

  gobject_class->finalize = gtk_reader_buffer_finalize;
}

static void gtk_reader_buffer_init(GtkReaderBuffer *reader_buffer)
{
  reader_buffer->paragraphs = NULL;
  reader_buffer->name   = NULL;
  reader_buffer->length = 0;
}

/**
 * Destroys a buffer when ref count == 0.
 * @param object a #GtkReaderBuffer.
 */
static void gtk_reader_buffer_finalize(GObject *object)
{
  GSList *it;
  GtkReaderBuffer *buffer;

  buffer = GTK_READER_BUFFER(object);
  g_assert(buffer != NULL);
  
  /* free all allocated data, including all paragraphs */
  for (it = buffer->paragraphs; it != NULL; it = g_slist_next(it))
  {
    GtkReaderParagraph *paragraph;
    
    g_assert(it != NULL && it->data != NULL);
    paragraph = GTK_READER_PARAGRAPH(it->data);
    g_object_unref(paragraph);
  }
  g_slist_free(buffer->paragraphs);
  
  if (buffer->name)
    g_free(buffer->name);

  G_OBJECT_CLASS(parent_class)->finalize(object);
}

GtkReaderBuffer* gtk_reader_buffer_new()
{
  GtkReaderBuffer *reader_buffer;

  reader_buffer = g_object_new(gtk_reader_buffer_get_type(), NULL);

  return reader_buffer;
}

GtkReaderBuffer* gtk_reader_buffer_new_with_name(const gchar *name)
{
  GtkReaderBuffer *buffer;
  
  buffer = gtk_reader_buffer_new();
  g_assert(buffer != NULL);
  gtk_reader_buffer_set_name(buffer, name);
  
  return buffer;
}

/**
 * Sets the name of the buffer.
 * @param buffer a #GtkReaderBuffer.
 * @param name a new name of the buffer.
 */
void gtk_reader_buffer_set_name(GtkReaderBuffer *buffer,
                                const gchar     *name)
{
  g_return_if_fail(buffer != NULL);
  g_return_if_fail(GTK_IS_READER_BUFFER(buffer));
  g_return_if_fail(name != NULL);

  if (buffer->name)
    g_free(buffer->name);

  buffer->name = g_strdup(name);
}

/**
 * Returns name of the buffer.
 * @param buffer a #GtkReaderBuffer.
 * @return a name of the buffer.
 */
const gchar* gtk_reader_buffer_get_name(GtkReaderBuffer *buffer)
{
  g_return_val_if_fail(buffer != NULL, NULL);
  g_return_val_if_fail(GTK_IS_READER_BUFFER(buffer), NULL);

  return buffer->name;
}

/**
 * Appends new paragraph to the buffer.
 * @param buffer a #GtkReaderBuffer.
 * @param paragraph a #GtkReaderParagraph to append.
 */
void gtk_reader_buffer_append_paragraph(GtkReaderBuffer    *buffer,
                                        GtkReaderParagraph *paragraph)
{
  GString *string;

  g_return_if_fail(buffer != NULL);
  g_return_if_fail(GTK_IS_READER_BUFFER(buffer));
  g_return_if_fail(paragraph != NULL);
  g_return_if_fail(GTK_IS_READER_PARAGRAPH(paragraph));

  g_object_ref(paragraph);

  buffer->paragraphs = g_slist_append(buffer->paragraphs, paragraph);
  buffer->length    += paragraph->length;
}


/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
