#include <math.h>
#include <stdio.h>
#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>
#include <pango/pango.h>
#include <string.h>

#include "gtkreaderarea.h"

#define READER_AREA_DEFAULT_SIZE 100
#define SCROLL_PIXELS            10
#define KEY_SCROLL_PIXELS        0

/* Forward declarations */

static void gtk_reader_area_class_init               (GtkReaderAreaClass *klass);
static void gtk_reader_area_init                     (GtkReaderArea      *reader_area);
static void gtk_reader_area_destroy                  (GtkObject        *object);
static void gtk_reader_area_realize                  (GtkWidget        *widget);
static void gtk_reader_area_size_request             (GtkWidget        *widget,
                                                      GtkRequisition   *requisition);
static void gtk_reader_area_size_allocate            (GtkWidget        *widget,
                                                      GtkAllocation    *allocation);
static gboolean gtk_reader_area_expose               (GtkWidget        *widget,
                                                      GdkEventExpose   *event);
static gboolean gtk_reader_area_button_press         (GtkWidget        *widget,
                                                      GdkEventButton   *event);
static gboolean gtk_reader_area_button_release       (GtkWidget        *widget,
                                                      GdkEventButton   *event);
static gboolean gtk_reader_area_motion_notify        (GtkWidget        *widget,
                                                      GdkEventMotion   *event);
static gboolean gtk_reader_area_configure            (GtkWidget        *widget,
                                                      GdkEventConfigure *event);
  
static void gtk_reader_area_adjustment_update        (GtkReaderArea *reader_area);
static void gtk_reader_area_adjustment_changed       (GtkAdjustment    *adjustment,
                                                      gpointer          data);
static void gtk_reader_area_adjustment_value_changed (GtkAdjustment    *adjustment,
                                                      gpointer          data);
static void gtk_reader_area_send_configure           (GtkReaderArea *reader_area);
void        gtk_reader_area_redraw                   (GtkReaderArea *reader_area);
guint       gtk_reader_area_get_column_width         (GtkReaderArea *reader_area);
void        gtk_reader_area_draw_paragraph           (GtkReaderArea *area,
                                                      GdkDrawable *drawable,
                                                      GtkReaderParagraph *paragraph,
                                                      gint xoffset,
                                                      gint yoffset,
                                                      guint width,
                                                      guint height,
                                                      guint *result_width,
                                                      guint *result_height,
                                                      GSList **line_heights);
void         gtk_reader_area_update_indexer          (GtkReaderArea *reader_area);
void         gtk_reader_area_set_position            (GtkReaderArea *reader_area,
                                                      guint paragraph_num,
                                                      gint  paragraph_offset);
GtkReaderBuffer* gtk_reader_area_get_cur_buffer      (GtkReaderArea *area);
gulong           gtk_reader_area_get_char_position   (GtkReaderArea *area);

typedef struct _RParagraphLine RParagraphLine;
struct _RParagraphLine
{
  gint line_height;
  gint line_width;
  gint glyphs_count;
  PangoLayoutLine *line;
};
RParagraphLine* r_paragraph_line_new  ();
void            r_paragraph_line_free (RParagraphLine **line);

/* Local data */

static GtkWidgetClass *parent_class = NULL;

GType gtk_reader_area_get_type()
{
  static GType reader_area_type = 0;
  
  if (!reader_area_type)
  {
    static const GTypeInfo reader_area_info =
      {
        sizeof (GtkReaderAreaClass),
        NULL,
        NULL,
        (GClassInitFunc) gtk_reader_area_class_init,
        NULL,
        NULL,
        sizeof (GtkReaderArea),
        0,
        (GInstanceInitFunc) gtk_reader_area_init,
      };
    
    reader_area_type = g_type_register_static(GTK_TYPE_WIDGET, "GtkReaderArea", &reader_area_info, 0);
  }
  
  return reader_area_type;
}

static void gtk_reader_area_class_init(GtkReaderAreaClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = GTK_OBJECT_CLASS(klass);
  widget_class = GTK_WIDGET_CLASS(klass);

  parent_class = gtk_type_class(gtk_widget_get_type());

  object_class->destroy = gtk_reader_area_destroy;

  widget_class->realize = gtk_reader_area_realize;
  widget_class->expose_event = gtk_reader_area_expose;
  widget_class->size_request = gtk_reader_area_size_request;
  widget_class->size_allocate = gtk_reader_area_size_allocate;
  widget_class->button_press_event = gtk_reader_area_button_press;
  widget_class->button_release_event = gtk_reader_area_button_release;
  widget_class->motion_notify_event = gtk_reader_area_motion_notify;
  widget_class->configure_event = gtk_reader_area_configure;
}

static void gtk_reader_area_init(GtkReaderArea *reader_area)
{
  PangoContext *context;

  GTK_WIDGET_SET_FLAGS(GTK_WIDGET(reader_area), GTK_CAN_FOCUS);

  reader_area->is_modified = FALSE;

  reader_area->current_buffer = NULL;
  reader_area->buffers = NULL;
  memset(&(reader_area->indexer), 0, sizeof(reader_area->indexer));
  reader_area->adjustment = NULL;

  reader_area->wrap_mode    = PANGO_WRAP_WORD;
  reader_area->column_count = 1;
  reader_area->spacing      = 2;
  reader_area->column_spacing = 2;
  reader_area->empty_line_height = 10;
  reader_area->display_partial = TRUE;
  reader_area->pos_paragraph = 0;
  reader_area->pos_offset    = 0;

  // getting context of the widget and creating PangoLayout
  context = gdk_pango_context_get();
  reader_area->pango_layout = pango_layout_new(context);
  reader_area->font = NULL;

  gtk_widget_add_events(GTK_WIDGET(reader_area), GDK_KEY_PRESS_MASK);
  // because we got widgets pango context, we must track changes in it
  g_signal_connect_swapped(G_OBJECT(reader_area), "style_set",
                           G_CALLBACK(pango_layout_context_changed),
                           reader_area->pango_layout);
  g_signal_connect_swapped(G_OBJECT(reader_area), "direction_changed",
                           G_CALLBACK(pango_layout_context_changed),
                           reader_area->pango_layout);
}

/**
 * Creates a new #GtkReaderArea.
 * @return a new #GtkReaderArea.
 */
GtkReaderArea* gtk_reader_area_new(GtkAdjustment *adjustment)
{
  GtkReaderArea *reader_area;

  reader_area = g_object_new(gtk_reader_area_get_type(), NULL);

  if (!adjustment)
    adjustment = (GtkAdjustment*) gtk_adjustment_new(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

  gtk_reader_area_set_adjustment(reader_area, adjustment);

  return reader_area;
}

static void gtk_reader_area_destroy(GtkObject *object)
{
  GtkReaderArea *reader_area;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(object));

  reader_area = GTK_READER_AREA(object);

  if (reader_area->adjustment)
  {
    g_object_unref(GTK_OBJECT(reader_area->adjustment));
    reader_area->adjustment = NULL;
  }

  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (* GTK_OBJECT_CLASS(parent_class)->destroy)(object);
}

GtkAdjustment* gtk_reader_area_get_adjustment(GtkReaderArea *reader_area)
{
  g_return_val_if_fail(reader_area != NULL, NULL);
  g_return_val_if_fail(GTK_IS_READER_AREA(reader_area), NULL);

  return reader_area->adjustment;
}

void gtk_reader_area_set_adjustment(GtkReaderArea *reader_area,
                                    GtkAdjustment *adjustment)
{
  g_return_if_fail(reader_area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(reader_area));
  g_return_if_fail(adjustment && GTK_IS_ADJUSTMENT(adjustment));
  
  if (reader_area->adjustment)
  {
    g_signal_handlers_disconnect_by_func(
      GTK_OBJECT(reader_area->adjustment),
      GTK_SIGNAL_FUNC(gtk_reader_area_adjustment_changed),
      (gpointer)reader_area);
    g_signal_handlers_disconnect_by_func(
      GTK_OBJECT(reader_area->adjustment),
      GTK_SIGNAL_FUNC(gtk_reader_area_adjustment_value_changed),
      (gpointer)reader_area);
    g_object_unref(GTK_OBJECT(reader_area->adjustment));
  }

  reader_area->adjustment = adjustment;
  if (reader_area->adjustment)
  {
    GtkReaderBuffer *buffer;
    
    g_object_ref(GTK_OBJECT(reader_area->adjustment));

    g_signal_connect(GTK_OBJECT(adjustment), "changed",
                     GTK_SIGNAL_FUNC(gtk_reader_area_adjustment_changed),
                     (gpointer) reader_area);
    g_signal_connect(GTK_OBJECT(adjustment), "value_changed",
                     GTK_SIGNAL_FUNC(gtk_reader_area_adjustment_value_changed),
                     (gpointer) reader_area);

    reader_area->adjustment->lower = 0;
    reader_area->adjustment->upper = 0;

    buffer = gtk_reader_area_get_cur_buffer(reader_area);
    if (buffer)
      reader_area->adjustment->upper = buffer->length;

    gtk_reader_area_adjustment_update(reader_area);
  }
}

static void gtk_reader_area_realize(GtkWidget *widget)
{
  GtkReaderArea *reader_area;
  GdkWindowAttr attributes;
  gint attributes_mask;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(widget));

  GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);
  reader_area = GTK_READER_AREA(widget);

  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.event_mask = gtk_widget_get_events(widget) | 
    GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | 
    GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK |
    GDK_POINTER_MOTION_HINT_MASK;
  attributes.visual = gtk_widget_get_visual(widget);
  attributes.colormap = gtk_widget_get_colormap(widget);

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
  widget->window = gdk_window_new(widget->parent->window, &attributes, attributes_mask);

  widget->style = gtk_style_attach(widget->style, widget->window);

  gdk_window_set_user_data(widget->window, widget);

  gtk_style_set_background(widget->style, widget->window, GTK_STATE_ACTIVE);

  gtk_reader_area_send_configure(reader_area);
}

static void gtk_reader_area_size_request(GtkWidget      *widget,
                                         GtkRequisition *requisition)
{
  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(widget));
  g_return_if_fail(requisition != NULL);

  requisition->width = READER_AREA_DEFAULT_SIZE;
  requisition->height = READER_AREA_DEFAULT_SIZE;
}

static void gtk_reader_area_size_allocate(GtkWidget     *widget,
                                          GtkAllocation *allocation)
{
  GtkReaderArea *reader_area;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(widget));
  g_return_if_fail(allocation != NULL);

  widget->allocation = *allocation;
  reader_area = GTK_READER_AREA(widget);

  if (GTK_WIDGET_REALIZED(widget))
  {
    gdk_window_move_resize(widget->window,
                           allocation->x, allocation->y,
                           allocation->width, allocation->height);

    // sending configure event
    gtk_reader_area_send_configure(reader_area);
  }
}

static void gtk_reader_area_send_configure(GtkReaderArea *reader_area)
{
  GtkWidget *widget;
  GdkEventConfigure event;
  
  g_return_if_fail(reader_area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(reader_area));

  widget = GTK_WIDGET(reader_area);
  
  event.type = GDK_CONFIGURE;
  event.window = widget->window;
  event.send_event = TRUE;
  event.x = widget->allocation.x;
  event.y = widget->allocation.y;
  event.width = widget->allocation.width;
  event.height = widget->allocation.height;
  
  gtk_widget_event(widget, (GdkEvent*)&event);
}

static gboolean gtk_reader_area_expose(GtkWidget      *widget,
                                       GdkEventExpose *event)
{
  GtkReaderArea *reader_area;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_READER_AREA(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  if (event->count > 0)
    return FALSE;
  
  reader_area = GTK_READER_AREA(widget);

  if (reader_area->is_modified)
    gtk_reader_area_redraw(reader_area);

  reader_area->is_modified = FALSE;
  
  g_assert(reader_area->pixmap != NULL);
  gdk_draw_drawable(widget->window,
                    widget->style->fg_gc[GTK_WIDGET_STATE(widget)],
                    GDK_DRAWABLE(reader_area->pixmap),
                    event->area.x, event->area.y,
                    event->area.x, event->area.y,
                    event->area.width, event->area.height);

  return FALSE;
}

static gboolean gtk_reader_area_button_press(GtkWidget      *widget,
                                             GdkEventButton *event)
{
  GtkReaderArea *reader_area;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_READER_AREA(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  reader_area = GTK_READER_AREA(widget);

  // Do something
  
  return FALSE;
}

static gboolean gtk_reader_area_button_release(GtkWidget      *widget,
                                               GdkEventButton *event)
{
  GtkReaderArea *reader_area;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_READER_AREA(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  reader_area = GTK_READER_AREA(widget);

  // Do something

  return FALSE;
}

static gboolean gtk_reader_area_motion_notify(GtkWidget      *widget,
                                              GdkEventMotion *event)
{
  GtkReaderArea *reader_area;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_READER_AREA(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  reader_area = GTK_READER_AREA(widget);

  // Do something

  return FALSE;
}

static void gtk_reader_area_adjustment_update(GtkReaderArea *area)
{
  GtkAdjustment *adj;
  gint height;
  
  g_return_if_fail(area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(area));

  adj    = area->adjustment;
  height = GTK_WIDGET(area)->allocation.height;

  /* TODO: make proper scrolling */
/*   buffer = gtk_reader_area_get_cur_buffer(area); */
/*   g_assert(buffer != NULL); */
  
  adj->step_increment = MIN(adj->upper, SCROLL_PIXELS);
  adj->page_increment = MIN(adj->upper, height - KEY_SCROLL_PIXELS);
  adj->page_size      = MIN(adj->upper, height);
  adj->value          = MIN(adj->value, adj->upper - adj->page_size);
  adj->value          = MAX(adj->value, 0.0);
}

static void gtk_reader_area_adjustment_changed(GtkAdjustment *adjustment,
                                               gpointer       data)
{
  GtkReaderArea *reader_area;

  g_return_if_fail(adjustment != NULL);
  g_return_if_fail(GTK_IS_ADJUSTMENT(adjustment));
  g_return_if_fail(data != NULL);

  reader_area = GTK_READER_AREA(data);

  /* TODO: what should I do here ?? */
  
  gtk_reader_area_adjustment_update(reader_area);
}

static void gtk_reader_area_adjustment_value_changed(GtkAdjustment *adjustment,
                                                     gpointer       data)
{
  GtkReaderArea *reader_area;

  g_return_if_fail(adjustment != NULL);
  g_return_if_fail(GTK_IS_ADJUSTMENT(adjustment));
  g_return_if_fail(data != NULL);

  if (GTK_IS_READER_AREA(data) == FALSE)
    return;
  
  reader_area = GTK_READER_AREA(data);

  /* scroll to given position */
  gtk_reader_area_set_position_absolute(reader_area,
                                        adjustment->value);
  
  gtk_widget_queue_draw(GTK_WIDGET(reader_area));
}

static gboolean gtk_reader_area_configure(GtkWidget         *widget,
                                          GdkEventConfigure *event)
{
  GtkReaderArea *reader_area;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_READER_AREA(widget), FALSE);
  
  reader_area = GTK_READER_AREA(widget);
  
  if (reader_area->pixmap)
    gdk_pixmap_unref(reader_area->pixmap);

  reader_area->pixmap = gdk_pixmap_new(widget->window,
                                       widget->allocation.width,
                                       widget->allocation.height,
                                       -1);
  g_assert(reader_area->pixmap != NULL);

  gtk_reader_area_scroll_pixels(reader_area, 0);

  gtk_reader_area_update_indexer(reader_area);
  
  gtk_reader_area_redraw(reader_area);
  
  return TRUE;
}

/**
 * Sets buffer list.
 * @param reader_area a #GtkReaderArea.
 * @param buffers a list of named buffers (#GtkReaderBuffer).
 * Buffers shouldn't be freed outside #GtkReaderArea.
 */
void gtk_reader_area_set_buffers(GtkReaderArea *reader_area,
                                 GSList        *buffers)
{
  const gchar *buffer_name;
  GSList      *it;
  
  g_return_if_fail(reader_area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(reader_area));
  g_return_if_fail(buffers != NULL);

  if (reader_area->buffers)
  {
    g_hash_table_destroy(reader_area->buffers);
    reader_area->buffers = NULL;
  }
  
  for (it = buffers; it != NULL; it = g_slist_next(it))
  {
    GtkReaderBuffer *buffer;
    
    g_assert(it != NULL && it->data != NULL);
    buffer = GTK_READER_BUFFER(it->data);
    gtk_reader_area_add_buffer(reader_area, buffer);
  }

  /* now we must set current buffer to the new one */
  buffer_name = gtk_reader_buffer_get_name(GTK_READER_BUFFER(buffers->data));
  gtk_reader_area_set_current_buffer_by_name(reader_area, (const gchar*)buffer_name);
  
  gtk_reader_area_update_indexer(reader_area);
  
  reader_area->is_modified = TRUE;
}

void gtk_reader_area_add_buffer(GtkReaderArea   *area,
                                GtkReaderBuffer *buffer)
{
  const gchar *name;
  gpointer v;
  
  g_return_if_fail(area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(area));
  g_return_if_fail(buffer != NULL);
  g_return_if_fail(GTK_IS_READER_BUFFER(buffer));

  if (area->buffers == NULL)
  {
    area->buffers = g_hash_table_new_full(
      g_str_hash, g_str_equal, g_free, (GDestroyNotify)g_object_unref);
    g_assert(area->buffers != NULL);
  }

  name = gtk_reader_buffer_get_name(buffer);
  if (name == NULL)
  {
    g_warning("Trying to add unnamed buffer\n");
    return;
  }
  
  v = g_hash_table_lookup(area->buffers, name);
  if (v != NULL)
  {
    g_object_unref(G_OBJECT(v));
  }

  g_object_ref(G_OBJECT(buffer));

  /* if already had buffer with the name, it will be replaced */
  g_hash_table_insert(area->buffers, g_strdup(name), buffer);
}

/**
 * Parses PangoLayout and returns list of lines which are drawn as
 * single paragraph.
 * @param area a GtkReaderArea.
 * @param paragraph a paragraph to parse.
 * @param lines stores newly created list of #RParagraphLine structs
 * which describe lines of paragraph.
 */
void gtk_reader_area_get_paragraph_lines(GtkReaderArea      *area,
                                         GtkReaderParagraph *paragraph,
                                         GSList            **lines)
{
  GSList *list;
  GSList *it;
  
  g_return_if_fail(area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(area));
  g_return_if_fail(paragraph != NULL);
  g_return_if_fail(GTK_IS_READER_PARAGRAPH(paragraph));
  g_return_if_fail(lines != NULL);

  g_assert(*lines == NULL /*must be initialized with NULL!*/);
  
  list = pango_layout_get_lines(area->pango_layout);
  g_return_if_fail(list != NULL);

  for (it = list; it != NULL; it = g_slist_next(it))
  {
    RParagraphLine *rpl=NULL;
    PangoRectangle  ink_rect, logical_rect;
    GSList *rit;

    g_assert(it != NULL && it->data != NULL);
    
    rpl = r_paragraph_line_new();
    g_assert(rpl != NULL);
    rpl->line = (PangoLayoutLine*)it->data;
    pango_layout_line_get_pixel_extents(rpl->line, &ink_rect, &logical_rect);
    rpl->line_width  = PANGO_LBEARING(ink_rect)+PANGO_RBEARING(ink_rect);
    rpl->line_height = PANGO_ASCENT(ink_rect)+PANGO_DESCENT(ink_rect);

    /* counting number of glyphs in a line */
    rpl->glyphs_count = 0;
    if (rpl->line->runs)
    {
      for (rit = rpl->line->runs; rit != NULL; rit = g_slist_next(rit))
      {
        PangoLayoutRun *run;
        
        g_assert(rit != NULL && rit->data != NULL);
        run = rit->data;
        rpl->glyphs_count += run->glyphs->num_glyphs;
      }
    }
    
    *lines = g_slist_append(*lines, rpl);
  }
}

void gtk_reader_area_init_options(GtkReaderArea *area,
                                  GtkReaderParagraph *paragraph,
                                  guint width,
                                  guint *indent_)
{
  PangoWrapMode  wrap;
  PangoAlignment align;
  gboolean       justify;
  gint           spacing;
  guint          indent;

  g_return_if_fail(area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(area));
  g_return_if_fail(paragraph != NULL);
  g_return_if_fail(GTK_IS_READER_PARAGRAPH(paragraph));

  if (area->font)
    pango_layout_set_font_description(area->pango_layout, area->font);
  
  wrap    = gtk_reader_area_get_wrap(area);
  spacing = gtk_reader_area_get_spacing(area);
  align   = gtk_reader_paragraph_get_align(paragraph);
  justify = gtk_reader_paragraph_get_justify(paragraph);

  pango_layout_set_width(area->pango_layout, width*PANGO_SCALE);
  pango_layout_set_wrap(area->pango_layout, wrap);
  pango_layout_set_alignment(area->pango_layout, align);
  pango_layout_set_justify(area->pango_layout, FALSE); /* native set_justify in pango is not implemented :( */
  switch (gtk_reader_paragraph_get_indent_type(paragraph))
  {
  case GTK_READER_PARAGRAPH_VALUE_PIXELS:
    indent = gtk_reader_paragraph_get_indent(paragraph);
    break;
  case GTK_READER_PARAGRAPH_VALUE_PERCENTS:
    indent = gtk_reader_paragraph_get_indent(paragraph)%100/100.0*width;
    break;
  }
  pango_layout_set_indent(area->pango_layout, indent*PANGO_SCALE);  

  if (indent_)
    *indent_ = indent;
}

/**
 * Draws a paragraph on drawable using layout.
 * @param area a #GtkReaderArea.
 * @param drawable specifies where to draw paragraph. If drawable ==
 * NULL then dont draw, but simple calculate how much space would
 * paragraph use.
 * @param xoffset a horizontal offset of paragraph.
 * @param yoffset a vertical offset of paragraph.
 * @param width a maximum width of paragraph.
 * @param height a maximum height of paragraph.
 * @param result_width will be placed true width of paragraph.
 * NULL if not needed.
 * @param result_height will be placed true height of paragraph.
 * NULL if not needed.
 * @param lines_height will be placed a list of paragaphs
 * heights. NULL if not needed.
 */
void gtk_reader_area_draw_paragraph(GtkReaderArea      *area,
                                    GdkDrawable        *drawable,
                                    GtkReaderParagraph *paragraph,
                                    gint                xoffset,
                                    gint                yoffset,
                                    guint               width,
                                    guint               height,
                                    guint              *result_width,
                                    guint              *result_height,
                                    GSList            **lines_height)
{
  GSList *lines=NULL;
  GSList *lit;
  PangoRectangle ink_rect, logical_rect;
  PangoAlignment align;
  gboolean       justify;
  gint  baseline;
  guint indent;
  PangoLayoutLine *line;
  guint line_num, line_count;
  GtkReaderParagraphVAlign valign;

  g_return_if_fail(area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(area));
  g_return_if_fail(paragraph != NULL);
  g_return_if_fail(GTK_IS_READER_PARAGRAPH(paragraph));

  g_assert(result_width != NULL && result_height != NULL);

  indent = 0;
  *result_width  = 0;
  *result_height = 0;

  if (paragraph->text == NULL)
  {
    *result_width  = 0;
    *result_height = 0;
    *lines_height  = NULL;
    return;
  }

  /* initialize option for this paragraph */
  gtk_reader_area_init_options(area, paragraph, width, &indent);
  align = gtk_reader_paragraph_get_align(paragraph);
  justify = gtk_reader_paragraph_get_justify(paragraph);
  valign  = gtk_reader_paragraph_get_valign(paragraph);

  /* fill layout with attributed text */
  gtk_reader_paragraph_fill_layout(paragraph, area->pango_layout);

  pango_layout_get_pixel_extents(area->pango_layout, &ink_rect, &logical_rect);
  *result_width  = PANGO_LBEARING(ink_rect)+PANGO_RBEARING(ink_rect);
  /* result_height we must calculate manual because of variable line spacing */
  
  /* set line height if line is empty */
  if (paragraph->text->len == 0)
    *result_height = area->empty_line_height;

  lines = NULL;
  gtk_reader_area_get_paragraph_lines(area, paragraph, &lines);
  g_return_if_fail(lines != NULL);
  
  line_count = g_slist_length(lines);

  baseline = yoffset;
  line_num = 0;
  
  for (lit = lines; lit != NULL; lit = g_slist_next(lit))
  {
    guint x;
    gboolean is_last_line_justify; /* нужно ли выравнивать последнюю строку параграфа */
    gint  chunk_num;
    GSList *rit;
    guint glyph_count;
    PangoGlyphString *gs;
    guint i;
    guint whitespaces_count = 0;
    guint line_free_space=0; // место которое нужно заполнить при justify
    RParagraphLine *rpl=NULL;
    
    chunk_num = 0;

    g_assert(lit != NULL && lit->data != NULL);
    rpl = (RParagraphLine*)lit->data;
    line = rpl->line;
    g_assert(line != NULL);
    
    pango_layout_line_get_pixel_extents(line, &ink_rect, &logical_rect);

    if (lines_height)
    {
      if (area->display_partial || baseline+PANGO_DESCENT(ink_rect) <= height)
        *lines_height = g_slist_append(*lines_height, GUINT_TO_POINTER(rpl->line_height));
    }

    baseline   += PANGO_ASCENT(ink_rect);

    *result_height += rpl->line_height + area->spacing;
    
    x = xoffset;
    if (align == PANGO_ALIGN_LEFT && line_num == 0) /* if first line of paragraph*/
    {
      x += indent;
    }
    
    is_last_line_justify = FALSE;
    if (justify)
    {
      if ((float)rpl->line_width/width > 0.7 && line_num == line_count-1)
        is_last_line_justify = TRUE;

      line_free_space = width-rpl->line_width;
      if (line_num == 0)
        line_free_space -= indent;
    }
    
    if (!justify || (line_num == line_count-1 && !is_last_line_justify))
    {
      if (align == PANGO_ALIGN_RIGHT)
      {
        x += width - rpl->line_width;
      }
      else if (align == PANGO_ALIGN_CENTER)
      {
        x += (width - rpl->line_width)/2;
      }
    }
    
    gs = pango_glyph_string_new();
    g_assert(gs != NULL);
    
    pango_glyph_string_set_size(gs, 1);
    g_assert(gs->glyphs != NULL);
    
    if (justify && drawable)
    { /* calculating number of whitespaces in a line */
      PangoLayoutRun *run; 
      gunichar unichar;
      
      for (rit = line->runs; rit != NULL; rit = g_slist_next(rit))
      {
        g_assert(rit != NULL && rit->data != NULL);
        run = rit->data;
        glyph_count = run->glyphs->num_glyphs;
        for (i = 0; i < glyph_count; i++)
        {
          const gchar *text;
          
          text = pango_layout_get_text(area->pango_layout);
          unichar = g_utf8_get_char(&text[run->item->offset + run->glyphs->log_clusters[i]]);
          if (g_unichar_isspace(unichar))
            whitespaces_count++;
        }
      }
      if (g_unichar_isspace(unichar))
        whitespaces_count--;
    }
    
    for (rit = line->runs; rit != NULL; rit = g_slist_next(rit))
    {
      PangoLayoutRun *run;
      
      g_assert(rit != NULL && rit->data != NULL);
      run = rit->data;
      glyph_count = run->glyphs->num_glyphs;
      
      for (i = 0; i < glyph_count; i++)
      {
        PangoGlyphInfo gi_old;
        
        gi_old = gs->glyphs[0];
        gs->glyphs[0] = run->glyphs->glyphs[i];
        
        if (drawable)
        {
          const gchar *text;
          gunichar unichar;
          gchar *symbol;

          if (area->display_partial || (!area->display_partial && baseline+PANGO_DESCENT(ink_rect) < height))
          {
            gdk_draw_glyphs(
              drawable,
              GTK_WIDGET(area)->style->black_gc,
              run->item->analysis.font,
              x, baseline,
              gs);
          }
          
          text = pango_layout_get_text(area->pango_layout);
          symbol = g_malloc0(6); /* need at least 6 bytes for single utf8 character */
          unichar = g_utf8_get_char(&text[run->item->offset + run->glyphs->log_clusters[i]]);
          g_unichar_to_utf8(unichar, symbol);
          
          if (gtk_reader_paragraph_is_chunkid(paragraph, unichar))
          {
            GtkReaderChunk *image;
            guint dy = 0;
            
            image = gtk_reader_paragraph_get_chunk(paragraph, unichar);
            g_assert(image != NULL);
            g_assert(image->type == GTK_READER_CHUNK_IMAGE);
            
            if (valign == GTK_READER_PARAGRAPH_VALIGN_BOTTOM)
              dy = PANGO_ASCENT(ink_rect);
            else if (valign == GTK_READER_PARAGRAPH_VALIGN_TOP)
              dy = image->value.image.height/PANGO_SCALE;
            
            if (drawable && area->display_partial && baseline+PANGO_DESCENT(ink_rect) < height)
              gdk_draw_rectangle(
                drawable,
                GTK_WIDGET(area)->style->black_gc,
                FALSE,
                x,
                baseline-dy,
                image->value.image.width/PANGO_SCALE,
                image->value.image.height/PANGO_SCALE);
            
            chunk_num++;
          }
          
          if (justify && g_unichar_isspace(unichar) && whitespaces_count > 0)
          {
            /* increase spacing to fill all line width */
            if ((is_last_line_justify) || (line_num < line_count-1))
            {
              x += line_free_space/whitespaces_count;
              line_free_space -= line_free_space/whitespaces_count;
            }
            whitespaces_count--;
          }
          g_free(symbol);
        }
        
        x += gs->glyphs[0].geometry.width/PANGO_SCALE;
        
        gs->glyphs[0] = gi_old;
      }
    }
    
    pango_glyph_string_free(gs);
    
    baseline += PANGO_DESCENT(ink_rect) + area->spacing;
    line_num++;
  }
  
  //TODO: r_paragraph_line_free(&lines);
}

gint gtk_reader_area_paragraph_pos_to_offset(GtkReaderArea *area,
                                             GtkReaderParagraph *paragraph,
                                             gint pos)
{
  GSList *lines=NULL;
  GSList *it;
  gint    count, height;
  
  g_return_val_if_fail(area != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_AREA(area), 0);
  g_return_val_if_fail(paragraph != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), 0);

  gtk_reader_area_init_options(area, paragraph,
                               gtk_reader_area_get_column_width(area),
                               NULL);
  gtk_reader_paragraph_fill_layout(paragraph, area->pango_layout);
  gtk_reader_area_get_paragraph_lines(area, paragraph, &lines);
  g_assert(lines != NULL);

  count = 0;
  height = 0;
  for (it = lines; it != NULL; it = g_slist_next(it))
  {
    RParagraphLine *line=NULL;

    g_assert(it != NULL && it->data != NULL);
    line = (RParagraphLine*)it->data;
    count  += line->glyphs_count;
    if (pos < count)
      break;
    height += line->line_height + area->spacing;
  }

  //TODO:r_paragraph_line_free(&lines);

  return height;
}

void gtk_reader_area_redraw(GtkReaderArea *reader_area)
{
  GtkWidget *widget;
  GSList    *it;
  gint       y_pos, column_height;
  guint      column_width, column_num,
    column_count, column_spacing;

  g_return_if_fail(reader_area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(reader_area));

  if (reader_area->pixmap == NULL)
    return;
  
  widget = GTK_WIDGET(reader_area);

  column_height = widget->allocation.height;
  
  // clear view port
  gdk_draw_rectangle(reader_area->pixmap,
                     widget->style->white_gc,
                     TRUE,
                     0, 0,
                     widget->allocation.width,
                     widget->allocation.height);

  if (reader_area->buffers == NULL || g_hash_table_size(reader_area->buffers) == 0)
    return;

  column_width   = gtk_reader_area_get_column_width(reader_area);
  column_spacing = gtk_reader_area_get_column_spacing(reader_area);
  column_count = gtk_reader_area_get_column_count(reader_area);
  column_num   = 0;
  y_pos        = -reader_area->pos_offset;
  it = g_slist_nth(gtk_reader_area_get_cur_buffer(reader_area)->paragraphs, reader_area->pos_paragraph);
  while (it != NULL && y_pos < column_height && column_num < column_count)
  {
    GtkReaderParagraph *paragraph;
    guint width, height;
    GSList *lines=NULL;

    g_assert(it != NULL && it->data != NULL);
    paragraph = GTK_READER_PARAGRAPH(it->data);
    
    gtk_reader_area_draw_paragraph(
      reader_area,
      reader_area->pixmap,
      paragraph,
      (column_width+column_spacing)*column_num,
      y_pos,
      column_width,
      column_height,
      &width, &height,
      &lines);
      
    if (y_pos + height >= column_height)
    {
      GSList *lit;
      guint k;
      guint h=0;

      column_num++;
      lit = lines;
      while (lit != NULL)
      {
        k = GPOINTER_TO_UINT(lit->data);
        if (y_pos + h + k >= column_height)
          break;
        h += k + reader_area->spacing;
        lit = g_slist_next(lit);
      }
      y_pos = -(h + column_height - y_pos - h);
    }
    else
    {
      y_pos += height;
      it = g_slist_next(it);
    }
  }
}

void gtk_reader_area_set_column_count(GtkReaderArea *reader_area,
                                      guint count)
{
  g_return_if_fail(reader_area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(reader_area));

  reader_area->column_count = count;

  gtk_reader_area_update_indexer(reader_area);

  gtk_reader_area_redraw(reader_area);
}

guint gtk_reader_area_get_column_count(GtkReaderArea *reader_area)
{
  g_return_val_if_fail(reader_area != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_AREA(reader_area), 0);

  return reader_area->column_count;
}

guint gtk_reader_area_get_column_width(GtkReaderArea *reader_area)
{
  GtkWidget *widget;

  g_return_val_if_fail(reader_area != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_AREA(reader_area), 0);

  widget = GTK_WIDGET(reader_area);
  
  return widget->allocation.width/reader_area->column_count;
}

void gtk_reader_area_set_wrap(GtkReaderArea *reader_area,
                              PangoWrapMode  wrap)
{
  g_return_if_fail(reader_area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(reader_area));

  reader_area->wrap_mode = wrap;
}

PangoWrapMode gtk_reader_area_get_wrap(GtkReaderArea *reader_area)
{
  g_return_val_if_fail(reader_area != NULL, PANGO_WRAP_WORD);
  g_return_val_if_fail(GTK_IS_READER_AREA(reader_area), PANGO_WRAP_WORD);

  return reader_area->wrap_mode;
}

/* Sets default font (i.e. font for drawing plain text (<p>)
 */
void gtk_reader_area_set_default_font_from_string(GtkReaderArea *reader_area,
                                                  gchar *font_description)
{
  g_return_if_fail(reader_area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(reader_area));
  g_return_if_fail(font_description != NULL);

  if (reader_area->font)
  {
    g_object_unref(reader_area->font);
  }

  reader_area->font = pango_font_description_from_string(font_description);
  g_assert(reader_area->font != NULL);
}

void gtk_reader_area_update_indexer(GtkReaderArea *reader_area)
{
  GtkReaderBuffer *buffer;
  GSList *it;
  PangoRectangle ink_rect, logical_rect;

  g_return_if_fail(reader_area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(reader_area));

/*   if (reader_area->indexer.paragraphs) */
/*     g_slist_free(reader_area->indexer.paragraphs); */
/*   reader_area->indexer.paragraphs = NULL; */
  
  
/*   reader_area->indexer.height = 0; */

/*   g_assert(reader_area->buffers != NULL); */
/*   if (g_hash_table_size(reader_area->buffers) == 0) */
/*     return; */

/*   it = gtk_reader_area_get_cur_buffer(reader_area)->paragraphs; */
/*   for (; it != NULL; it = g_slist_next(it)) */
/*   { */
/*     GtkReaderParagraph *paragraph; */
/*     gint width, height; */
    
/*     g_assert(it != NULL && it->data != NULL); */
/*     paragraph = GTK_READER_PARAGRAPH(it->data); */

/*     height = gtk_reader_area_get_paragraph_height( */
/*       reader_area, */
/*       paragraph, */
/*       gtk_reader_area_get_column_width(reader_area)); */

/*     reader_area->indexer.height += height; */
/*     reader_area->indexer.paragraphs = g_slist_append(reader_area->indexer.paragraphs, */
/*                                                      GINT_TO_POINTER(height)); */
/*   } */

  buffer = gtk_reader_area_get_cur_buffer(reader_area);
  if (reader_area->adjustment && buffer)
  {
    reader_area->adjustment->upper = buffer->length;
    gtk_reader_area_adjustment_update(reader_area);
  }
}

void gtk_reader_area_set_spacing(GtkReaderArea *reader_area,
                                 gint spacing)
{
  g_return_if_fail(reader_area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(reader_area));

  reader_area->spacing = spacing;
}

gint gtk_reader_area_get_spacing(GtkReaderArea *reader_area)
{
  g_return_val_if_fail(reader_area != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_AREA(reader_area), 0);

  return reader_area->spacing;
}

void gtk_reader_area_set_column_spacing(GtkReaderArea *reader_area,
                                        guint spacing)
{
  g_return_if_fail(reader_area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(reader_area));

  reader_area->column_spacing = spacing;
}

guint gtk_reader_area_get_column_spacing(GtkReaderArea *reader_area)
{
  g_return_val_if_fail(reader_area != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_AREA(reader_area), 0);

  return reader_area->column_spacing;
}

void gtk_reader_area_set_display_partial(GtkReaderArea *reader_area,
                                         gboolean is_display)
{
  g_return_if_fail(reader_area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(reader_area));

  reader_area->display_partial = is_display;
}

gboolean gtk_reader_area_get_display_partial(GtkReaderArea *reader_area)
{
  g_return_val_if_fail(reader_area != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_READER_AREA(reader_area), FALSE);

  return reader_area->display_partial;
}

/**
 * Specifies height of empty line. Empty line is line with text length == 0.
 * @param area a #GtkReaderArea.
 * @param height a height of line.
 */
void gtk_reader_area_set_empty_line_height(GtkReaderArea *area,
                                           guint height)
{
  g_return_if_fail(area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(area));

  area->empty_line_height = height;
}

void gtk_reader_area_scroll_pixels(GtkReaderArea *area,
                                   gint value)
{
  gint v = value;
  gint  height;
  guint paragraphs_count;
  guint pos;
  GSList *it;
  GtkReaderBuffer *buffer;
  GtkReaderParagraph *paragraph;

  g_return_if_fail(area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(area));

  if (area->buffers == NULL || g_hash_table_size(area->buffers) == 0)
    return;

  buffer = gtk_reader_area_get_cur_buffer(area);
  g_assert(buffer != NULL);
  it = g_slist_nth(buffer->paragraphs, area->pos_paragraph);
  if (it == NULL || it->data == NULL)
  {
    /* this can be if GtkReaderBuffer is empty, i.e. document doesn't contain paragraphs */
    return;
  }

  paragraph = GTK_READER_PARAGRAPH(it->data);

  gtk_reader_area_init_options(area, paragraph,
                               gtk_reader_area_get_column_width(area),
                               NULL);
  gtk_reader_paragraph_fill_layout(paragraph, area->pango_layout);
  height = gtk_reader_area_get_paragraph_height(
      area,
      paragraph,
      gtk_reader_area_get_column_width(area));
  paragraphs_count = g_slist_length(buffer->paragraphs);

  if (value == 0)
  { /* update offset in characters */
    return;
  }
  else if (value > 0)
  { /* scrolling down */
    if (area->pos_offset + v < height)
    {
      area->pos_offset += v;
      v = 0;
    }
    else
    {
      while (v > 0 && area->pos_paragraph+1 < paragraphs_count)
      {
        if (area->pos_offset + v < height)
        {
          area->pos_offset += v;
          v = 0;
          break;
        }
        else
        {
          v -= height - area->pos_offset;
          area->pos_paragraph++;
          area->pos_offset = 0;
        }
        it = g_slist_next(it);
        g_assert(it != NULL);
        height = gtk_reader_area_get_paragraph_height(
          area,
          GTK_READER_PARAGRAPH(it->data),
          gtk_reader_area_get_column_width(area));
      }
    }
    v = value - v;
  }
  else
  { /* scroll up */
    v = -v;
    if (area->pos_offset >= v)
    {
      area->pos_offset -= v;
      v = value;
    }
    else if (area->pos_paragraph == 0)
    {
      v = -area->pos_offset;
      area->pos_offset = 0;
    }
    else
    {
      v -= area->pos_offset;
      area->pos_offset = 0;
      if (area->pos_paragraph > 0)
        area->pos_paragraph--;
      while (v > 0 && area->pos_paragraph >= 0)
      {
        it = g_slist_nth(gtk_reader_area_get_cur_buffer(area)->paragraphs, area->pos_paragraph);
        height = gtk_reader_area_get_paragraph_height(
          area,
          GTK_READER_PARAGRAPH(it->data),
          gtk_reader_area_get_column_width(area));

        if (v <= height)
        {
          area->pos_offset = height - v;
          v = value;
          break;
        }
        else
        {
          if (area->pos_paragraph == 0)
          {
            area->pos_offset = 0;
            break;
          }
          v -= height;
          area->pos_paragraph--;
          area->pos_offset = height;
        }
      }
    }
  }
  
  /* send adjustment signal */

  pos = gtk_reader_area_get_char_position(area);
  v   = 0;
  
  it = buffer->paragraphs;
  while (it != NULL && v < area->pos_paragraph)
  {
    GtkReaderParagraph *paragraph;

    g_assert(it != NULL && it->data != NULL);

    paragraph = GTK_READER_PARAGRAPH(it->data);
    pos += paragraph->length;

    it = g_slist_next(it);
    v++;
  }
  
  area->adjustment->value = pos;
  if (area->adjustment->value < area->adjustment->lower)
    area->adjustment->value = area->adjustment->lower;
  if (area->adjustment->value > area->adjustment->upper)
    area->adjustment->value = area->adjustment->upper;
  g_signal_handlers_block_by_func(GTK_OBJECT(area->adjustment),
                                  GTK_SIGNAL_FUNC(gtk_reader_area_adjustment_value_changed),
                                  (gpointer)area);
  gtk_adjustment_value_changed(area->adjustment);
  g_signal_handlers_unblock_by_func(GTK_OBJECT(area->adjustment),
                                    GTK_SIGNAL_FUNC(gtk_reader_area_adjustment_value_changed),
                                    (gpointer)area);

  area->is_modified = TRUE;
}

void gtk_reader_area_set_position(GtkReaderArea *area,
                                  guint          paragraph_num,
                                  gint           paragraph_offset)
{
  GSList *it;
  GtkReaderParagraph *paragraph;
  gint height;
  
  g_return_if_fail(area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(area));

  /* checking if arguments are acceptable */
  
  if (paragraph_num >= g_slist_length(gtk_reader_area_get_cur_buffer(area)->paragraphs))
    return;

  it = g_slist_nth(gtk_reader_area_get_cur_buffer(area)->paragraphs, paragraph_num);
  g_assert(it != NULL && it->data != NULL);
  paragraph = GTK_READER_PARAGRAPH(it->data);
  
  height = gtk_reader_area_get_paragraph_height(
    area,
    paragraph,
    gtk_reader_area_get_column_width(area));
  
  if (paragraph_offset > height)
    return;
  
  area->pos_paragraph = paragraph_num;
  area->pos_offset    = paragraph_offset;

  area->is_modified = TRUE;
}

void gtk_reader_area_set_position_absolute(GtkReaderArea *area,
                                           guint offset)
{
  GtkReaderParagraph *paragraph;
  GSList *it;
  guint offs = offset;
  
  g_return_if_fail(area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(area));
  
  if (area->buffers == NULL)
    return;
  
  g_return_if_fail(gtk_reader_area_get_cur_buffer(area)->paragraphs != NULL);
  
  it = g_slist_nth(gtk_reader_area_get_cur_buffer(area)->paragraphs, 0);
  g_assert(it != NULL && it->data != NULL);
  paragraph = GTK_READER_PARAGRAPH(it->data);

  area->pos_paragraph = 0;
  area->pos_offset    = 0;
  
  while (offs > 0)
  {
    guint height;
    
    height = gtk_reader_area_get_paragraph_height(
      area,
      paragraph,
      gtk_reader_area_get_column_width(area));

    if (offs >= height)
    {
      offs -= height;
      if (area->pos_paragraph+1 < g_slist_length(gtk_reader_area_get_cur_buffer(area)->paragraphs))
      {
        area->pos_paragraph++;
        it = g_slist_nth(gtk_reader_area_get_cur_buffer(area)->paragraphs, area->pos_paragraph);
        g_assert(it != NULL && it->data != NULL);
        paragraph = GTK_READER_PARAGRAPH(it->data);
      }
      else
      {
        area->pos_offset = height;
        offs = 0;
      }
    }
    else
    {
      area->pos_offset = offs;
      offs = 0;
    }
  }

  area->is_modified = TRUE;
}

/**
 * Returns height of paragraph in pixels.
 * @param area a #GtkReaderArea.
 * @param paragraph a #GtkReaderParagraph.
 * @param width specifies what paragraph width to use when calculating height.
 * @return height of paragraph.
 */
guint gtk_reader_area_get_paragraph_height(GtkReaderArea *area,
                                           GtkReaderParagraph *paragraph,
                                           guint width)
{
  guint result_height=0;
  guint indent=0, i=0, linecnt;
  
  g_return_val_if_fail(area != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_AREA(area), 0);
  g_return_val_if_fail(paragraph != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_PARAGRAPH(paragraph), 0);

  if (paragraph->text == NULL)
    return 0;

  if (paragraph->text->len == 0)
    return area->empty_line_height;
  
  /* initialize option for this paragraph */
  gtk_reader_area_init_options(area, paragraph, width, &indent);

  /* fill layout with attributed text */
  gtk_reader_paragraph_fill_layout(paragraph, area->pango_layout);

  linecnt = pango_layout_get_line_count(area->pango_layout);
  for (i = 0; i < linecnt; i++)
  {
    PangoLayoutLine *pll;
    PangoRectangle   ink_rect;

    pll = pango_layout_get_line(area->pango_layout, i);
    pango_layout_line_get_pixel_extents(pll, &ink_rect, NULL);
    result_height += PANGO_ASCENT(ink_rect) + PANGO_DESCENT(ink_rect);

  }
  result_height += linecnt * area->spacing;
  
  return result_height;
}

GtkReaderBuffer* gtk_reader_area_get_cur_buffer(GtkReaderArea *area)
{
  gpointer v;

  g_return_val_if_fail(area != NULL, NULL);
  g_return_val_if_fail(GTK_IS_READER_AREA(area), NULL);

  if (area->buffers == NULL || area->current_buffer == NULL)
    return NULL;

  v = g_hash_table_lookup(area->buffers, area->current_buffer);
  g_assert(v != NULL);
  
  return GTK_READER_BUFFER(v);
}

/**
 * Sets current buffer.
 * @param area a #GtkReaderArea.
 * @param buffer a pointer to buffer to make current. Buffer must
 * already added #gtk_reader_area_add_buffer or #gtk_reader_area_set_buffers.
 * @todo implement.
 */
void gtk_reader_area_set_current_buffer(GtkReaderArea   *area,
                                        GtkReaderBuffer *buffer)
{
  g_return_if_fail(area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(area));
  g_return_if_fail(buffer != NULL);
  g_return_if_fail(GTK_IS_READER_BUFFER(buffer));

  /* TODO: */
}

/**
 * Sets current buffer.
 * @param area a #GtkReaderArea.
 * @param name a name of the buffer to make current.
 */
void gtk_reader_area_set_current_buffer_by_name(GtkReaderArea *area,
                                                const gchar   *name)
{
  GSList *names;
  GSList *it;
  
  g_return_if_fail(area != NULL);
  g_return_if_fail(GTK_IS_READER_AREA(area));
  g_return_if_fail(name != NULL);

  names = gtk_reader_area_get_buffer_names(area);
  if (area->buffers == NULL || names == NULL)
    return;

  for (it = names; it != NULL; it = g_slist_next(it))
  {
    g_assert(it != NULL && it->data != NULL);
    if (!g_utf8_collate((const gchar*)it->data, name))
      break;
  }
  g_slist_free(names); names = NULL;

  if (it == NULL)
  {
    /* no such buffer */
    g_warning("Trying to select non-existing buffer (%s)\n", name);
    return;
  }
  
  if (area->current_buffer != NULL)
  {
    g_free(area->current_buffer);
  }

  area->current_buffer = g_strdup(name);

  gtk_reader_area_update_indexer(area);

  area->is_modified = TRUE;
}

const gchar* gtk_reader_area_get_current_buffer(GtkReaderArea *area)
{
  g_return_val_if_fail(area != NULL, NULL);
  g_return_val_if_fail(GTK_IS_READER_AREA(area), NULL);

  return area->current_buffer;
}

void hash_table_get_keys_func(gpointer key,
                              gpointer value,
                              gpointer user_data)
{
  GSList *list;

  g_return_if_fail(key != NULL && value != NULL);
  g_return_if_fail(user_data != NULL);

  list = *(GSList**)user_data;
  list = g_slist_append(list, key);
  *(GSList**)user_data = list;
}

GSList* gtk_reader_area_get_buffer_names(GtkReaderArea *area)
{
  GSList *list=NULL;

  g_return_val_if_fail(area != NULL, NULL);
  g_return_val_if_fail(GTK_IS_READER_AREA(area), NULL);

  g_hash_table_foreach(area->buffers, hash_table_get_keys_func, &list);

  return list;
}

/**
 * Gets position of first visible character in current paragraph
 * (i.e. in first visible paragraph).
 * @param area a GtkReaderArea.
 * @return position in characters of first visible character in
 * paragraph.
 */
gulong gtk_reader_area_get_char_position(GtkReaderArea *area)
{
  GtkReaderBuffer    *buffer;
  GtkReaderParagraph *paragraph;
  GSList *lines;
  GSList *it;
  guint   voffset, pos;
  
  g_return_val_if_fail(area != NULL, 0);
  g_return_val_if_fail(GTK_IS_READER_AREA(area), 0);

  buffer = gtk_reader_area_get_cur_buffer(area);
  g_assert(buffer != NULL);
  it = g_slist_nth(buffer->paragraphs, area->pos_paragraph);
  g_assert(it != NULL && it->data != NULL);
  paragraph = GTK_READER_PARAGRAPH(it->data);

  lines = NULL;
  gtk_reader_area_get_paragraph_lines(area, paragraph, &lines);
  if (lines == NULL)
    return 0;

  voffset = 0; pos = 0;
  for (it = lines; it != NULL; it = g_slist_next(it))
  {
    RParagraphLine *line;
    PangoRectangle  ink_rect;
    
    g_assert(it != NULL && it->data != NULL);
    line = (RParagraphLine*)it->data;
    pango_layout_line_get_pixel_extents(line->line, &ink_rect, NULL);
    
    voffset += PANGO_ASCENT(ink_rect) + PANGO_DESCENT(ink_rect);
    if (voffset > area->pos_offset)
    {
      return pos;
    }
    pos += line->glyphs_count;
  }

  return pos;
}

//---------------------------------------------------------------------------
RParagraphLine* r_paragraph_line_new()
{
  RParagraphLine *line;

  line = g_malloc0(sizeof(RParagraphLine));
  return line;
}
void r_paragraph_line_free(RParagraphLine **line)
{
  if (line == NULL || *line == NULL)
    return;
  pango_layout_line_unref((*line)->line);
  g_free(*line);
  *line = NULL;
}
//---------------------------------------------------------------------------

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
