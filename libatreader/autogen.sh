#!/bin/sh
#
# autogen.sh glue for CMU Cyrus IMAP
# $Id$
#
# Requires: automake, autoconf, dpkg-dev
set -e

# Refresh GNU autotools toolchain.
aclocal
autoheader
libtoolize --force --copy
automake --verbose --foreign --add-missing --copy
autoconf

exit 0
