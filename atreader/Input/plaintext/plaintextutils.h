#ifndef PLAINTEXTUTILS_H
#define PLAINTEXTUTILS_H

#include <glib.h>


gboolean plaintext_string_in_stringv(const gchar **listarr, const gchar *element);
gchar**  plaintext_list_to_stringv(GSList *string_list);
GStringChunk* plaintext_get_encoding_list(GList **list);


#endif

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
