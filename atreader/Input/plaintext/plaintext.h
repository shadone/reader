#ifndef PLAINTEXT_H
#define PLAINTEXT_H

#include <gmodule.h>
#include <libatreader/gtkreaderbuffer.h>
#include "atreader/rmoduleinfo.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


G_MODULE_IMPORT RModuleInfo* get_info(void);
G_MODULE_IMPORT void set_properties(GHashTable *props);
G_MODULE_IMPORT void get_properties(GHashTable **props);
G_MODULE_IMPORT gboolean open_file(GtkWidget *toplevel, const gchar *filename, GSList **buffers);
G_MODULE_IMPORT gboolean configure(GtkDialog *dialog, gpointer *user_data);
G_MODULE_IMPORT gboolean configure_get_data(GtkDialog *dialog, gpointer user_data);
G_MODULE_IMPORT GtkWindow *show_about(GtkWindow *parent_window);

  
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
