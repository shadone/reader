#include <glib.h>
#include "plaintextutils.h"

gboolean plaintext_string_in_stringv(const gchar **listarr, const gchar *element)
{
  gint i;

  i = 0;
  while (listarr[i] != NULL)
  {
    if (!g_utf8_collate(listarr[i], element))
      return TRUE;
    i++;
  }
  return FALSE;
}

gchar** plaintext_list_to_stringv(GSList *string_list)
{
  GSList *it;
  gchar **resval=NULL;
  gint    i;

  if (string_list == NULL)
    return NULL;

  resval = g_malloc0(sizeof(gchar*) * ( g_slist_length(string_list) + 1 ));
  for (i=0, it = string_list; it != NULL; it = g_slist_next(it),i++)
  {
    g_assert(it != NULL && it->data != NULL);
    resval[i] = g_strdup(it->data);
  }
  // make array of strings NULL-terminated
  resval[i] = NULL;

  return resval;
}

GStringChunk* plaintext_get_encoding_list(GList **list)
{
  GStringChunk *strings;
  
  g_return_val_if_fail(list != NULL, NULL);

  strings = g_string_chunk_new(2);
  
  /* TODO: get list of available encodings */
  
  *list = g_list_append(*list,
                        g_string_chunk_insert(
                          strings,
                          g_strdup("koi8-r")));
  *list = g_list_append(*list,
                        g_string_chunk_insert(
                          strings,
                          g_strdup("cp1251")));
  *list = g_list_append(*list,
                        g_string_chunk_insert(
                          strings,
                          g_strdup("utf-8")));

  return strings;
}


/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
