#include <gtk/gtk.h>
#include <gmodule.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <libatreader/gtkreaderbuffer.h>
#include "atreader/rconfigvalue.h"
#include "atreader/rmoduleinfo.h"
#include "plaintextutils.h"
#include "plaintext.h"

/* Forward declarations */

#define MAX_BUF 2048

/* TODO: we should somewhere free all allocated properties. */
typedef struct _ModuleProperties ModuleProperties;
struct _ModuleProperties
{
  gchar    *default_encoding;
  gboolean  is_ask_encoding;
  gchar   **encodings;
  guint     indent;
};

typedef struct _ConfigureUserData ConfigureUserData;
struct _ConfigureUserData
{
  GtkWidget *ask_encoding_btn;
  GtkWidget *encodings_tree_view;
  GtkWidget *indent_entry;
  GtkWidget *encoding_combo;
};

ModuleProperties properties;

GtkTreeModel* ui_create_and_fill_model(void);
GtkWidget* ui_create_view_and_model(void);
gboolean   plugin_ok_configure_func(GtkDialog *dialog, gpointer user_data);

/* Implementation */


guint get_interface_version(void)
{
  return MODULE_VERSION_1_0;
}

G_MODULE_EXPORT void module_init(void)
{
  // set default property values
  memset(&properties, 0, sizeof(ModuleProperties));
}

G_MODULE_EXPORT RModuleInfo* get_info(void)
{
  RModuleInfo *mi;
  /* NULL-terminated array of supported extensions */
  gchar *extensions[] = {".txt", NULL};
  
  mi = r_module_info_new_with_value(
    "libplaintext",
    extensions,
    "Author: Denis Dzyubenko");
  g_assert(mi != NULL);
  
  return mi;
}

G_MODULE_EXPORT void set_properties(GHashTable *props)
{
  RConfigValue *cv;
  
  g_return_if_fail(props != NULL);

  cv = g_hash_table_lookup(props, "encoding");
  if (cv == NULL)
  {
    cv = r_config_value_new_with_value(CVT_STRING, "koi8-r");
    g_hash_table_insert(props, g_strdup("encoding"), cv);
  }
  if (properties.default_encoding)
    g_free(properties.default_encoding);
  properties.default_encoding = g_strdup(r_config_value_get_value_with_type(cv, CVT_STRING));

  cv = g_hash_table_lookup(props, "ask_encoding");
  if (cv == NULL)
  {
    cv = r_config_value_new_with_value(CVT_BOOL, GINT_TO_POINTER(TRUE));
    g_hash_table_insert(props, g_strdup("ask_encoding"), cv);
  }
  properties.is_ask_encoding = (gboolean)r_config_value_get_value_with_type(cv, CVT_BOOL);
  
  cv = g_hash_table_lookup(props, "encodings");
  if (cv == NULL)
  {
    gchar *buf[] = {"koi8-r", "cp1251", NULL};
    
    cv = r_config_value_new_with_value(CVT_STRING|CVT_ARRAY, buf);
    g_hash_table_insert(props, g_strdup("encodings"), cv);
  }
  if (properties.encodings)
    g_strfreev(properties.encodings);
  properties.encodings = g_strdupv(r_config_value_get_value_with_type(cv, CVT_STRING|CVT_ARRAY));

  cv = g_hash_table_lookup(props, "indent");
  if (cv == NULL)
  {
    cv = r_config_value_new_with_value(CVT_INT, GINT_TO_POINTER(10));
    g_hash_table_insert(props, g_strdup("indent"), cv);
  }
  properties.indent = GPOINTER_TO_INT(r_config_value_get_value_with_type(cv, CVT_INT));
}

G_MODULE_EXPORT void get_properties(GHashTable **props)
{
  RConfigValue *cv;
  
  g_return_if_fail(props != NULL);
  g_return_if_fail(*props != NULL);

  cv = r_config_value_new_with_value(CVT_STRING,
                                     properties.default_encoding);
  g_hash_table_insert(*props, g_strdup("encoding"), cv);

  cv = r_config_value_new_with_value(CVT_BOOL,
                                     GINT_TO_POINTER(properties.is_ask_encoding));
  g_hash_table_insert(*props, g_strdup("ask_encoding"), cv);

  cv = r_config_value_new_with_value(CVT_STRING|CVT_ARRAY,
                                     properties.encodings);
  g_hash_table_insert(*props, g_strdup("encodings"), cv);
  
  cv = r_config_value_new_with_value(CVT_INT,
                                     GINT_TO_POINTER(properties.indent));
  g_hash_table_insert(*props, g_strdup("indent"), cv);
}

G_MODULE_EXPORT gboolean open_file(GtkWidget *toplevel, const gchar *filename, GSList **buffers)
{
  GtkReaderBuffer *buffer;
  GtkWidget       *dialog;
  gint result;
  gchar *encoding=NULL;
  FILE *fd;
  gchar buf[MAX_BUF];

  g_return_val_if_fail(toplevel != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_WINDOW(toplevel), FALSE);
  g_return_val_if_fail(filename != NULL, FALSE);
  g_return_val_if_fail(buffers != NULL, FALSE);

  if (properties.is_ask_encoding)
  {
    GtkWidget *view;

    dialog = gtk_dialog_new_with_buttons(
      "Choose encoding:", GTK_WINDOW(toplevel),
      GTK_DIALOG_MODAL,
      GTK_STOCK_OK, GTK_RESPONSE_OK,
      NULL);
    g_assert(dialog != NULL);

    /* fill dialog with widgets */
    view = ui_create_view_and_model();
    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), view);
    gtk_widget_show(view);
    
    result = gtk_dialog_run(GTK_DIALOG(dialog));
    if (result == GTK_RESPONSE_OK)
    {
      GtkTreePath *path;
      gint *indices;
      GList *it;
      GList  *list;
      GStringChunk *strings;

      gtk_tree_view_get_cursor(GTK_TREE_VIEW(view), &path, NULL);
      indices = gtk_tree_path_get_indices(path);
      list = NULL;
      strings = plaintext_get_encoding_list(&list);
      g_assert(indices[0] < g_list_length(list));
      it = g_list_nth(list, indices[0]);
      g_assert(it != NULL && it->data != NULL);
      encoding = g_strdup((gchar*)it->data);
      g_string_chunk_free(strings);
      g_list_free(list);
    }
    gtk_widget_destroy(dialog);
    if (result != GTK_RESPONSE_OK)
      return FALSE;
  }
  else
  {
    encoding = g_strdup(properties.default_encoding);
  }

  g_assert(encoding != NULL);

  /* open file */

  fd = fopen(filename, "r");
  if (fd == NULL)
  {
    /* TODO: handle file open errors */
    g_free(encoding);
    return FALSE;
  }

  buffer = gtk_reader_buffer_new_with_name("Main");
  g_assert(buffer != NULL);
  
  while (fgets(buf, MAX_BUF, fd))
  {
    GtkReaderParagraph *paragraph;

    if (strlen(buf) > 0 && buf[strlen(buf)-1] == '\n')
      buf[strlen(buf)-1] = '\0';
    paragraph = gtk_reader_paragraph_new();
    gtk_reader_paragraph_set_indent(paragraph, GTK_READER_PARAGRAPH_VALUE_PIXELS, properties.indent);
    gtk_reader_paragraph_set_align(paragraph, PANGO_ALIGN_LEFT);
    gtk_reader_paragraph_set_valign(paragraph, GTK_READER_PARAGRAPH_VALIGN_BOTTOM);
    gtk_reader_paragraph_set_justify(paragraph, FALSE);
    paragraph->text = g_string_new(g_convert(buf, -1, "utf-8", encoding,
                                             NULL, NULL, NULL));
    /* TODO: handle convert errors */
    gtk_reader_buffer_append_paragraph(buffer, paragraph);
  }

  *buffers = g_slist_append(*buffers, buffer);

  g_free(encoding);
  fclose(fd);
  
  return TRUE;
}

G_MODULE_EXPORT gboolean configure(GtkDialog *dialog, gpointer *user_data)
{
  GtkWidget *hbox, *vbox, *frame1, *frame2;
  GtkWidget           *label;
  GtkCellRenderer     *renderer;
  GtkTreeModel        *model;
  GtkTreeSelection    *selection;
  GtkTreeIter          iter;
  gboolean             valid;
  gchar               *buf;
  ConfigureUserData   *cud;
  GList               *list;
  GStringChunk        *strings;

  g_return_val_if_fail(dialog != NULL, FALSE);
  
  g_assert(user_data != NULL);

  cud = g_malloc0(sizeof(ConfigureUserData));
  g_assert(cud != NULL);

  hbox = gtk_hbox_new(TRUE, 2);
  gtk_container_add(GTK_CONTAINER(dialog->vbox), GTK_WIDGET(hbox));
  gtk_widget_show(hbox);
  
  frame1 = gtk_frame_new("Encodings");
  gtk_box_pack_start(GTK_BOX(hbox), frame1, TRUE, TRUE, 0);
  gtk_widget_show(frame1);

  frame2 = gtk_frame_new("Misc");
  gtk_box_pack_start(GTK_BOX(hbox), frame2, TRUE, TRUE, 0);
  gtk_widget_show(frame2);

  vbox = gtk_vbox_new(FALSE, 2);
  gtk_container_add(GTK_CONTAINER(frame1), GTK_WIDGET(vbox));
  gtk_widget_show(vbox);
  
  cud->ask_encoding_btn = gtk_check_button_new_with_label("Ask file encoding on open");
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cud->ask_encoding_btn), properties.is_ask_encoding);
  gtk_box_pack_start(GTK_BOX(vbox), cud->ask_encoding_btn, FALSE, FALSE, 0);
  gtk_widget_show(cud->ask_encoding_btn);

  // listbox with encodings
  cud->encodings_tree_view = gtk_tree_view_new();
  /* column */
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(cud->encodings_tree_view),
                                              -1,
                                              "Available encodings",
                                              renderer,
                                              "text", 0,
                                              NULL);
  model = ui_create_and_fill_model();
  gtk_tree_view_set_model(GTK_TREE_VIEW(cud->encodings_tree_view), model);
  g_object_unref(model); /* destroy model automatically with view */
  selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(cud->encodings_tree_view));
  gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
  
  /* select items which are specified in configuration */
  model = gtk_tree_view_get_model(GTK_TREE_VIEW(cud->encodings_tree_view));
  g_assert(model != NULL);
  valid = gtk_tree_model_get_iter_first(model, &iter);
  while (valid)
  {
    gchar *str_data=NULL;
    
    gtk_tree_model_get(model, &iter,
                       0, &str_data,
                       -1);

    if (plaintext_string_in_stringv((const gchar**)properties.encodings, str_data))
    {
      GtkTreePath *path;
      
      path = gtk_tree_model_get_path(model, &iter);
      g_assert(path != NULL);

      gtk_tree_selection_select_path(selection, path);
      gtk_tree_path_free(path);
    }

    g_free(str_data);
    
    valid = gtk_tree_model_iter_next(model, &iter);
  }
  // pack list view into vbox on frame1
  gtk_box_pack_start(GTK_BOX(vbox), cud->encodings_tree_view, TRUE, TRUE, 0);
  gtk_widget_show(cud->encodings_tree_view);

  cud->encoding_combo = gtk_combo_new();
  list = NULL;
  strings = plaintext_get_encoding_list(&list);
  gtk_combo_set_popdown_strings(GTK_COMBO(cud->encoding_combo), list);
  g_string_chunk_free(strings);
  g_list_free(list);
  
  gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(cud->encoding_combo)->entry), properties.default_encoding);
  gtk_entry_set_editable(GTK_ENTRY(GTK_COMBO(cud->encoding_combo)->entry), FALSE);
  gtk_box_pack_start(GTK_BOX(vbox), cud->encoding_combo, TRUE, TRUE, 0);
  gtk_widget_show(cud->encoding_combo);
  
  // frame2
  vbox = gtk_vbox_new(FALSE, 2);
  gtk_container_add(GTK_CONTAINER(frame2), GTK_WIDGET(vbox));
  gtk_widget_show(vbox);

  label = gtk_label_new("Indent");
  gtk_misc_set_alignment(GTK_MISC(label), 0, 0);
  gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
  gtk_widget_show(label);

  cud->indent_entry = gtk_entry_new();
  buf = g_strdup_printf("%d", properties.indent);
  gtk_entry_set_text(GTK_ENTRY(cud->indent_entry), buf);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), cud->indent_entry);
  g_free(buf);
  gtk_box_pack_start(GTK_BOX(vbox), cud->indent_entry, FALSE, FALSE, 0);
  gtk_widget_show(cud->indent_entry);

  /* fill 'user_data' args */
  *user_data = (gpointer)cud;
  
  return TRUE;
}

G_MODULE_EXPORT gboolean configure_get_data(GtkDialog *dialog, gpointer user_data)
{
  ConfigureUserData *cud;
  GtkTreeModel      *model;
  GtkTreeSelection  *selection;
  GtkTreeIter        iter;
  gboolean           valid;
  GSList            *list=NULL;
  GStringChunk      *strings;

  g_assert(dialog != NULL);
  g_assert(user_data != NULL);
  
  cud = (ConfigureUserData*)user_data;
  properties.is_ask_encoding = gtk_toggle_button_get_active(
    GTK_TOGGLE_BUTTON(cud->ask_encoding_btn));

  properties.indent = atoi(gtk_entry_get_text(GTK_ENTRY(cud->indent_entry)));

  g_assert(properties.encodings != NULL);
  g_strfreev(properties.encodings);
  properties.encodings = NULL;
  selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(cud->encodings_tree_view));
  model = gtk_tree_view_get_model(GTK_TREE_VIEW(cud->encodings_tree_view));
  valid = gtk_tree_model_get_iter_first(model, &iter);
  strings = g_string_chunk_new(2);
  while (valid)
  {
    if (gtk_tree_selection_iter_is_selected(selection, &iter))
    {
      gchar *str_data=NULL;
      
      gtk_tree_model_get(model, &iter,
                         0, &str_data,
                         -1);

      list = g_slist_append(list, (gpointer)g_string_chunk_insert(strings, str_data));
    }
    valid = gtk_tree_model_iter_next(model, &iter);
  }
  properties.encodings = plaintext_list_to_stringv(list);
  g_string_chunk_free(strings);
  strings = NULL;

  /* set 'encoding' property */
  if (properties.default_encoding)
    g_free(properties.default_encoding);
  properties.default_encoding = g_strdup(
    gtk_entry_get_text(
      GTK_ENTRY(GTK_COMBO(cud->encoding_combo)->entry)));
  
  // finally, free user_data we received.
/*   g_free(user_data); */

  return TRUE;
}

G_MODULE_EXPORT GtkWindow *show_about(GtkWindow *parent_window)
{
  GtkWidget *dialog;

  dialog = gtk_message_dialog_new(parent_window,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_INFO,
                                  GTK_BUTTONS_OK,
                                  "Plaintext plugin\n\nAuthor: Denis Dzyubenko <shad@novoross.ru>");
  g_signal_connect_swapped(G_OBJECT(dialog), "response",
                           G_CALLBACK(gtk_widget_destroy),
                           GTK_OBJECT(dialog));

  return GTK_WINDOW(dialog);
}

/* utility functions */

GtkTreeModel* ui_create_and_fill_model(void)
{ 
  GtkListStore  *store;
  GtkTreeIter    iter;
  GList         *list, *it;
  
  store = gtk_list_store_new(1, G_TYPE_STRING);

  /* Append rows and fill in some data */
  list = NULL;
  plaintext_get_encoding_list(&list);
  for (it = list; it != NULL; it = g_list_next(it))
  {
    g_assert(it != NULL && it->data != NULL);
    
    gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter,
                        0, (gchar*)it->data,
                        -1);
  }
  g_list_free(list);
  
  /* TODO: free memory used by encoding list when TreeView (and Model)
     is destroyed. I think this should be done in 'destroy' signal. */
  
  return GTK_TREE_MODEL (store);
}

GtkWidget* ui_create_view_and_model(void)
{
  GtkCellRenderer     *renderer;
  GtkTreeModel        *model;
  GtkWidget           *view;

  view = gtk_tree_view_new();

  /* column */
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(view),
                                              -1,
                                              "Encoding",
                                              renderer,
                                              "text", 0,
                                              NULL);

  model = ui_create_and_fill_model();

  gtk_tree_view_set_model(GTK_TREE_VIEW(view), model);

  g_object_unref(model); /* destroy model automatically with view */

  return view;
}


/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
