#include <gtk/gtk.h>
#include <gmodule.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <expat.h>
#if HAVE_SYS_STAT
#  include <sys/stat.h>
#endif
#if HAVE_SYS_TYPES
#  include <sys/types.h>
#endif
#include <fcntl.h>
#include "atreader/rconfigvalue.h"
#include "atreader/rmoduleinfo.h"
#include "fictionbook.h"

/* Forward declarations */

#define	FB2_NS      "http://www.gribuser.ru/xml/fictionbook/2.0"
#define	NS_SEP      "|"
#define BUF_SIZE    1024

GHashTable *properties;

typedef struct _FB2UserData FB2UserData;
struct _FB2UserData
{
  gboolean is_parse_error;      /**< whether parse error occured */
  gchar   *parse_error_string;  /**< text message describing parse error */
  
  gboolean in_fb;               /**< under FictionBook root element */
  gboolean in_binary;           /**< Binary element */
  gboolean in_paragraph;        /**< p element */
  gboolean in_v;                /**< in poem::stanza::v element */
  gboolean in_emphasis;         /**< in emphasis element */
  gboolean in_strong;           /**< in strong element */
  gboolean is_ptype;            /**< if current element is pType (i.e. paragraphaph like) */

  GSList *buffers;              /**< array of named buffers (#GtkReaderBuffer) */
  GtkReaderParagraph *paragraph;/**< current paragraph */
  GSList  *path;                /**< path to current element */
  gchar   *element;             /**< current element name (w/o path) */

  gchar   *text;                /**< text to store */
  guint    ptype_pos;           /**< start position (first character) of current style */
};

int              fb_handler_unknown_encoding    (gpointer        encodingHandlerData,
                                                 const XML_Char *name,
                                                 XML_Encoding   *info);
void             fb_handler_start_element       (gpointer        userData,
                                                 const XML_Char *name_,
                                                 const XML_Char **atts);
void             fb_handler_end_element         (gpointer        userData,
                                                 const XML_Char *name_);
void             fb_handler_character_data      (gpointer        userData,
                                                 const XML_Char *text_,
                                                 gint            len);
GtkReaderBuffer* fb2_utils_get_buffer           (GSList     **buffers,
                                                 const gchar *name);
GtkReaderAttr*   fb_utils_set_paragraph_style   (GtkReaderParagraph *paragraph,
                                                 GSList             *path,
                                                 const gchar        *element);
void             fb2_utils_copy_hash_table      (gpointer key,
                                                 gpointer value,
                                                 gpointer userdata);


#define PARSE_ERROR(ud, str)                 \
  {                                          \
    ud->is_parse_error = TRUE;               \
    ud->parse_error_string = g_strdup(str);  \
    return;                                  \
  }

#define UNHANDLED() \
  g_warning("Unhandled element '%s'", name);

const gchar *complex_attrs[] = {
  "body",
  "title",
  "section",
  "epigraph",
  "poem",
  "cite",
  "stanza"
};
const int complex_attrs_size = sizeof(complex_attrs)/sizeof(complex_attrs[0]);

/* Implementation */


guint get_interface_version(void)
{
  return MODULE_VERSION_1_0;
}

G_MODULE_EXPORT void module_init(void)
{
  properties = g_hash_table_new_full(g_str_hash, g_str_equal,
                                     (GDestroyNotify*)g_free,
                                     (GDestroyNotify*)r_config_value_free);
  g_assert(properties != NULL);
}

G_MODULE_EXPORT RModuleInfo* get_info(void)
{
  RModuleInfo *mi;
  /* NULL-terminated array of supported extensions */
  gchar *extensions[] = {".fb2", NULL};
  
  mi = r_module_info_new_with_value(
    "libfictionbook",
    extensions,
    "Author: Denis Dzyubenko");
  g_assert(mi != NULL);
  
  return mi;
}

G_MODULE_EXPORT void set_properties(GHashTable *props)
{
  RConfigValue *cv;
  
  g_return_if_fail(props != NULL);

  g_hash_table_foreach(props, fb2_utils_copy_hash_table, properties);
}

G_MODULE_EXPORT void get_properties(GHashTable **props)
{
  RConfigValue *cv;
  
  g_return_if_fail(props != NULL);
  g_return_if_fail(*props != NULL);
  
  g_hash_table_foreach(properties, fb2_utils_copy_hash_table, props);
}

G_MODULE_EXPORT gboolean open_file(GtkWidget *toplevel, const gchar *filename, GSList **buffers)
{
  XML_Parser  parser;
  gboolean    ret = TRUE;
  gint        bytes_read;
  FB2UserData userdata;
  gint        fd;
  
  g_return_val_if_fail(toplevel != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_WINDOW(toplevel), FALSE);
  g_return_val_if_fail(filename != NULL, FALSE);
  g_return_val_if_fail(buffers != NULL, FALSE);

  fd = open(filename, O_RDONLY);
  if (fd < 0)
  {
    g_warning("Can't open file '%s'", filename);
    return FALSE;
  }
  
  parser = XML_ParserCreate_MM(NULL, NULL, NS_SEP);
  if (!parser)
  {
    g_warning("Can't create XML parser");
    return FALSE;
  }

  memset(&userdata, 0, sizeof(userdata));
  userdata.buffers = *buffers;

  XML_SetElementHandler(parser, fb_handler_start_element, fb_handler_end_element);
  XML_SetCharacterDataHandler(parser, fb_handler_character_data);
  XML_SetUnknownEncodingHandler(parser, fb_handler_unknown_encoding, NULL);
  XML_SetUserData(parser, &userdata);

  do
  {
    void *buf;

    buf = XML_GetBuffer(parser, BUF_SIZE);
    bytes_read = read(fd, buf, BUF_SIZE);
    if (bytes_read < 0)
    {
      g_warning("Error reading from file '%s'", filename);
      close(fd);
      fd = 0;
      ret = FALSE;
      break;
    }
    
    if (!XML_ParseBuffer(parser, bytes_read, bytes_read == 0))
    {
      enum XML_Error err;

      err = XML_GetErrorCode(parser);
      
      g_warning("XML parse error: %s at line %d, column %d",
                XML_ErrorString(err),
                XML_GetCurrentLineNumber(parser),
                XML_GetCurrentColumnNumber(parser));
      
      ret = FALSE;
      break;
    }
    if (userdata.is_parse_error)
    {
      /* user-defined parse error occured */
      /* TODO: handle error and print message to stdout */
      g_warning("parse error");
    }
  } while (bytes_read > 0);

  if (fd)
    close(fd);

  if (ret)
    *buffers = userdata.buffers;
  
  return ret;
}

G_MODULE_EXPORT gboolean configure(GtkDialog *dialog, gpointer *user_data)
{
  g_return_val_if_fail(dialog != NULL, FALSE);
  
  return FALSE;
}

G_MODULE_EXPORT gboolean configure_get_data(GtkDialog *dialog, gpointer user_data)
{
  g_assert(dialog != NULL);
  g_assert(user_data != NULL);
  
  return TRUE;
}

G_MODULE_EXPORT GtkWindow *show_about(GtkWindow *parent_window)
{
  GtkWidget *dialog;

  dialog = gtk_message_dialog_new(parent_window,
                                  GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_INFO,
                                  GTK_BUTTONS_OK,
                                  "Fictionbook plugin\n\nAuthor: Denis Dzyubenko <shad@novoross.ru>");
  g_signal_connect_swapped(G_OBJECT(dialog), "response",
                           G_CALLBACK(gtk_widget_destroy),
                           GTK_OBJECT(dialog));

  return GTK_WINDOW(dialog);
}

/* utility functions */

int fb_handler_unknown_encoding(gpointer        encodingHandlerData,
                                const XML_Char *name,
                                XML_Encoding   *info)
{
  return 0;
}

void fb_handler_start_element(gpointer         userData,
                              const XML_Char  *name_,
                              const XML_Char **atts)
{
  FB2UserData *ud;
  const gchar *name = (const gchar*)name_;
  guint        i;

  ud = (FB2UserData*)userData;

  if (ud->is_parse_error)
    return;
  
  if (!g_utf8_collate(name, FB2_NS NS_SEP "FictionBook"))
  {
    ud->in_fb = TRUE;
    return;
  }

  if (!ud->in_fb)
    PARSE_ERROR(ud, "Missed root element");

  for (i = 0; i < complex_attrs_size; i++)
  {
    gchar *buf;

    buf = g_strconcat(FB2_NS, NS_SEP, complex_attrs[i], NULL);
    if (!g_utf8_collate(name, buf))
    { 
      ud->path = g_slist_append(ud->path, g_strdup(complex_attrs[i]));
      if (ud->element)
      {
        /* just in case of non-valid fb2-document */
        g_free(ud->element);
        ud->element = NULL;
      }
      break;
    }
  }

  if (!g_utf8_collate(name, FB2_NS NS_SEP "stylesheet"))
  {
    UNHANDLED();
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "description"))
  {
    UNHANDLED();
  }
  
  if (!g_utf8_collate(name, FB2_NS NS_SEP "image"))
  {
    ud->element = g_strdup("image");
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "binary"))
  {
    ud->in_binary = TRUE;
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "p") ||
           !g_utf8_collate(name, FB2_NS NS_SEP "subtitle") ||
           !g_utf8_collate(name, FB2_NS NS_SEP "text-author"))
  {
    if (!g_utf8_collate(name, FB2_NS NS_SEP "p"))
    {
      ud->in_paragraph = TRUE;
      ud->element      = g_strdup("p");
    }
    else if (!g_utf8_collate(name, FB2_NS NS_SEP "subtitle"))
      ud->element = g_strdup("subtitle");
    else
      ud->element = g_strdup("text-author");
    
    ud->is_ptype     = TRUE;
    ud->ptype_pos    = 0;
    ud->paragraph    = gtk_reader_paragraph_new();
    g_assert(ud->paragraph != NULL);
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "v"))
  {
    ud->in_v      = TRUE;
    ud->is_ptype  = TRUE;
    ud->ptype_pos = 0;
    UNHANDLED();
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "empty-line") ||
           !g_utf8_collate(name, FB2_NS NS_SEP "title"))
  {
    GtkReaderBuffer    *buffer;
    GtkReaderParagraph *paragraph;

    buffer = fb2_utils_get_buffer(&ud->buffers, "main");
    
    paragraph = gtk_reader_paragraph_new(); /* by default paragraph is empty */
    gtk_reader_paragraph_set_text(paragraph, "");

    gtk_reader_buffer_append_paragraph(buffer, paragraph);
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "strong") ||
           !g_utf8_collate(name, FB2_NS NS_SEP "emphasis"))
  {
    if (!g_utf8_collate(name, FB2_NS NS_SEP "strong"))
      ud->in_strong = TRUE;
    else
      ud->in_emphasis = TRUE;
    
    if (ud->text)
    {
      GtkReaderAttr *rattr;

      /* setting paragraph style */
      /* ud->element is a parent element */
      rattr = fb_utils_set_paragraph_style(ud->paragraph, ud->path, ud->element);
      
      if (rattr)
      {
        GtkReaderChunk *chunk;

        chunk = gtk_reader_chunk_new();
        chunk->start_index = ud->ptype_pos;
        /* I use strlen to calc size of ud->text in bytes. */
        chunk->end_index = strlen(ud->text);
        gtk_reader_chunk_set_text_value(chunk, rattr);
        gtk_reader_paragraph_add_chunk(ud->paragraph, chunk);
      }
      ud->ptype_pos = strlen(ud->text);
    }
    else
      ud->ptype_pos = 0;
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "emphasis"))
  {
    ud->in_emphasis = TRUE;
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "a"))
  {
    UNHANDLED();
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "table"))
  {
    UNHANDLED();
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "tr"))
  {
    /* TODO: handle 'align'-attribute */
    UNHANDLED();
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "td"))
  {
    UNHANDLED();
  }
  else
    UNHANDLED();
}

void fb_handler_end_element(gpointer        userData,
                            const XML_Char *name_)
{ 
  FB2UserData *ud;
  guint        i;

  const gchar *name = (const gchar*)name_;
  ud = (FB2UserData*)userData;

  if (!g_utf8_collate(name, FB2_NS NS_SEP "FictionBook"))
  {
    ud->in_fb = FALSE;
    return;
  }

  for (i = 0; i < complex_attrs_size; i++)
  {
    gchar *buf;

    buf = g_strconcat(FB2_NS NS_SEP, complex_attrs[i], NULL);
    if (!g_utf8_collate(name, buf))
    {
      GSList *it;

      if (ud->path)
      {
        it = g_slist_last(ud->path);
        g_assert(it != NULL && it->data != NULL);
        g_free(it->data);
        ud->path = g_slist_remove_link(ud->path, it);
      }
      break;
    }
  }

  if (!g_utf8_collate(name, FB2_NS NS_SEP "stylesheet"))
  {
    UNHANDLED();
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "description"))
  {
    UNHANDLED();
  }
  
  if (!g_utf8_collate(name, FB2_NS NS_SEP "image"))
  {
    g_free(ud->element); ud->element = NULL;
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "binary"))
  {
    ud->in_binary = FALSE;
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "p") ||
           !g_utf8_collate(name, FB2_NS NS_SEP "subtitle") ||
           !g_utf8_collate(name, FB2_NS NS_SEP "text-author"))
  {
    GtkReaderBuffer *buffer;
    
    ud->in_paragraph = FALSE;
    ud->is_ptype     = FALSE;

    g_assert(ud->paragraph != NULL);

    if (ud->text)
    {
      GtkReaderAttr *rattr;

      /* setting paragraph style */
      rattr = fb_utils_set_paragraph_style(ud->paragraph, ud->path, ud->element);
      if (rattr)
      {
        GtkReaderChunk *chunk;

        chunk = gtk_reader_chunk_new();
        chunk->start_index = ud->ptype_pos;
        /* I use strlen to calc size of ud->text in bytes. */
        chunk->end_index = strlen(ud->text);
        gtk_reader_chunk_set_text_value(chunk, rattr);
        gtk_reader_paragraph_add_chunk(ud->paragraph, chunk);
      }

      gtk_reader_paragraph_append_text(ud->paragraph, ud->text);
    }

    buffer = fb2_utils_get_buffer(&ud->buffers, "main");
    gtk_reader_buffer_append_paragraph(buffer, ud->paragraph);
    ud->paragraph = NULL;
    
    g_free(ud->element); ud->element = NULL;
    g_free(ud->text);    ud->text    = NULL;
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "v"))
  {
    ud->in_v     = FALSE;
    ud->is_ptype = FALSE;
    UNHANDLED();
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "text-author"))
  {
    UNHANDLED();
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "strong") ||
           !g_utf8_collate(name, FB2_NS NS_SEP "emphasis"))
  {
    if (ud->text)
    {
      GtkReaderAttr *rattr;

      /* setting paragraph style */
      if (!g_utf8_collate(name, FB2_NS NS_SEP "strong"))
        rattr = fb_utils_set_paragraph_style(ud->paragraph, ud->path, "strong");
      else
        rattr = fb_utils_set_paragraph_style(ud->paragraph, ud->path, "emphasis");
      
      if (rattr)
      {
        GtkReaderChunk *chunk;

        chunk = gtk_reader_chunk_new();
        chunk->start_index = ud->ptype_pos;
        /* I use strlen to calc size of ud->text in bytes. */
        chunk->end_index = strlen(ud->text);
        gtk_reader_chunk_set_text_value(chunk, rattr);
        gtk_reader_paragraph_add_chunk(ud->paragraph, chunk);
      }
    }

    if (!g_utf8_collate(name, FB2_NS NS_SEP "strong"))
      ud->in_strong = FALSE;
    else
      ud->in_emphasis = FALSE;
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "a"))
  {
    UNHANDLED();
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "table"))
  {
    UNHANDLED();
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "tr"))
  {
    UNHANDLED();
  }
  else if (!g_utf8_collate(name, FB2_NS NS_SEP "td"))
  {
    UNHANDLED();
  }
}

void fb_handler_character_data(gpointer        userData,
                               const XML_Char *text_,
                               gint            len)
{
  FB2UserData *ud;
  gchar       *text;

  ud = (FB2UserData*)userData;

  if (len == 0 || text == NULL || text_[0] == 0)
    return;

  text = g_utf8_normalize(text_, len, G_NORMALIZE_ALL);
  g_strdelimit(text, "\n", ' '); /* replace all newlines with space */
  if (!g_utf8_collate(text, "\n") || text[0] == 0)
    return;
  
  if (ud->is_ptype)
  {
    if (ud->text)
    {
      gchar *buf;
      
      buf = g_strconcat(ud->text, text, NULL);
      g_free(ud->text);
      ud->text = buf;
    }
    else
    {
      ud->text = g_strdup(text);
    }
  }

  g_free(text);
}

/* utility functions */

/**
 * Gets buffer by name from the list (GSList).
 * If there we no such buffer, create a new one, so the function
 * should never return NULL.
 * @param buffers a list with #GtkReaderBuffer as values.
 * @param name a buffer name to get.
 * @return a #GtkReaderBuffer. */
GtkReaderBuffer* fb2_utils_get_buffer(GSList     **buffers,
                                      const gchar *name)
{
  GtkReaderBuffer *buffer=NULL;
  GSList  *it;
  gpointer v;

  g_assert(name != NULL);
  g_assert(buffers != NULL);
  
  for (it = *buffers; it != NULL; it = g_slist_next(it))
  {
    g_assert(it != NULL && it->data != NULL);
    
    buffer = GTK_READER_BUFFER(it->data);
    if (!g_utf8_collate(gtk_reader_buffer_get_name(buffer),
                        name))
      return buffer;
  }

  /* no such buffer, creating a new one */
  buffer = gtk_reader_buffer_new_with_name(name);
  g_assert(buffer != NULL);
  *buffers = g_slist_append(*buffers, buffer);
  
  return buffer;
}

/**
 * Iterates through configuration options and create attribute for
 * paragraph.
 * @param paragraph a #GtkReaderParagraph.
 * @param path a list of string which makes the path to element.
 * @param element an element name.
 * @return new attribute or NULL if there were no appropriate value in
 * configuration.
 */
GtkReaderAttr* fb_utils_set_paragraph_style(GtkReaderParagraph *paragraph,
                                            GSList             *path,
                                            const gchar        *element)
{
  GtkReaderAttr *attr=NULL;
  GSList *it;
  gchar  *pathstring = NULL;

  attr = gtk_reader_attr_new(element);
  
  for (it = path; it != NULL; it = g_slist_next(it))
  {
    RConfigValue *cv;
    gpointer      v;
    gchar        *elementpath;
    gchar        *option;
    
    g_assert(it != NULL && it->data != NULL);
    
    if (pathstring == NULL)
      pathstring = g_strdup(it->data);
    else
    {
      gchar *buf;
      buf = g_strconcat(pathstring, "::", it->data, NULL);
      g_free(pathstring);
      pathstring = buf;
    }

    elementpath = g_strconcat(pathstring, "::", element, NULL);

    /* indent */
    option = g_strconcat(elementpath, "::", "indent", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      guint indent;
      indent = GPOINTER_TO_UINT(r_config_value_get_value_with_type(cv, CVT_INT));
      gtk_reader_paragraph_set_indent(paragraph, GTK_READER_PARAGRAPH_VALUE_PIXELS,
                                      indent);
    }
    g_free(option);

    /* indent in percents */
    option = g_strconcat(elementpath, "::", "indentp", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      guint indentp;
      indentp = GPOINTER_TO_UINT(r_config_value_get_value_with_type(cv, CVT_INT));
      gtk_reader_paragraph_set_indent(paragraph, GTK_READER_PARAGRAPH_VALUE_PERCENTS,
                                      indentp);
    }
    g_free(option);

    /* horizontal offset */
    option = g_strconcat(elementpath, "::", "offset", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      guint offset;
      offset = GPOINTER_TO_UINT(r_config_value_get_value_with_type(cv, CVT_INT));
      gtk_reader_paragraph_set_offset(paragraph, GTK_READER_PARAGRAPH_VALUE_PIXELS,
                                      offset);
    }
    g_free(option);

    /* horizontal offset in percents */
    option = g_strconcat(elementpath, "::", "offsetp", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      guint offsetp;
      offsetp = GPOINTER_TO_UINT(r_config_value_get_value_with_type(cv, CVT_INT));
      gtk_reader_paragraph_set_offset(paragraph, GTK_READER_PARAGRAPH_VALUE_PERCENTS,
                                      offsetp);
    }
    g_free(option);

    /* font description */
    option = g_strconcat(elementpath, "::", "font", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      gchar *font;
      font = (gchar*)r_config_value_get_value_with_type(cv, CVT_STRING);
      gtk_reader_attr_set_font_family(attr, font);
    }
    g_free(option);

    /* font size */
    option = g_strconcat(elementpath, "::", "fontsize", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      guint fontsize;
      fontsize = GPOINTER_TO_UINT(r_config_value_get_value_with_type(cv, CVT_INT));
      gtk_reader_attr_set_size(attr, fontsize);
    }
    g_free(option);

    /* font size relative to base one */
    option = g_strconcat(elementpath, "::", "relfontsize", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      guint relfontsize;
      relfontsize = GPOINTER_TO_UINT(r_config_value_get_value_with_type(cv, CVT_INT));
      gtk_reader_attr_set_size_relative(attr, relfontsize);
    }
    g_free(option);
    
    /* foreground color */
    option = g_strconcat(elementpath, "::", "foreground", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      gchar *color;
      color = (gchar*)r_config_value_get_value_with_type(cv, CVT_STRING);
/*       gtk_reader_attr_set_foreground(attr, color); */
    }
    g_free(option);
    
    /* background color */
    option = g_strconcat(elementpath, "::", "background", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      gchar *color;
      color = (gchar*)r_config_value_get_value_with_type(cv, CVT_STRING);
/*       gtk_reader_attr_set_background(attr, color); */
    }
    g_free(option);

    /* align */
    option = g_strconcat(elementpath, "::", "align", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      gchar *align;
      PangoAlignment alignment;
      
      align = (gchar*)r_config_value_get_value_with_type(cv, CVT_STRING);
      if (!g_utf8_collate(align, "center"))
        alignment = PANGO_ALIGN_CENTER;
      else if (!g_utf8_collate(align, "right"))
        alignment = PANGO_ALIGN_RIGHT;
      else
      {
        if (g_utf8_collate(align, "left"))
          g_warning("Incorrect value (%s) of option '%s'\n", align, option);
        alignment = PANGO_ALIGN_LEFT;
      }
      
      gtk_reader_paragraph_set_align(paragraph, alignment);
    }
    g_free(option);

    /* justification */
    option = g_strconcat(elementpath, "::", "justify", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      gboolean justify;
      justify = GPOINTER_TO_INT(r_config_value_get_value_with_type(cv, CVT_BOOL));
      gtk_reader_paragraph_set_justify(paragraph, justify);
    }
    g_free(option);

    /* font style (normal, obliq, italic) */
    option = g_strconcat(elementpath, "::", "style", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      gchar *style;
      style = (gchar*)r_config_value_get_value_with_type(cv, CVT_STRING);
      if (!g_utf8_collate(style, "italic"))
      { 
        gtk_reader_attr_set_style(attr, PANGO_STYLE_ITALIC);
      }
      else if (!g_utf8_collate(style, "oblique"))
        gtk_reader_attr_set_style(attr, PANGO_STYLE_OBLIQUE);
      else
      {
        if (g_utf8_collate(style, "normal"))
          g_warning("Incorrect value (%s) of option '%s'\n", style, option);
        gtk_reader_attr_set_style(attr, PANGO_STYLE_NORMAL);
      }
    }
    g_free(option);

    /* font weight (normal, bold, ...) */
    option = g_strconcat(elementpath, "::", "weight", NULL);
    v = g_hash_table_lookup(properties, option);
    if (v && (cv = R_CONFIG_VALUE(v)))
    {
      gchar *weight;
      weight = (gchar*)r_config_value_get_value_with_type(cv, CVT_STRING);
      if (!g_utf8_collate(weight, "ultralight"))
        gtk_reader_attr_set_weight(attr, PANGO_WEIGHT_ULTRALIGHT);
      else if (!g_utf8_collate(weight, "light"))
        gtk_reader_attr_set_weight(attr, PANGO_WEIGHT_LIGHT);
      else if (!g_utf8_collate(weight, "bold"))
        gtk_reader_attr_set_weight(attr, PANGO_WEIGHT_BOLD);
      else if (!g_utf8_collate(weight, "ultrabold"))
        gtk_reader_attr_set_weight(attr, PANGO_WEIGHT_ULTRABOLD);
      else if (!g_utf8_collate(weight, "heavy"))
        gtk_reader_attr_set_weight(attr, PANGO_WEIGHT_HEAVY);
      else
      {
        if (g_utf8_collate(weight, "normal"))
          g_warning("Incorrect value (%s) of option '%s'\n", weight, option);
        gtk_reader_attr_set_weight(attr, PANGO_WEIGHT_NORMAL);
      }
    }
    g_free(option);

    g_free(elementpath);
  }

  if (!gtk_reader_attr_is_set(attr))
  {
    g_object_unref(G_OBJECT(attr));
    attr = NULL;
  }
  
  return attr;
}

void fb2_utils_copy_hash_table(gpointer key,
                               gpointer value,
                               gpointer userdata)
{
  GHashTable *dst;
  
  g_assert(key != NULL);
  g_assert(value != NULL);
  g_assert(userdata != NULL);

  dst = (GHashTable*)userdata;
  g_hash_table_insert(dst, g_strdup((gchar*)key),
                      r_config_value_copy(R_CONFIG_VALUE(value)));
}

/**
 * Converts position in characters to position in bytes in utf-8
 * string.
 * @param utf8string a unicode string.
 * @param pos a position in characters.
 * @return a position in bytes (offset).
 */
glong fb2_utils_utf8_position_to_offset(GString *utf8string,
                                        guint pos)
{
  guint i;
  gchar *text = utf8string->str;
  
  g_return_val_if_fail(utf8string != NULL, 0);

  g_assert(pos < utf8string->len);
  g_return_val_if_fail(g_utf8_validate(utf8string->str, utf8string->len, NULL) == TRUE,
                       pos);
  
  for (i = 0; i < pos; i++)
    text = g_utf8_next_char(text);
  
  return text-utf8string->str;
}


/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
