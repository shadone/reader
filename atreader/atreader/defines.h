#ifndef DEFINES_H
#define DEFINES_H

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif
#include "configuration.h"

#define COUNT(x) (sizeof(x)/sizeof(x[0]))

#ifdef OS_WIN32
#  define DOTREADER_DIR "reader"
#else
#  define DOTREADER_DIR ".reader"
#endif

#define PLUGIN_INTERFACE_VERSION 100

typedef struct _MainOptions MainOptions;
struct _MainOptions
{
  gchar *current_file;          /**< current opened file */
};

extern GSList *plugins;
extern RConfig *configuration;
extern MainOptions *options;

#endif

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
