#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif
#include <gdk/gdkkeysyms.h>
#include "rconfigvalue.h"
#include "defines.h"
#include "callback.h"
#include "ui.h"
#include "plugins.h"

/* Forward declarations */

void callback_menubar_open(gpointer   callback_data,
                           guint      callback_action,
                           GtkWidget *widget);
gboolean callback_reader_button_press(GtkWidget *widget,
                                      GdkEventButton *event,
                                      gpointer user_data);
gboolean callback_reader_key_press(GtkWidget *widget,
                                   GdkEventKey *event,
                                   gpointer user_data);
void callback_reader_popup_options(gpointer   callback_data,
                                   guint      callback_action,
                                   GtkWidget *widget);
void callback_help_plugins(gpointer   callback_data,
                           guint      callback_action,
                           GtkWidget *widget);
void callback_help_about(gpointer   callback_data,
                         guint      callback_action,
                         GtkWidget *widget);
void callback_file_selection_opened_file(GtkButton *button,
                                         gpointer   user_data);


/* Implementation */

void callback_file_selection_opened_file(GtkButton *button,
                                         gpointer   user_data)
{
  GtkFileSelection *fs;
  gchar *filename;

  g_return_if_fail(button != NULL);
  g_return_if_fail(user_data != NULL);
  g_return_if_fail(GTK_FILE_SELECTION(user_data));

  fs = GTK_FILE_SELECTION(user_data);
  filename = g_strdup(gtk_file_selection_get_filename(fs));
  g_assert(filename != NULL);

  gtk_widget_destroy(GTK_WIDGET(user_data));
  ui_open_file(filename);

  g_free(filename);
}

void callback_menubar_open(gpointer   callback_data,
                           guint      callback_action,
                           GtkWidget *widget)
{
  GtkWidget *w;

  w = gtk_file_selection_new("Choose a file:");
   			   
  g_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(w)->ok_button),
                   "clicked",
                   G_CALLBACK(callback_file_selection_opened_file),
                   (gpointer)w);

  g_signal_connect_swapped(GTK_OBJECT(GTK_FILE_SELECTION(w)->cancel_button),
                           "clicked",
                           G_CALLBACK(gtk_widget_destroy),
                           (gpointer)w);

  gtk_widget_show(w);
  return;
}

void callback_menubar_save_prefs(gpointer   callback_data,
                                 guint      callback_action,
                                 GtkWidget *widget)
{
  gchar *config_path;
  GSList *it;
  
  config_path = g_build_filename(g_get_home_dir(), DOTREADER_DIR, "config", NULL);
  g_assert(config_path != NULL);

  /* get properties from plugins and merge them with main property database */
  for (it = plugins; it != NULL; it = g_slist_next(it))
  {
    ReaderPlugin *plugin;
    GHashTable *ht = NULL;

    g_assert(it != NULL && it->data != NULL);
    plugin = (ReaderPlugin*)it->data;

    // create hash table and remember its name
    ht = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, r_config_value_free);
    g_assert(ht != NULL);
    g_hash_table_insert(ht, (gpointer)g_strdup("__name"),
                        r_config_value_new_with_value(CVT_STRING, plugin->info->module_name));

    plugin->get_properties(&ht);
    g_assert(ht != NULL);
    r_config_merge(configuration, plugin->info->module_name, ht);

    g_hash_table_destroy(ht);
  }

  r_config_dump(configuration, config_path);
  g_free(config_path);
}

void callback_reader_popup_options(gpointer   callback_data,
                                   guint      callback_action,
                                   GtkWidget *widget_)
{
  GtkWidget *widget;
  
  if (!g_ascii_strcasecmp(GUINT_TO_POINTER(callback_action), "menubar"))
  {
    widget = widgets.menubar;
  }
  else if (!g_ascii_strcasecmp(GUINT_TO_POINTER(callback_action), "toolbar"))
  {
    widget = widgets.toolbar;
  }
  else if (!g_ascii_strcasecmp(GUINT_TO_POINTER(callback_action), "scroller"))
  {
    widget = widgets.scroller;
  }
  else
  {
    g_assert_not_reached();
  }

  ui_check_item_set_active(GUINT_TO_POINTER(callback_action),
                           !GTK_WIDGET_VISIBLE(widget));
  
  if (GTK_WIDGET_VISIBLE(widget))
    gtk_widget_hide(widget);
  else
    gtk_widget_show(widget);
}

gboolean callback_reader_button_press(GtkWidget *widget,
                                     GdkEventButton *event,
                                     gpointer user_data)
{
  if (event->button == 3)       // right button, show popup menu
  {
    gtk_item_factory_popup(widgets.popup_item_factory, event->x_root, event->y_root,
                           event->button, event->time);
  }
  return TRUE;
}

gboolean callback_reader_key_press(GtkWidget *widget,
                                   GdkEventKey *event,
                                   gpointer user_data)
{
  if (event->keyval == GDK_Down)
  {
    gtk_reader_area_scroll_pixels(widgets.reader, 10);
    gtk_widget_queue_draw(GTK_WIDGET(widgets.reader));
  }
  else if (event->keyval == GDK_Up)
  {
    gtk_reader_area_scroll_pixels(widgets.reader, -10);
    gtk_widget_queue_draw(GTK_WIDGET(widgets.reader));
  }
  else if (event->keyval == GDK_Next || event->keyval == GDK_space)
  {
    gtk_reader_area_scroll_pixels(widgets.reader, widget->allocation.height);
    gtk_widget_queue_draw(GTK_WIDGET(widgets.reader));
  }
  else if (event->keyval == GDK_Prior)
  {
    gtk_reader_area_scroll_pixels(widgets.reader, -widget->allocation.height);
    gtk_widget_queue_draw(GTK_WIDGET(widgets.reader));
  }
  return TRUE;
}

void callback_help_plugins(gpointer   callback_data,
                           guint      callback_action,
                           GtkWidget *widget)
{
  GtkWidget *ok_button, *plugin_button;
  GtkCellRenderer     *renderer;
  GtkTreeModel        *model;
  GtkWidget           *view;
  GtkListStore        *store;
  GtkTreeIter          iter;
  GSList              *it;
  
  if (widgets.plugin_about_dialog != NULL)
  { 
    /* about dialog is already opened */
    gtk_window_activate_focus(GTK_WINDOW(widgets.about_dialog));
    return;
  }

  widgets.plugin_about_dialog = gtk_dialog_new();
  gtk_window_set_title(GTK_WINDOW(widgets.plugin_about_dialog), "Plugins about dialog");
  gtk_window_set_transient_for(GTK_WINDOW(widgets.plugin_about_dialog),
                               GTK_WINDOW(widgets.main_window));
  
  g_signal_connect_swapped(G_OBJECT(widgets.plugin_about_dialog), "destroy",
                           G_CALLBACK(g_nullify_pointer),
                           &widgets.plugin_about_dialog);

  plugin_button = gtk_dialog_add_button(GTK_DIALOG(widgets.plugin_about_dialog),
                                        "Plugin info", 0);
  ok_button = gtk_dialog_add_button(GTK_DIALOG(widgets.plugin_about_dialog),
                                    GTK_STOCK_OK, GTK_RESPONSE_OK);
  g_signal_connect_swapped(G_OBJECT(ok_button), "clicked",
                           G_CALLBACK(gtk_widget_destroy),
                           widgets.plugin_about_dialog);

  view = gtk_tree_view_new();

  g_signal_connect(G_OBJECT(plugin_button), "clicked",
                   G_CALLBACK(callback_show_plugin_info_dialog),
                   (gpointer)view);
  
  /* column 0 */
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(view),
                                              -1,
                                              "Plugin",
                                              renderer,
                                              "text", 0,
                                              NULL);
  /* column 1 */
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(view),
                                              -1,
                                              "Copyright",
                                              renderer,
                                              "text", 1,
                                              NULL);

  store = gtk_list_store_new(2, G_TYPE_STRING, G_TYPE_STRING);

  /* Append rows and fill in some data */
  for (it = plugins; it != NULL; it = g_slist_next(it))
  {
    ReaderPlugin *plugin;

    g_assert(it != NULL && it->data != NULL);
    plugin = (ReaderPlugin*)it->data;
    
    gtk_list_store_append(store, &iter);
    gtk_list_store_set(store, &iter,
                       0, plugin->info->module_name,
                       1, plugin->info->copyright_info,
                       -1);
  }
  
  model = GTK_TREE_MODEL(store);
  gtk_tree_view_set_model(GTK_TREE_VIEW(view), model);
  g_object_unref(model); /* destroy model automatically with view */

  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(widgets.plugin_about_dialog)->vbox),
                     view, TRUE, TRUE, 0);
  gtk_widget_show(view);


  /* show plugin info dialog window */
  gtk_widget_show_all(widgets.plugin_about_dialog);
}

void callback_help_about(gpointer   callback_data,
                         guint      callback_action,
                         GtkWidget *widget)
{
  GtkWidget *ok_button, *hbox, *label;

  if (widgets.about_dialog == NULL)
  {
    gchar *about_string;
    
    widgets.about_dialog = gtk_dialog_new();
    gtk_window_set_title(GTK_WINDOW(widgets.about_dialog), "About reader...");
    gtk_window_set_transient_for(GTK_WINDOW(widgets.about_dialog),
                                 GTK_WINDOW(widgets.main_window));
    ok_button = gtk_dialog_add_button(GTK_DIALOG(widgets.about_dialog),
                                      GTK_STOCK_OK, GTK_RESPONSE_OK);
    g_signal_connect_swapped(G_OBJECT(ok_button), "clicked",
                             G_CALLBACK(gtk_widget_destroy),
                             widgets.about_dialog);
    g_signal_connect_swapped(G_OBJECT(widgets.about_dialog), "destroy",
                             G_CALLBACK(g_nullify_pointer),
                             &widgets.about_dialog);
    
    hbox = gtk_hbox_new(TRUE, 0);
    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(widgets.about_dialog)->vbox), hbox);
    gtk_widget_show(hbox);

    about_string = g_strdup_printf("Reader version %s\n\nAuthor: Denis Dzyubenko <shad@novoross.ru>", VERSION);
    label = gtk_label_new(about_string);
    gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 10);
    gtk_widget_show(label);
    g_free(about_string);
    
    gtk_widget_show_all(widgets.about_dialog);
  }
  else
  {
    /* about dialog is already opened */
    gtk_window_activate_focus(GTK_WINDOW(widgets.about_dialog));
  }
}

void callback_show_plugin_preferences(GtkWidget *widget,
                                      gpointer user_data)
{
  GtkTreeView *view;
  GtkTreeSelection *selection;
  GtkTreeModel *model;
  GtkTreeIter iter;
  gboolean res;
  gchar *module_name=NULL;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(user_data != NULL);
  g_return_if_fail(GTK_IS_TREE_VIEW(user_data));

  view = GTK_TREE_VIEW(user_data);

  selection = gtk_tree_view_get_selection(view);
  g_assert(selection != NULL);

  res = gtk_tree_selection_get_selected(selection, &model, &iter);
  if (!res)
    return;

  gtk_tree_model_get(model, &iter,
                     0, &module_name,
                     -1);
  g_assert(module_name != NULL);

  /* show plugin properties dialog */
  ui_show_plugin_configuration(module_name);
}

void callback_plugin_configure_ok_button(GtkWidget *widget,
                                         gpointer   user_data)
{
  ReaderPlugin *plugin;
  
  g_return_if_fail(user_data != NULL);

  plugin = READER_PLUGIN(user_data);

  g_assert(plugin->configure_dialog != NULL);
  if (plugin->configure_get_data(GTK_DIALOG(plugin->configure_dialog),
                                 plugin->callback_ok_configure_userdata))
  {
    gtk_widget_destroy(plugin->configure_dialog);
    plugin->configure_dialog = NULL;

    
  }
}

void callback_show_plugin_info_dialog(GtkWidget *widget,
                                      gpointer   user_data)
{
  GtkTreeView *view;
  GtkTreeSelection *selection;
  GtkTreeModel *model;
  GtkTreeIter iter;
  GSList     *it;
  gchar      *module_name=NULL;
  gboolean    res;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(user_data != NULL);
  g_return_if_fail(GTK_IS_TREE_VIEW(user_data));

  view = GTK_TREE_VIEW(user_data);

  selection = gtk_tree_view_get_selection(view);
  g_assert(selection != NULL);

  res = gtk_tree_selection_get_selected(selection, &model, &iter);
  if (!res)
    return;

  gtk_tree_model_get(model, &iter,
                     0, &module_name,
                     -1);
  g_assert(module_name != NULL);

  for (it = plugins; it != NULL; it = g_slist_next(it))
  {
    ReaderPlugin *plugin;
    
    g_assert(it != NULL && it->data != NULL);
    plugin = READER_PLUGIN(it->data);
    if (g_utf8_collate(plugin->info->module_name, module_name))
      continue;

    if (plugin->about_dialog == NULL)
    {
      plugin->about_dialog = GTK_WIDGET(plugin->show_about(GTK_WINDOW(widgets.main_window)));
      if (plugin->about_dialog != NULL)
      {
        g_signal_connect_swapped(G_OBJECT(plugin->about_dialog), "destroy",
                                 G_CALLBACK(g_nullify_pointer),
                                 &plugin->about_dialog);
        gtk_widget_show_all(plugin->about_dialog);
        g_assert(GTK_IS_WINDOW(plugin->about_dialog));
      }
    }
    else
    {
      /* about dialog is already opened */
      gtk_window_activate_focus(GTK_WINDOW(plugin->about_dialog));
    }
  }
}

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
