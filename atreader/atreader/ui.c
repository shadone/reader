#include <gtk/gtk.h>
#include "rmoduleinfo.h"
#include "defines.h"
#include "ui.h"
#include "callback.h"
#include "utils.h"
#include "plugins.h"


/* Forward declarations */

GtkWidget* ui_create_main_menubar();
void       ui_create_main_window(void);
void       ui_create_main_popup_menu_item_factory(void);
GtkWidget* ui_create_plugin_list(const gchar *plugintype,
                                 GSList *plugin_list,
                                 gboolean separate_by_extension);
ReaderPlugin* ui_choose_plugin_dialog(GSList *plugin_list);

/* Implementation */

Widgets widgets;

static GtkItemFactoryEntry main_menubar_items[]=
{
  {"/_File",          NULL,         0,                         0, "<Branch>",},
  {"/File/tearoff1",  NULL,         0,                         0, "<Tearoff>",},
  {"/File/_Open",     "<control>O", G_CALLBACK(callback_menubar_open),  0,},
  {"/File/sep1",      NULL,         0,                         0, "<Separator>",},
  {"/File/_Save preferences", NULL, G_CALLBACK(callback_menubar_save_prefs),  0,},
  {"/File/Preferences", NULL, G_CALLBACK(ui_show_preferences),  0,},
  {"/File/Plugins", NULL, G_CALLBACK(ui_show_plugins_preferences),  0,},
  {"/File/sep2",      NULL,         0,                         0, "<Separator>",},
  {"/File/_Quit",     "<control>Q", G_CALLBACK(gtk_main_quit), 0,},
  {"/_View",          NULL,         0,                         0, "<Branch>",},
  {"/View/tearoff1",  NULL,         0,                         0, "<Tearoff>",},
  {"/View/Show _menu",NULL,        G_CALLBACK(callback_reader_popup_options),   GPOINTER_TO_UINT("menubar"), "<ToggleItem>",},
  {"/View/Show _toolbar",NULL,     G_CALLBACK(callback_reader_popup_options),   GPOINTER_TO_UINT("toolbar"), "<ToggleItem>",},
  {"/View/Show _scroller",NULL,    G_CALLBACK(callback_reader_popup_options),   GPOINTER_TO_UINT("scroller"), "<ToggleItem>",},
  {"/_Help",       NULL,    0,   0, "<LastBranch>",},
  {"/Help/Plugins",NULL,    G_CALLBACK(callback_help_plugins),   0, "<Item>",},
  {"/Help/About",  NULL,    G_CALLBACK(callback_help_about),     0, "<Item>",},
  
};

static GtkItemFactoryEntry main_popupmenu_items[]=
{
  {"/_View",  NULL,         0,                         0, "<Branch>"},
  {"/View/Show _menu",NULL,    G_CALLBACK(callback_reader_popup_options), GPOINTER_TO_UINT("menubar"), "<ToggleItem>"},
  {"/View/Show _toolbar",NULL,    G_CALLBACK(callback_reader_popup_options), GPOINTER_TO_UINT("toolbar"), "<ToggleItem>"},
  {"/View/Show _scroller",NULL,    G_CALLBACK(callback_reader_popup_options), GPOINTER_TO_UINT("scroller"), "<ToggleItem>"},
  {"/sep1",      NULL,         0,                         0, "<Separator>"},
  {"/_Quit",     "<control>Q", G_CALLBACK(gtk_main_quit), 0}
};

void ui_check_item_set_active(gchar *item, gboolean value)
{
  GtkWidget *widget;
  GtkItemFactory *factory;

  g_return_if_fail(item != NULL);
  
  if (!g_ascii_strcasecmp(item, "menubar"))
  {
    factory = gtk_item_factory_from_widget(widgets.menubar);
    widget = gtk_item_factory_get_item(factory, "/View/Show menu");
    GTK_CHECK_MENU_ITEM(widget)->active = value;
    
    widget = gtk_item_factory_get_item(widgets.popup_item_factory, "/View/Show menu");
    GTK_CHECK_MENU_ITEM(widget)->active = value;
  }
  else if (!g_ascii_strcasecmp(item, "toolbar"))
  {
    factory = gtk_item_factory_from_widget(widgets.menubar);
    widget = gtk_item_factory_get_item(factory, "/View/Show toolbar");
    GTK_CHECK_MENU_ITEM(widget)->active = value;
    
    widget = gtk_item_factory_get_item(widgets.popup_item_factory, "/View/Show toolbar");
    GTK_CHECK_MENU_ITEM(widget)->active = value;
  }
  else if (!g_ascii_strcasecmp(item, "scroller"))
  {
    factory = gtk_item_factory_from_widget(widgets.menubar);
    widget = gtk_item_factory_get_item(factory, "/View/Show scroller");
    GTK_CHECK_MENU_ITEM(widget)->active = value;
    
    widget = gtk_item_factory_get_item(widgets.popup_item_factory, "/View/Show scroller");
    GTK_CHECK_MENU_ITEM(widget)->active = value;
  }
  else
    g_assert_not_reached();
}

GtkWidget* ui_create_main_menubar()
{
  GtkItemFactory *item_factory;
  GtkAccelGroup *accel_group;
    
  accel_group = gtk_accel_group_new();
  item_factory = gtk_item_factory_new(GTK_TYPE_MENU_BAR, "<main>",
                                      accel_group);
  g_object_set_data_full(G_OBJECT(widgets.main_window), "<main>",
                         item_factory, (GDestroyNotify)g_object_unref);
  gtk_window_add_accel_group(GTK_WINDOW(widgets.main_window), accel_group);
  gtk_item_factory_create_items(item_factory, COUNT(main_menubar_items),
                                main_menubar_items, NULL);

  return gtk_item_factory_get_widget(item_factory, "<main>");
}

void ui_set_default_menu(void)
{
  ui_check_item_set_active("menubar", TRUE);
  ui_check_item_set_active("toolbar", FALSE);
  ui_check_item_set_active("scroller", TRUE);
}

void ui_create_main_window(void)
{
  GtkAdjustment *adjustment;
  GtkWidget *vbox;
  GtkStockItem sit;
  GtkIconSet *is;
  
  widgets.main_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  g_signal_connect(G_OBJECT(widgets.main_window), "destroy",
                   G_CALLBACK(gtk_main_quit), NULL);
  gtk_window_set_title(GTK_WINDOW(widgets.main_window), "Reader");

  vbox = gtk_vbox_new(FALSE, 0);

  gtk_container_add(GTK_CONTAINER(widgets.main_window), vbox);
  gtk_widget_show(vbox);

  // create menubar
  widgets.menubar = ui_create_main_menubar();
  gtk_box_pack_start(GTK_BOX(vbox), widgets.menubar, FALSE, TRUE, 0);
  gtk_widget_show(widgets.menubar);
  
  // create toolbar
  widgets.toolbar = gtk_toolbar_new();
  g_assert( gtk_stock_lookup(GTK_STOCK_OPEN, &sit) == TRUE );
  is = gtk_style_lookup_icon_set(gtk_widget_get_style(widgets.toolbar),
                                 sit.stock_id);
  gtk_toolbar_append_item(GTK_TOOLBAR(widgets.toolbar), sit.label,
                          "Open book", "Open book",
                          gtk_image_new_from_icon_set(is, GTK_ICON_SIZE_SMALL_TOOLBAR),
                          NULL, NULL);
  gtk_box_pack_start(GTK_BOX(vbox), widgets.toolbar, FALSE, TRUE, 0);

  // create popup menu
  ui_create_main_popup_menu_item_factory();
  
  ui_set_default_menu();
  
  // create main viewport
  widgets.reader = gtk_reader_area_new(NULL);
  adjustment = gtk_reader_area_get_adjustment(widgets.reader);
  g_signal_connect(G_OBJECT(widgets.reader), "button_press_event",
                   G_CALLBACK(callback_reader_button_press), NULL);
  g_signal_connect(G_OBJECT(widgets.reader), "key_press_event",
                   G_CALLBACK(callback_reader_key_press), NULL);
  gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(widgets.reader), TRUE, TRUE, 0);
  gtk_widget_show(GTK_WIDGET(widgets.reader));

  // create status(scroll) line
  widgets.scroller = gtk_hscrollbar_new(adjustment);
  gtk_box_pack_start(GTK_BOX(vbox), widgets.scroller, FALSE, TRUE, 0);
  gtk_widget_show(widgets.scroller);

  gtk_window_set_default_size(GTK_WINDOW(widgets.main_window), 400, 250);
  gtk_widget_show(widgets.main_window);
}

void ui_create_main_popup_menu_item_factory(void)
{
  GtkAccelGroup *accel_group;

  if (widgets.popup_item_factory == NULL)
  {
    accel_group = gtk_accel_group_new();
    widgets.popup_item_factory = gtk_item_factory_new(
      GTK_TYPE_MENU, "<popup>",
      accel_group);
    g_object_set_data_full(G_OBJECT(widgets.main_window), "<popup>",
                           widgets.popup_item_factory,
                           (GDestroyNotify)g_object_unref);
    gtk_window_add_accel_group(GTK_WINDOW(widgets.main_window), accel_group);
    gtk_item_factory_create_items(widgets.popup_item_factory, COUNT(main_popupmenu_items),
                                  main_popupmenu_items, NULL);
  }
}

gboolean ui_open_file(const gchar *filename)
{
  GSList          *it;
  gchar           *extension;
  GSList          *appropriate_plugins=NULL;
  ReaderPlugin    *plugin;
  GSList          *buffers;
  gboolean         retval;

  g_return_val_if_fail(filename != NULL, FALSE);

  if (!g_file_test(filename, G_FILE_TEST_EXISTS) ||
      g_file_test(filename, G_FILE_TEST_IS_DIR))
    return FALSE;
  
  extension = utils_get_file_extension(filename);
  if (extension == NULL)
  {
    /* ask user to choose apropriate plugin for file without extension*/
    plugin = ui_choose_plugin_dialog(NULL);

    if (plugin)
      appropriate_plugins = g_slist_append(appropriate_plugins, plugin);
  }

  if (appropriate_plugins == NULL)
  {
    for (it = plugins; it != NULL; it = g_slist_next(it))
    {
      ReaderPlugin *plugin;
      guint i=0;
      
      g_assert(it != NULL && it->data != NULL);
      plugin = READER_PLUGIN(it->data);
      
      g_assert(plugin->info->supported_ext != NULL);
      while (plugin->info->supported_ext[i] != NULL)
      {
        if (!g_utf8_collate(extension, plugin->info->supported_ext[i]))
        {
          appropriate_plugins = g_slist_append(appropriate_plugins, plugin);
        }
        i++;
      }
    }
  }
  
  if (appropriate_plugins == NULL)
  {
    /* no plugin for this extension */
    return FALSE;
  }


  if (g_slist_length(appropriate_plugins) > 1)
  {
    /* ask user what plugin to use */
    plugin = ui_choose_plugin_dialog(appropriate_plugins);
  }
  else
    plugin = (ReaderPlugin*)appropriate_plugins->data;

  g_assert(plugin != NULL);

  buffers = NULL;               /* init slist */
  g_assert(plugin->open_file != NULL);
  retval = plugin->open_file(widgets.main_window, filename, &buffers);
  if (retval)
  {
    if (buffers == NULL)
    {
      g_warning("Plugin '%s' returned no buffers when opening file '%s'\n",
                plugin->info->module_name, filename);
    }
    else
    {
      gtk_reader_area_set_buffers(widgets.reader, buffers);
      gtk_reader_area_set_position(widgets.reader, 0, 0);
      gtk_widget_queue_draw(GTK_WIDGET(widgets.reader));
      
      /* TODO: invent ROptions class and replace this code */
      if (options->current_file)
        g_free(options->current_file);
      options->current_file = g_strdup(filename); /* remember current file */
    }
  }
  
  g_slist_free(buffers); buffers = NULL;
  g_free(extension);

  return retval;
}

void ui_show_preferences(gpointer   callback_data,
                         guint      callback_action,
                         GtkWidget *widget)
{
}

/* Return TreeView filled with plugin list of specified type.
 * If plugin_list is specified, iterate search plugins only in this
 * list instead of global list of plugins
 * If plugintype==NULL, all plugins filled.
 * Argument separate_by_extension tells whether to specify multiple
 * times plugins which support several extensions
 */
GtkWidget* ui_create_plugin_list(const gchar *plugintype,
                                 GSList      *plugin_list,
                                 gboolean     separate_by_extension)
{
  GtkCellRenderer     *renderer;
  GtkTreeModel        *model;
  GtkWidget           *view;
  GtkListStore        *store;
  GtkTreeIter          iter;
  GSList *it;

  view = gtk_tree_view_new();

  /* column 0 */
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(view),
                                              -1,
                                              "Plugin",
                                              renderer,
                                              "text", 0,
                                              NULL);
  /* column 1 */
  renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(view),
                                              -1,
                                              "Extension",
                                              renderer,
                                              "text", 1,
                                              NULL);

  store = gtk_list_store_new(2, G_TYPE_STRING, G_TYPE_STRING);

  it = (plugin_list == NULL) ? plugins : plugin_list;
  for (; it != NULL; it = g_slist_next(it))
  {
    ReaderPlugin *plugin;
    guint i;

    g_assert(it != NULL && it->data != NULL);
    plugin = (ReaderPlugin*)it->data;
    
    /* TODO: if plugintype != NULL, check that plugin type match plugintype */
    
    g_assert(plugin->info->supported_ext != NULL);

    if (separate_by_extension)
    {
      i = 0;
      while (plugin->info->supported_ext[i] != NULL)
      {
        gtk_list_store_append(store, &iter);
        gtk_list_store_set(store, &iter,
                           0, plugin->info->module_name,
                           1, plugin->info->supported_ext[i],
                           -1);
        i++;
      }
    }
    else
    {
      gchar *str=NULL;
      i = 0;
      while (plugin->info->supported_ext[i] != NULL)
      {
        if (str)
        {
          gchar *buf;
          
          buf = g_strdup_printf("%s, %s", str, plugin->info->supported_ext[i]);
          g_free(str);
          str = buf;
        }
        else
          str = g_strdup_printf("%s", plugin->info->supported_ext[i]);

        i++;
      }
      gtk_list_store_append(store, &iter);
      gtk_list_store_set(store, &iter,
                         0, plugin->info->module_name,
                         1, str,
                         -1);
      g_free(str);
    }
  }
  
  model = GTK_TREE_MODEL(store);

  gtk_tree_view_set_model(GTK_TREE_VIEW(view), model);

  g_object_unref(model); /* destroy model automatically with view */

  return view;
}

void ui_show_plugins_preferences(gpointer   callback_data,
                                 guint      callback_action,
                                 GtkWidget *widget)
{
  if (widgets.plugin_prefs == NULL)
  {
    GtkWidget *view;
    GtkWidget *close_button, *prefs_button;

    widgets.plugin_prefs = gtk_dialog_new();
    g_assert(widgets.plugin_prefs != NULL);
    gtk_window_set_title(GTK_WINDOW(widgets.plugin_prefs), "Plugin list");

    g_signal_connect_swapped(G_OBJECT(widgets.plugin_prefs), "destroy",
                             G_CALLBACK(g_nullify_pointer), &widgets.plugin_prefs);

    prefs_button = gtk_dialog_add_button(GTK_DIALOG(widgets.plugin_prefs),
                                         "Show preferences", 1);

    close_button = gtk_dialog_add_button(GTK_DIALOG(widgets.plugin_prefs),
                                         GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);
    g_signal_connect_swapped(G_OBJECT(close_button), "clicked",
                             G_CALLBACK(gtk_widget_destroy), widgets.plugin_prefs);
    
    view = ui_create_plugin_list("Input", NULL, FALSE);
    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(widgets.plugin_prefs)->vbox), view);

    g_signal_connect(G_OBJECT(prefs_button), "clicked",
                     G_CALLBACK(callback_show_plugin_preferences), view);

    gtk_widget_show_all(widgets.plugin_prefs);
  }
  else
  {
    /* FIXME: activate dialog window */
    gtk_window_activate_focus(GTK_WINDOW(widgets.plugin_prefs));
  }
}

void ui_show_plugin_configuration(gchar *module_name)
{
  GSList *it;

  g_return_if_fail(module_name != NULL);

  for (it = plugins; it != NULL; it = g_slist_next(it))
  {
    ReaderPlugin *plugin;
    
    g_assert(it != NULL && it->data != NULL);
    plugin = READER_PLUGIN(it->data);
    if (g_utf8_collate(plugin->info->module_name, module_name))
      continue;
    
    if (plugin->configure_dialog == NULL)
    {
      GtkWidget *ok_btn, *close_btn;
      gchar *title;

      title = g_strdup_printf("Configuration: %s", plugin->info->module_name);

      // create and show plugin properties dialog
      plugin->configure_dialog = gtk_dialog_new();
      gtk_window_set_title(GTK_WINDOW(plugin->configure_dialog), title);
      g_free(title);
      gtk_window_set_transient_for(GTK_WINDOW(plugin->configure_dialog),
                                   GTK_WINDOW(widgets.main_window));
      ok_btn = gtk_dialog_add_button(GTK_DIALOG(plugin->configure_dialog),
                                     GTK_STOCK_OK,
                                     GTK_RESPONSE_OK);
      close_btn = gtk_dialog_add_button(GTK_DIALOG(plugin->configure_dialog),
                                        GTK_STOCK_CLOSE,
                                        GTK_RESPONSE_CLOSE);
      
      if (!plugin->configure(GTK_DIALOG(plugin->configure_dialog),
                             &plugin->callback_ok_configure_userdata))
      {
        // don't show dialog (maybe this plugin doesn't need a configuration dialog)
        gtk_widget_destroy(plugin->configure_dialog);
        plugin->configure_dialog = NULL;
        return;
      }
      
      g_signal_connect(G_OBJECT(ok_btn), "clicked",
                       G_CALLBACK(callback_plugin_configure_ok_button),
                       (gpointer)plugin);

      g_signal_connect_swapped(G_OBJECT(close_btn), "clicked",
                               G_CALLBACK(gtk_widget_destroy), plugin->configure_dialog);
      g_signal_connect_swapped(G_OBJECT(plugin->configure_dialog), "destroy",
                               G_CALLBACK(g_nullify_pointer), &(plugin->configure_dialog));
 
      gtk_widget_show(plugin->configure_dialog);
    }
    else
    {
      /* FIXME: activate dialog window */
      gtk_window_activate_focus(GTK_WINDOW(plugin->configure_dialog));
    }
    return;
  }
  g_assert_not_reached();
}


ReaderPlugin* ui_choose_plugin_dialog(GSList *plugin_list)
{
  ReaderPlugin *plugin=NULL;
  GtkWidget    *choose_dlg;
  GtkWidget    *view;
  
  choose_dlg = gtk_dialog_new_with_buttons(
    "Choose a plugin:",
    widgets.main_window,
    GTK_DIALOG_MODAL,
    GTK_STOCK_OK,
    GTK_RESPONSE_OK,
    NULL);
  g_assert(choose_dlg != NULL);

  view = ui_create_plugin_list(NULL, plugin_list, FALSE);
  gtk_container_add(GTK_CONTAINER(GTK_DIALOG(choose_dlg)->vbox), view);
  gtk_widget_show(view);

  if (gtk_dialog_run(GTK_DIALOG(choose_dlg)) == GTK_RESPONSE_OK)
  {
    GtkTreePath *path;
    gint *indices;
    
    gtk_tree_view_get_cursor(GTK_TREE_VIEW(view), &path, NULL);
    indices = gtk_tree_path_get_indices(path);

    if (indices != NULL)
    {
      GSList *it;
      it = g_slist_nth(plugins, indices[0]);
      if (it && it->data)
        plugin = (ReaderPlugin*)it->data;
    }
  }

  gtk_widget_destroy(choose_dlg);

  return plugin;
}

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
