#ifndef UTILS_H
#define UTILS_H

#include <glib.h>


glong utils_utf8_position_to_offset(GString *utf8string,
                                    guint pos);
gchar* utils_get_file_extension    (const gchar *filename);
void   utils_make_reader_dir       (void);
void   utils_hash_table_get_keys   (GHashTable *ht,
                                    GSList **list);
void   utils_initialize_config_file(const gchar *filename);

#endif

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
