#ifndef RMODULEINFO_H
#define RMODULEINFO_H

#include <gtk/gtk.h>
#include <glib.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define R_MODULE_INFO(obj)          GTK_CHECK_CAST(obj, r_module_info_get_type(), RModuleInfo)
#define R_MODULE_INFO_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, r_module_info_get_type(), RModuleInfoClass)
#define R_IS_MODULE_INFO(obj)       GTK_CHECK_TYPE(obj, r_module_info_get_type())

/* possible module versions */
#define MODULE_VERSION_1_0         100

typedef struct _RModuleInfo        RModuleInfo;
typedef struct _RModuleInfoClass   RModuleInfoClass;

struct _RModuleInfo
{
  GObject parent_instance;

  gchar  *module_name;
  gchar **supported_ext;
  gchar  *copyright_info;
};

struct _RModuleInfoClass
{
  GObjectClass parent_class;
};


RModuleInfo* r_module_info_new       ();
RModuleInfo* r_module_info_new_with_value(gchar *name, gchar **extensions, gchar *copyright);
GtkType      r_module_info_get_type  (void);
void         r_module_info_free      (RModuleInfo *mi);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* RMODULEINFO_H */

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
