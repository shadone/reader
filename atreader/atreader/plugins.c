#include <string.h>
#include "plugins.h"

/* Forward declarations */

static void reader_plugin_class_init               (ReaderPluginClass *klass);
static void reader_plugin_init                     (ReaderPlugin      *plugin);

GType reader_plugin_get_type()
{
  static GType reader_plugin_type = 0;
  
  if (!reader_plugin_type)
  {
    static const GTypeInfo reader_plugin_info =
      {
        sizeof (ReaderPluginClass),
        NULL,
        NULL,
        (GClassInitFunc) reader_plugin_class_init,
        NULL,
        NULL,
        sizeof (ReaderPlugin),
        0,
        (GInstanceInitFunc) reader_plugin_init,
      };
    
    reader_plugin_type = g_type_register_static(G_TYPE_OBJECT, "ReaderPlugin", &reader_plugin_info, 0);
  }
  
  return reader_plugin_type;
}

static void reader_plugin_class_init(ReaderPluginClass *klass)
{
}

static void reader_plugin_init(ReaderPlugin *plugin)
{
  plugin->module = NULL;
  memset(&(plugin->info), 0, sizeof(plugin->info));
  plugin->configure_dialog      = NULL;
  plugin->callback_ok_configure_userdata = NULL;
  plugin->about_dialog          = NULL;
  
  // initialize module callbacks
  plugin->module_init        = NULL;
  plugin->get_info           = NULL;
  plugin->open_file          = NULL;
  plugin->set_properties     = NULL;
  plugin->get_properties     = NULL;
  plugin->configure          = NULL;
  plugin->configure_get_data = NULL;
  plugin->show_about         = NULL;
  plugin->get_interface_version=NULL;
}

ReaderPlugin* reader_plugin_new(GModule *module)
{
  ReaderPlugin *plugin;

  g_return_val_if_fail(module != NULL, NULL);
  
  plugin = g_object_new(reader_plugin_get_type(), NULL);
  
  plugin->module = module;
  plugin->info   = NULL;

  if (!g_module_symbol(plugin->module, "module_init", (gpointer*)&plugin->module_init))
  {
    g_warning(g_module_error());
    return NULL;
  }
  if (!g_module_symbol(plugin->module, "get_info", (gpointer*)&plugin->get_info))
  {
    g_warning(g_module_error());
    return NULL;
  }
  if (!g_module_symbol(plugin->module, "open_file", (gpointer*)&plugin->open_file))
  {
    g_warning(g_module_error());
    return NULL;
  }
  if (!g_module_symbol(plugin->module, "set_properties", (gpointer*)&plugin->set_properties))
  {
    g_warning(g_module_error());
    return NULL;
  }
  if (!g_module_symbol(plugin->module, "get_properties", (gpointer*)&plugin->get_properties))
  {
    g_warning(g_module_error());
    return NULL;
  }
  if (!g_module_symbol(plugin->module, "configure", (gpointer*)&plugin->configure))
  {
    g_warning(g_module_error());
    return NULL;
  }
  if (!g_module_symbol(plugin->module, "configure_get_data", (gpointer*)&plugin->configure_get_data))
  {
    g_warning(g_module_error());
    return NULL;
  }
  if (!g_module_symbol(plugin->module, "show_about", (gpointer*)&plugin->show_about))
  {
    g_warning(g_module_error());
    return NULL;
  }
  if (!g_module_symbol(plugin->module, "get_interface_version", (gpointer*)&plugin->get_interface_version))
  {
    g_warning(g_module_error());
    return NULL;
  }

  plugin->info = plugin->get_info();
  g_assert(plugin->info != NULL);

  return plugin;
}

void reader_plugin_free(ReaderPlugin *plugin)
{
  gchar *module_name;
  
  g_return_if_fail(plugin != NULL);
  g_return_if_fail(IS_READER_PLUGIN(plugin));

  g_assert(plugin->module != NULL);

  module_name = g_strdup(g_module_name(plugin->module));
  if (!g_module_close(plugin->module))
    g_warning("Error closing plugin '%s'", module_name);
  g_free(module_name);

  r_module_info_free(plugin->info);

  /* make all inconsistent pointers NULL */
  reader_plugin_init(plugin);
}


/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
