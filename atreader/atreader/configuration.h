#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <gtk/gtk.h>
#include <glib.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define R_CONFIG(obj)          GTK_CHECK_CAST(obj, r_config_get_type(), RConfig)
#define R_CONFIG_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, r_config_get_type(), RConfigClass)
#define R_IS_CONFIG(obj)       GTK_CHECK_TYPE(obj, r_config_get_type())
  
typedef struct _RConfig      RConfig;
typedef struct _RConfigClass RConfigClass;

/**
 * Container for configuration options.
 */
struct _RConfig
{
  GObject parent_instance;

  /**
   *  Current selected profile name
   */
  gchar      *profile;
  
  /**
   *  Hash which contains all config values.
   *  Hash of (list of hashes) with structure key => value, where
   *  key is a string (profile name), and value is a slist which
   *  contains hashes for each section defined in config. Key of this
   *  internal hash is a string (path to option or group,
   *  e.g. body::title::p) and value is a #RConfigValue* or NULL
   *  if it is a group.
   *  @see RConfigValue
   */
  GHashTable *hashes;           /* Hash with per-profile configs. */
};

struct _RConfigClass
{
  GObjectClass parent_class;
};


RConfig*    r_config_new            (void);
GtkType     r_config_get_type       (void);
void        r_config_free           (RConfig *config);
void        r_config_parse          (RConfig     *config,
                                     const gchar *filename);
void        r_config_dump           (RConfig     *config,
                                     const gchar *filename);
void        r_config_merge          (RConfig     *config,
                                     const gchar *section_name,
                                     GHashTable  *table);
GHashTable* r_config_get_properties (RConfig     *config,
                                     const gchar *section_name);
gint        r_config_get_int_default(RConfig     *config,
                                     const gchar *section_name,
                                     const gchar *option_name,
                                     gint defvalue);
gboolean    r_config_get_bool_default(RConfig     *config,
                                      const gchar *section_name,
                                      const gchar *option_name,
                                      gboolean     defvalue);
gchar*      r_config_get_string_default(RConfig     *config,
                                        const gchar *section_name,
                                        const gchar *option_name,
                                        const gchar *defvalue);
gboolean    r_config_is_value_exists(RConfig     *config,
                                     const gchar *section_name,
                                     const gchar *option_name);
gboolean    r_config_is_group_exists(RConfig     *config,
                                     const gchar *section_name,
                                     const gchar *group_name);
void        r_config_set_profile    (RConfig     *config,
                                     const gchar *profile);
const gchar* r_config_get_profile   (RConfig     *config);
  
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CONFIGURATION_H */

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
