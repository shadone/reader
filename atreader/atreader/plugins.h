#ifndef PLUGINS_H
#define PLUGINS_H

#include <gtk/gtk.h>
#include <glib.h>
#include <gmodule.h>
#include <libatreader/gtkreaderbuffer.h>
#include "rmoduleinfo.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define READER_PLUGIN(obj)          GTK_CHECK_CAST(obj, reader_plugin_get_type(), ReaderPlugin)
#define READER_PLUGIN_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, gtk_reader_chunk_get_type(), ReaderPluginClass)
#define IS_READER_PLUGIN(obj)       GTK_CHECK_TYPE(obj, reader_plugin_get_type())
  
typedef struct _ReaderPlugin      ReaderPlugin;
typedef struct _ReaderPluginClass ReaderPluginClass;

struct _ReaderPlugin
{
  GObject parent_instance;

  GModule     *module;
  RModuleInfo *info;

  GtkWidget *configure_dialog;
  gpointer   callback_ok_configure_userdata;

  GtkWidget *about_dialog;

  void         (*module_init)(void);
  RModuleInfo* (*get_info)(void);
  gboolean     (*open_file)(GtkWidget *parent, const gchar *filename, GSList **buffer);
  void         (*set_properties)(GHashTable *props);
  void         (*get_properties)(GHashTable **props);
  gboolean     (*configure)(GtkDialog *dialog, gpointer *user_data);
  gboolean     (*configure_get_data)(GtkDialog *dialog, gpointer user_data);
  GtkWindow*   (*show_about)(GtkWindow *parent_window);
  guint        (*get_interface_version)(void);
};

struct _ReaderPluginClass
{
  GObjectClass parent_class;
};

ReaderPlugin*   reader_plugin_new       (GModule *module);
GtkType         reader_plugin_get_type  (void);
void            reader_plugin_free      (ReaderPlugin *plugin);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* PLUGINS_H */

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
