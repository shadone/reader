#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif
#include <gmodule.h>
#include <gtk/gtk.h>
#include <libatreader/gtkreaderarea.h>
#include <libatreader/gtkreaderparagraph.h>
#include <libatreader/gtkreaderchunk.h>
#include <libatreader/gtkreaderattr.h>
#include "defines.h"
#include "utils.h"
#include "ui.h"
#include "plugins.h"
#include "configuration.h"

GSList *plugins;
RConfig *configuration;
MainOptions *options;

GtkReaderBuffer* create_default_reader_buffer()
{
  GtkReaderBuffer    *buffer;
  GtkReaderParagraph *paragraph;

  buffer = gtk_reader_buffer_new_with_name("intro");

  {
    gsize s;
    GtkReaderChunk *chunk;
    GtkReaderChunkImage image;
    gunichar chunkid;
    GtkReaderAttr *attr;

    const gchar *str = "The PangoLayoutLine structure represents one of the lines resulting from laying out a paragraph via PangoLayout. PangoLayoutLine structures are obtained by calling pango_layout_get_line() and are only valid until the text, attributes, or settings of the parent PangoLayout are modified.";
    gchar *unistr = g_convert(str, -1, "utf-8", "koi8-r", NULL, NULL, NULL);
    
    paragraph = gtk_reader_paragraph_new();
    gtk_reader_paragraph_set_indent(paragraph, GTK_READER_PARAGRAPH_VALUE_PIXELS, 50);
    gtk_reader_paragraph_set_align(paragraph, PANGO_ALIGN_LEFT);
    gtk_reader_paragraph_set_valign(paragraph, GTK_READER_PARAGRAPH_VALIGN_BOTTOM);
    gtk_reader_paragraph_set_justify(paragraph, TRUE);
    gtk_reader_paragraph_append_text(paragraph, unistr);

/*     chunk = gtk_reader_chunk_new(); */
/*     image.width  = 80*PANGO_SCALE; */
/*     image.height = 50*PANGO_SCALE; */
/*     gtk_reader_chunk_set_image_value(chunk, image); */
/*     chunkid = gtk_reader_paragraph_add_chunk(paragraph, chunk); */
/*     paragraph->text = g_string_prepend_unichar(paragraph->text, chunkid); */
/*     chunk->start_index = utils_utf8_position_to_offset(paragraph->text, 0); */
/*     chunk->end_index   = utils_utf8_position_to_offset(paragraph->text, 1); */

    attr = gtk_reader_attr_new("strong");
    gtk_reader_attr_set_size(attr, 50);

    chunk = gtk_reader_chunk_new();
    chunk->start_index = utils_utf8_position_to_offset(paragraph->text, 0);
    chunk->end_index   = utils_utf8_position_to_offset(paragraph->text, 1);
    gtk_reader_chunk_set_text_value(chunk, attr);
    gtk_reader_paragraph_add_chunk(paragraph, chunk);

    gtk_reader_buffer_append_paragraph(buffer, paragraph);
    g_free(unistr);
  }

  {
    paragraph = gtk_reader_paragraph_new();
    gtk_reader_paragraph_set_text(paragraph, "");
    gtk_reader_buffer_append_paragraph(buffer, paragraph);
  }

  {
    GtkReaderChunk *chunk;
    GtkReaderChunkImage image;
    GtkReaderAttr *attr;

    const gchar *str = "В мире Open Source было сделано многое для создания функционально полноценной, гибкой и удобной в использовании системы управления базами данных - СУБД, которая смогла бы выдерживать большие нагрузки и еще иметь интерфейс доступа из популярных языков программирования, таких, как PHP и Perl. За долгое время развития систем хранения данных на этом рынке определились два сильнейших игрока, MySQL и PostgreSQL. Но постепенно в борьбу за право называться лучшей СУБД вступает третий игрок - СУБД Firebird. Сервер предлагает широкий набор функций, имеет отличный послужной список еще в бытность им СУБД InterBase. Как будет рассказано ниже, Firebird имеет внушительные возможности, доступные, как правило, только в коммерческих системах управления базами данных включая хранимые процедуры, триггеры, архивирование базы во время ее работы (hot backup - \"горячая\" архивация), репликацию и многое другое. Как и другие уже сложившиеся продукты, Firebird несет в себе наследство от своих предшественников, что может немного отпугнуть нового пользователя. Так что перед тем, как погрузиться в исследование особенностей, предоставляемых этим сервером баз данных, рассмотрим некоторые общие проблемы, которые могут возникнуть.";
    gchar *unistr = g_convert(str, -1, "utf-8", "koi8-r", NULL, NULL, NULL);
    
    paragraph = gtk_reader_paragraph_new();
    gtk_reader_paragraph_set_indent(paragraph, GTK_READER_PARAGRAPH_VALUE_PIXELS, 50);
    gtk_reader_paragraph_set_align(paragraph, PANGO_ALIGN_LEFT);
    gtk_reader_paragraph_set_valign(paragraph, GTK_READER_PARAGRAPH_VALIGN_BOTTOM);
    gtk_reader_paragraph_set_justify(paragraph, TRUE);
    gtk_reader_paragraph_append_text(paragraph, unistr);

    attr = gtk_reader_attr_new("strong");
    gtk_reader_attr_set_size(attr, 50);
    
    chunk = gtk_reader_chunk_new();
    chunk->start_index = utils_utf8_position_to_offset(paragraph->text, 7);
    chunk->end_index   = utils_utf8_position_to_offset(paragraph->text, 23);
    gtk_reader_chunk_set_text_value(chunk, attr);
    gtk_reader_paragraph_add_chunk(paragraph, chunk);

    gtk_reader_buffer_append_paragraph(buffer, paragraph);
    
    g_free(unistr);
  }
  {
    gsize s;
    GtkReaderChunk *chunk;
    GtkReaderChunkImage image;
    gunichar chunkid;

    const gchar *str = "Although the GTK distribution comes with many types of widgets that should cover most basic needs, there may come a time when you need to create your own new widget type. Since GTK uses widget inheritance extensively, and there is already a widget that is close to what you want, it is often possible to make a useful new widget type in just a few lines of code. But before starting work on a new widget, check around first to make sure that someone has not already written it. This will prevent duplication of effort and keep the number of GTK widgets out there to a minimum, which will help keep both the code and the interface of different applications consistent. As a flip side to this, once you finish your widget, announce it to the world so other people can benefit. The best place to do this is probably the gtk-list.";
    gchar *unistr = g_convert(str, -1, "utf-8", "koi8-r", NULL, NULL, NULL);
    
    paragraph = gtk_reader_paragraph_new();
    gtk_reader_paragraph_set_indent(paragraph, GTK_READER_PARAGRAPH_VALUE_PIXELS, 50);
    gtk_reader_paragraph_set_align(paragraph, PANGO_ALIGN_LEFT);
    gtk_reader_paragraph_set_valign(paragraph, GTK_READER_PARAGRAPH_VALIGN_BOTTOM);
    gtk_reader_paragraph_set_justify(paragraph, TRUE);
    gtk_reader_paragraph_append_text(paragraph, unistr);

    gtk_reader_buffer_append_paragraph(buffer, paragraph);
    g_free(unistr);
  }
  
  return buffer;
}

void plugins_load(void)
{
  gchar *plugin_path, *fname;
  GModule *module;
  ReaderPlugin *plugin;
  GDir *dir;
  
  g_return_if_fail(g_module_supported() == TRUE);

  dir = g_dir_open(INPUT_PLUGIN_DIR, 0, NULL);
  /* TODO: handle dir open errors */
  g_assert(dir != NULL);
  while ( (fname = g_dir_read_name(dir)) != NULL)
  {
    gchar *ext;

    ext = utils_get_file_extension(fname);
    if (ext && !g_utf8_collate(ext, PLUGIN_SUFFIX))
    {
      guint version;
      
      plugin_path = g_build_filename(INPUT_PLUGIN_DIR, fname, NULL);
      g_assert(plugin_path != NULL);

      module = g_module_open(plugin_path, 0);
      if (module == NULL)
      {
        g_warning(g_module_error());
        continue;
      }
      
      plugin = reader_plugin_new(module);
      g_return_if_fail(plugin != NULL);

      version = plugin->get_interface_version();
      if (version != PLUGIN_INTERFACE_VERSION)
      {
        g_warning("Unsupported plugin interface version (%.2f) of plugin '%s'\n", version/100.0, plugin_path);
        reader_plugin_free(plugin);
        plugin = NULL;
        continue;
      }
       
      plugins = g_slist_append(plugins, plugin);
      
      // initialize plugin
      plugin->module_init();
      
      // set plugin properties
      plugin->set_properties(r_config_get_properties(configuration, plugin->info->module_name));
      
      g_free(plugin_path);
    }
    g_free(ext);
  }
  g_dir_close(dir);
}

void read_configuration(void)
{
  gchar *value;
  gchar *config_path;
  
  configuration = r_config_new();
  g_assert(configuration != NULL);

  config_path = g_build_filename(g_get_home_dir(), DOTREADER_DIR, "config", NULL);
  g_assert(config_path != NULL);
  r_config_parse(configuration, config_path);
  g_free(config_path);

  /* set default profile */
  r_config_set_profile(configuration, "main");
}

/**
 * Sets widget properties to default values, or ones read from config-file.
 */
void set_reader_buffer_properties(void)
{
  PangoWrapMode wrap_mode;
  gchar        *value;
  
  gtk_reader_area_set_column_count(
    widgets.reader,
    r_config_get_int_default(configuration, "main", "column_count", 1));
  gtk_reader_area_set_column_spacing(
    widgets.reader,
    r_config_get_int_default(configuration, "main", "column_spacing", 2));
  gtk_reader_area_set_default_font_from_string(
    widgets.reader,
    r_config_get_string_default(configuration, "main", "font_description", "Arial 12"));
  gtk_reader_area_set_spacing(
    widgets.reader,
    r_config_get_int_default(configuration, "main", "spacing", 2));
  gtk_reader_area_set_display_partial(
    widgets.reader,
    r_config_get_bool_default(configuration, "main", "display_partial", TRUE));
  gtk_reader_area_set_empty_line_height(
    widgets.reader,
    r_config_get_int_default(configuration, "main", "empty-line-height", 10));

  value = r_config_get_string_default(configuration, "main", "wrap_mode", "word");
  if (!g_ascii_strcasecmp(value, "word"))
    wrap_mode = PANGO_WRAP_WORD;
  else if (!g_ascii_strcasecmp(value, "char"))
    wrap_mode = PANGO_WRAP_CHAR;
#ifndef PANGO_1_0
  else if (!g_ascii_strcasecmp(value, "wordchar"))
    wrap_mode = PANGO_WRAP_WORD_CHAR;
#endif
  else
  {
    g_warning("Unknown value in configuration 'wrap_mode': %s", value);
    wrap_mode = PANGO_WRAP_WORD;
  }
  gtk_reader_area_set_wrap(
    widgets.reader,
    wrap_mode);

  value = r_config_get_string_default(configuration, "main", "gtkrc", "");
  g_assert(value != NULL);
  gtk_rc_parse_string(value);
}

MainOptions* r_options_new(void)
{
  MainOptions *opt;

  opt = g_new0(MainOptions, 1);

  return opt;
}

int main(int argc, char **argv)
{
  GtkReaderBuffer *buffer;
  gchar *path;

  options = r_options_new();
  
  /* add application-specified default rc-file */
  path = g_build_filename(g_get_home_dir(), DOTREADER_DIR, "gtkrc", NULL);
  gtk_rc_add_default_file(path);
  g_free(path);
  
  gtk_set_locale();
  gtk_init(&argc, &argv);

  if (argc > 2)
  {
    printf("Too many arguments.\n");
    printf("Usage: /path/to/reader <optional_path_to_file_name>\n");
    return 1;
  }
  
  ui_create_main_window();

  utils_make_reader_dir();
  
  read_configuration();
  plugins_load();

  /* set widget properties. Default values, or ones read from config-file */
  set_reader_buffer_properties();

  if (argc > 1)
  {
    if (ui_open_file(argv[1]) == FALSE)
    {
      /* show error message */
      GtkWidget *dialog;
      dialog = gtk_message_dialog_new(GTK_WINDOW(widgets.main_window),
                                      GTK_DIALOG_DESTROY_WITH_PARENT,
                                      GTK_MESSAGE_ERROR,
                                      GTK_BUTTONS_OK,
                                      "Cannot open file '%s'", argv[1]);
      g_signal_connect_swapped(G_OBJECT(dialog), "response",
                               G_CALLBACK(gtk_widget_destroy),
                               GTK_OBJECT(dialog));
      gtk_widget_show(dialog);
    }
  }
  if (options->current_file == NULL)
  {
    buffer = create_default_reader_buffer();
    gtk_reader_area_add_buffer(widgets.reader, buffer);
    gtk_reader_area_set_current_buffer_by_name(widgets.reader, "intro");
  }
  
  gtk_widget_queue_draw(GTK_WIDGET(widgets.reader));

  gtk_main();

  return 0;
}

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
