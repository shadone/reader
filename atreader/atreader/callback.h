#ifndef CALLBACK_H
#define CALLBACK_H

#include <gtk/gtk.h>
#include <glib.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

  
void callback_menubar_open(gpointer   callback_data,
                           guint      callback_action,
                           GtkWidget *widget);
void callback_menubar_save_prefs(gpointer   callback_data,
                                 guint      callback_action,
                                 GtkWidget *widget);
gboolean callback_reader_button_press(GtkWidget *widget,
                                      GdkEventButton *event,
                                      gpointer user_data);
gboolean callback_reader_key_press(GtkWidget *widget,
                                   GdkEventKey *event,
                                   gpointer user_data);
void callback_reader_popup_options(gpointer   callback_data,
                                   guint      callback_action,
                                   GtkWidget *widget);
void callback_help_plugins(gpointer   callback_data,
                           guint      callback_action,
                           GtkWidget *widget);
void callback_help_about(gpointer   callback_data,
                         guint      callback_action,
                         GtkWidget *widget);
void callback_show_plugin_preferences(GtkWidget *widget,
                                      gpointer user_data);
void callback_plugin_configure_ok_button(GtkWidget *widget,
                                         gpointer   user_data);
void callback_show_plugin_info_dialog(GtkWidget *widget,
                                      gpointer   user_data);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CALLBACK_H */

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
