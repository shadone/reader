#include "rconfigvalue.h"

/* Forward declarations */

static void r_config_value_class_init         (RConfigValueClass *klass);
static void r_config_value_init               (RConfigValue *plugin);
gchar*      r_config_value_val_to_string      (guint    type,
                                               gpointer v);
void        r_config_value_free_value         (RConfigValue *cv);


/* Implementation */

GType r_config_value_get_type()
{
  static GType config_value_type = 0;
  
  if (!config_value_type)
  {
    static const GTypeInfo config_value_info =
      {
        sizeof (RConfigValueClass),
        NULL,
        NULL,
        (GClassInitFunc) r_config_value_class_init,
        NULL,
        NULL,
        sizeof (RConfigValue),
        0,
        (GInstanceInitFunc) r_config_value_init,
      };
    
    config_value_type = g_type_register_static(G_TYPE_OBJECT, "RConfigValue", &config_value_info, 0);
  }
  
  return config_value_type;
}

static void r_config_value_class_init(RConfigValueClass *klass)
{
}

static void r_config_value_init(RConfigValue *cv)
{
  cv->type  = CVT_UNDEF;
  cv->value = NULL;
}

RConfigValue* r_config_value_new(void)
{
  RConfigValue *cv;

  cv = g_object_new(r_config_value_get_type(), NULL);
  
  return cv;
}

RConfigValue* r_config_value_new_with_value(guint type,
                                            gpointer value)
{
  RConfigValue *cv;

  cv = r_config_value_new();
  g_assert(cv != NULL);

  r_config_value_set_value(cv, type, value);
  
  return cv;
}

void r_config_value_free(RConfigValue *cv)
{
  //TODO:
}

/**
 * Creates copy of existing RConfigValue. Stored value also copied,
 * i.e. original #RConfigValue may be freed.
 * @param cv a #RConfigValue.
 * @return a newly allocated #RConfigValue object.
 */
RConfigValue* r_config_value_copy(RConfigValue *cv)
{
  g_return_if_fail(cv != NULL);
  g_return_if_fail(R_IS_CONFIG_VALUE(cv));
  
  return r_config_value_new_with_value(cv->type, cv->value);
}

/**
 * Sets value with specified type.
 * @param cv a #RConfigValue.
 * @param type a type of stored value.
 * @param value a value to store. This value is copied, so original
 * pointer given to r_config_value_set_value may be freed.
 */
void r_config_value_set_value(RConfigValue *cv,
                              guint         type,
                              gpointer      value)
{
  g_return_if_fail(cv != NULL);
  g_return_if_fail(R_IS_CONFIG_VALUE(cv));
  g_return_if_fail(type != CVT_UNDEF);

  r_config_value_free_value(cv);
  
  cv->type = type;
  if (type & CVT_ARRAY)
  {
    /* array of something */
    type ^= CVT_ARRAY;
    g_assert(type != CVT_UNDEF);
    g_assert(value != NULL /* must not be empty array */);

    if ((type & CVT_INT) == CVT_INT)
    {
      GSList *lst=NULL;
      gint *v = (gint*)value;
      while (v != NULL)
      {
        lst = g_slist_append(lst, GINT_TO_POINTER(*v));
        v++;
      }
      cv->value = lst;
    }
    else if ((type & CVT_BOOL) == CVT_BOOL)
    {
      GSList *lst=NULL;
      gboolean *v = (gboolean*)value;
      while (v != NULL)
      {
        lst = g_slist_append(lst, GINT_TO_POINTER(*v));
        v++;
      }
      cv->value = lst;
    }
    else if ((type & CVT_POINTER) == CVT_POINTER)
    {
      GSList *lst=NULL;
      gpointer *v = (gpointer*)value;
      while (v != NULL)
      {
        lst = g_slist_append(lst, *v);
        v++;
      }
      cv->value = lst;
    }
    else if ((type & CVT_STRING) == CVT_STRING)
    {
      /* NULL-terminated array of strings (gchar**) */
      cv->value = g_strdupv(value);
    }
    else
      g_assert_not_reached();
  }
  else
  {
    /* NOT array */
    if ((type & CVT_STRING) == CVT_STRING)
    {
      cv->value = g_strdup(value); /* make a copy of string */
    }
    else
    {
      cv->value = value;          /* just keep it */
    }
  }
}

/**
 * Return stored value as it internally stored.
 * Without any type checking.
 * @param cv a #RCofigValue.
 * @return stored value.
 */
gpointer r_config_value_get_value(RConfigValue *cv)
{
  g_return_val_if_fail(cv != NULL, NULL);
  g_return_val_if_fail(R_IS_CONFIG_VALUE(cv), NULL);

  g_assert(cv->type != CVT_UNDEF);

  return cv->value;
}

/**
 * Checks value type and return stored value.
 * Returned val should not be freed.
 * @param cv a #RConfigValue.
 * @param type a type of returned val.
 * @return a value as it were gived to #r_config_value_set_value.
 */
gpointer r_config_value_get_value_with_type(RConfigValue *cv,
                                            guint         type)
{
  g_return_val_if_fail(cv != NULL, NULL);
  g_return_val_if_fail(R_IS_CONFIG_VALUE(cv), NULL);

  g_assert(type != CVT_UNDEF);
  g_assert(cv->type != CVT_UNDEF);

  if (type != cv->type)
  {
    g_warning("Type mismatch in RConfigValue!");
    return NULL;
  }

  return r_config_value_get_value(cv);
}

/* r_config_value_get_value_type
 * Return config value type as binary OR of CVT_ constants.
 */
guint r_config_value_get_value_type(RConfigValue *cv)
{
  g_return_val_if_fail(cv != NULL, CVT_UNDEF);
  g_return_val_if_fail(R_IS_CONFIG_VALUE(cv), CVT_UNDEF);

  g_assert(cv->type != CVT_UNDEF);

  return cv->type;
}

/**
 * Return value stored in RConfigValue* formatted as string.
 * Returned val should be freed with g_strfreev().
 * @param cv a #RConfigValue.
 * @return NULL-terminated string array, which should be freed with
 * g_strfreev().
 */
gchar** r_config_value_get_string_value(RConfigValue *cv)
{
  g_return_val_if_fail(cv != NULL, NULL);
  g_return_val_if_fail(R_IS_CONFIG_VALUE(cv), NULL);

  g_assert(cv->type != CVT_UNDEF);

  if (cv->type & CVT_ARRAY)
  {
    /* IS array */
    guint   type;
    GSList *lst;
    gchar **val;
    gint    i;

    type = cv->type;
    type ^= CVT_ARRAY;
    g_assert(type != CVT_UNDEF);

    if ((type & CVT_STRING) == CVT_STRING)
    {
      return g_strdupv((gchar**)cv->value);
    }

    val = g_new(gchar*, g_slist_length((GSList*)cv->value)+1);
    g_assert(val != NULL);
    
    /* iterate through list and convert data to string */
    for (i=0, lst = cv->value; lst != NULL; lst = g_slist_next(lst),i++)
    {
      gpointer v;
      
      g_assert(lst != NULL);    /* lst->data CAN be NULL (for example, array of int with element '0' */
      v = lst->data;

      val[i] = r_config_value_val_to_string(type, v);
    }
    
    /* make array NULL-terminated */
    val[i] = NULL;

    return val;
  }
  else
  {
    /* is NOT array */
    gchar **val;

    val    = g_new(gchar*, 2);
    val[0] = r_config_value_val_to_string(cv->type, cv->value);
    val[1] = NULL;

    return val;
  }
}

/**
 * Returns config value's type as string.
 * @param cv a #RConfigValue.
 * @return type of stored value as string. Returned val should be
 * freed with g_free().
 */
gchar* r_config_value_get_string_type(RConfigValue *cv)
{
  gchar *t=NULL;
  
  g_return_val_if_fail(cv != NULL, NULL);
  g_return_val_if_fail(R_IS_CONFIG_VALUE(cv), NULL);
  
  if (cv->type & CVT_INT)
    t = g_strdup("int");
  else if (cv->type & CVT_BOOL)
    t = g_strdup("bool");
  else if (cv->type & CVT_STRING)
    t = g_strdup("string");
  else if (cv->type & CVT_POINTER)
    t = g_strdup("pointer");
  else
    g_assert(FALSE/*unknown type*/);

  return t;
}

/* internal used function for converting value of specified type to string */
gchar* r_config_value_val_to_string(guint    type,
                                    gpointer v)
{
  gchar *buf=NULL;
  
  g_return_val_if_fail(type != CVT_UNDEF, NULL);

  g_assert(!(type & CVT_ARRAY));

  if ((type & CVT_INT) == CVT_INT)
    buf = g_strdup_printf("%d", (gint)v);
  else if ((type & CVT_BOOL) == CVT_BOOL)
    buf = g_strdup( (gboolean)v ? "true" : "false");
  else if ((type & CVT_STRING) == CVT_STRING)
    buf = g_strdup((gchar*)v);
  else if ((type & CVT_POINTER) == CVT_POINTER)
  {
    g_warning("Pointer to string cast!");
    buf = g_strdup_printf("PTR:%d", (gint)v);
  }
  else
    g_assert_not_reached();

  g_assert(buf != NULL);
  
  return buf;
}

void r_config_value_free_value(RConfigValue *cv)
{
  /* TODO: */
}

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
