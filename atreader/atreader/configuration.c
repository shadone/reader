#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif
#include "rconfigvalue.h"
#include "defines.h"
#include "utils.h"
#include "plugins.h"
#include "configuration.h"


/* Forward declarations */

static void r_config_class_init     (RConfigClass *klass);
static void r_config_init           (RConfig *plugin);
RConfig*    r_config_new            (void);
GtkType     r_config_get_type       (void);
void        r_config_parse          (RConfig      *config,
                                     const gchar  *filename);
void        r_config_dump           (RConfig      *config,
                                     const gchar  *filename);
GHashTable* r_config_get_properties (RConfig      *config,
                                     const gchar  *section_name);
void        r_config_insert_value   (RConfig      *config,
                                     const gchar  *profile,
                                     const gchar  *section_name,
                                     const gchar  *option_name,
                                     RConfigValue *value);
void        r_config_parse_start_element(GMarkupParseContext *context,
                                         const gchar         *element_name,
                                         const gchar        **attribute_names,
                                         const gchar        **attribute_values,
                                         gpointer             user_data,
                                         GError             **error);
void        r_config_parse_end_element  (GMarkupParseContext *context,
                                         const gchar         *element_name,
                                         gpointer             user_data,
                                         GError             **error);
void        r_config_parse_text         (GMarkupParseContext *context,
                                         const gchar         *text,
                                         gsize                text_len,  
                                         gpointer             user_data,
                                         GError             **error);
void        r_config_parse_passthrough  (GMarkupParseContext *context,
                                         const gchar         *passthrough_text,
                                         gsize                text_len,  
                                         gpointer             user_data,
                                         GError             **error);
void        r_config_parse_error        (GMarkupParseContext *context,
                                         GError              *error,
                                         gpointer             user_data);
void        r_config_do_merge_foreach   (gpointer key,
                                         gpointer value,
                                         gpointer user_data);
GSList*     r_config_util_get_profile   (RConfig *config,
                                         const gchar *profile);
GHashTable* r_config_util_get_section   (RConfig *config,
                                         const gchar *profile,
                                         const gchar *section);
void        r_config_util_free_hash_list(GSList *list);
gpointer    r_config_util_get_value_default(RConfig     *config,
                                            const gchar *section_name,
                                            const gchar *option_name,
                                            gpointer     defvalue,
                                            guint        valuetype);
gchar*      utils_get_option_path       (GSList      *pathes,
                                         const gchar *option);

#define PARSE_ERROR(typ, str) \
  {                      \
    gchar *buf;          \
    gint n_line, n_char; \
                         \
    g_markup_parse_context_get_position(ud->context, &n_line, &n_char);   \
    buf = g_strdup_printf("%s at line %d, char %d", str, n_line, n_char); \
    g_set_error(error,   \
                G_MARKUP_ERROR,       \
                G_MARKUP_ERROR_##typ, \
                buf);    \
    g_free(buf);         \
    return;              \
  }

#define PARSE_ERROR_1(typ, str, param) \
  {                      \
    gchar *buf;          \
    gchar *msg;          \
    gint n_line, n_char; \
                         \
    g_markup_parse_context_get_position(ud->context, &n_line, &n_char);   \
    msg = g_strdup_printf(str, param);                                    \
    buf = g_strdup_printf("%s at line %d, char %d", msg, n_line, n_char); \
    g_set_error(error,   \
                G_MARKUP_ERROR,       \
                G_MARKUP_ERROR_##typ, \
                buf);    \
    g_free(buf); g_free(msg);         \
    return;              \
  }

typedef struct _RConfigUserData RConfigUserData;
struct _RConfigUserData
{
  RConfig *config;
  GMarkupParseContext *context;
  gboolean in_reader;
  gboolean in_profile;
  gboolean in_section;
  gboolean in_attr;
  gboolean in_list;
  gboolean in_list_value;

  gchar        *profile_name;
  gchar        *section_name;
  gchar        *attr_name;
  gchar        *attr_type;
  guint         attr_type_cvt;
  gpointer      attr_value;
  guint         attr_value_num;
  
  GSList       *group_path;

  /* write options */
  FILE   *writefd;
  GSList *wrote_profiles;
  GStringChunk *wrote_profiles_strings;
  GSList *wrote_sections;
  GStringChunk *wrote_sections_strings;
  GSList *wrote_options;
  GStringChunk *wrote_options_strings;
  gboolean is_unwritten_sections;
};

/* Implementation */

GType r_config_get_type()
{
  static GType config_type = 0;
  
  if (!config_type)
  {
    static const GTypeInfo config_info =
      {
        sizeof (RConfigClass),
        NULL,
        NULL,
        (GClassInitFunc) r_config_class_init,
        NULL,
        NULL,
        sizeof (RConfig),
        0,
        (GInstanceInitFunc) r_config_init,
      };
    
    config_type = g_type_register_static(G_TYPE_OBJECT, "RConfig", &config_info, 0);
  }
  
  return config_type;
}

static void r_config_class_init(RConfigClass *klass)
{
}

static void r_config_init(RConfig *config)
{
  config->profile = NULL;
  config->hashes  = NULL;
}

/**
 * Create a new #RConfig.
 */
RConfig* r_config_new(void)
{
  RConfig *config;

  config = g_object_new(r_config_get_type(), NULL);

  config->hashes = g_hash_table_new_full(
    g_str_hash, g_str_equal,
    g_free, (GDestroyNotify)r_config_util_free_hash_list);
    
  return config;
}

void r_config_free(RConfig *config)
{
  g_return_if_fail(config != NULL);
  g_return_if_fail(R_IS_CONFIG(config));

  if (config->profile)
  {
    g_free(config->profile);
    config->profile = NULL;
  }
  
  g_hash_table_destroy(config->hashes);
  config->hashes = NULL;
}

void r_config_parse(RConfig     *config,
                    const gchar *filename)
{
  GMarkupParser parser;
  GMarkupParseContext *context;
  GError *err;
  RConfigUserData userdata;
  gchar *contents=NULL;
  gssize len;

  memset(&userdata, 0, sizeof(RConfigUserData));

  parser.start_element = r_config_parse_start_element;
  parser.end_element   = r_config_parse_end_element;
  parser.text          = r_config_parse_text;
  parser.passthrough   = r_config_parse_passthrough;
  parser.error         = r_config_parse_error;

  context = g_markup_parse_context_new(&parser, 0, &userdata, NULL);
  userdata.config  = config;
  userdata.context = context;

  if (g_file_test(filename, G_FILE_TEST_EXISTS))
  {
    if (!g_file_get_contents(filename, &contents, &len, NULL))
    {
      g_warning("Can' read file '%s'\n", filename);
    }
    else
    {
      err = NULL;
      g_markup_parse_context_parse(context, contents, len, &err);
      if (err == NULL)
        g_markup_parse_context_end_parse(context, &err);
      if (err && err->domain == G_MARKUP_ERROR && err->code == G_MARKUP_ERROR_EMPTY)
      {
        utils_initialize_config_file(filename);
      }
      else if (err)
      {
        g_warning("config_parse: %s\n", err->message);
        g_error_free(err);
      }
    }
  }
  
  g_markup_parse_context_free(context);
}

/**
 * Dump configuration to config-file.
 * @param config a #RConfig.
 * @param filename a file name to dump to.
 * @todo dumping options with deep path doesn't work (i.e. body::p,
 * won't be dumped as it should be, using <group>). Fix dumping
 * group'ed options (i.e. options with multiple
 * pathes, ex: body::title::p) or rewrote all dump code to simplify
 * configuration dumping.
 */
void r_config_dump(RConfig     *config,
                   const gchar *filename)
{
  GMarkupParser parser;
  GMarkupParseContext *context;
  GError *err;
  RConfigUserData userdata;
  gchar *contents=NULL;
  gssize len;
  gchar *bakfilename;

  memset(&userdata, 0, sizeof(RConfigUserData));

  parser.start_element = r_config_parse_start_element;
  parser.end_element   = r_config_parse_end_element;
  parser.text          = r_config_parse_text;
  parser.passthrough   = r_config_parse_passthrough;
  parser.error         = r_config_parse_error;

  bakfilename = g_strdup_printf("%s.##", filename);

  context = g_markup_parse_context_new(&parser, 0, &userdata, NULL);
  userdata.config  = config;
  userdata.context = context;
  userdata.writefd = fopen(bakfilename, "w");
  if (userdata.writefd == NULL)
  {
    g_warning("Can't open file for writing, '%s'\n", bakfilename);
  }

  // if config file doesn't exist, create it.
  if (!g_file_test(filename, G_FILE_TEST_EXISTS))
    utils_initialize_config_file(filename);
  
  if (userdata.writefd &&
      (g_file_test(bakfilename, G_FILE_TEST_IS_REGULAR) || !g_file_test(bakfilename, G_FILE_TEST_EXISTS)))
  {
    if (!g_file_get_contents(filename, &contents, &len, NULL))
    {
      g_warning("Can' read file '%s'\n", filename);
    }
    else
    {
      err = NULL;
      g_markup_parse_context_parse(context, contents, len, &err);
      if (err == NULL)
        g_markup_parse_context_end_parse(context, &err);

      if (err && err->domain == G_MARKUP_ERROR && err->code == G_MARKUP_ERROR_EMPTY)
      {
        utils_initialize_config_file(filename);
      }
      else if (err)
      {
        g_warning("config_parse: %s\n", err->message);
        g_error_free(err);
      }
      g_free(contents);
    }
  }

  fclose(userdata.writefd);
  userdata.writefd = NULL;

  g_markup_parse_context_free(context);

  if (!g_file_get_contents(bakfilename, &contents, &len, NULL))
  {
    g_warning("something is wrong - can't read '%s'\n", bakfilename);
  }
  else
  {
    FILE *fd;
    
    g_assert(contents != NULL);
    
    fd = fopen(filename, "w");
    if (fd == NULL)
    {
      g_warning("Can't open file to write '%s'\n", filename);
    }
    if (fputs(contents, fd) == EOF)
      g_warning("Something wrong, can't write to file '%s'\n", filename);
    fclose(fd);
    g_free(contents);
  }

  unlink(bakfilename);
  g_free(bakfilename);

  // if there we unwritten sections, re-write them
  if (userdata.is_unwritten_sections)
  {
    r_config_dump(config, filename);
  }
}

void r_config_do_merge_foreach(gpointer key,
                               gpointer value,
                               gpointer user_data)
{
  RConfigUserData *sect;

  g_assert(key != NULL);
  g_assert(value != NULL);
  g_assert(user_data != NULL);

  sect = (RConfigUserData*)user_data;
  g_assert(sect != NULL);
  
  if (g_utf8_collate((gchar*)key, "__name"))
    r_config_insert_value(sect->config, sect->config->profile,
                          sect->section_name,
                          g_strdup((gchar*)key),
                          r_config_value_copy(R_CONFIG_VALUE(value)));
}

/**
 * Merge section options with specified ones using current profile.
 * @param config a #RConfig.
 * @param section_name a section to merge into.
 * @param table a hash table of options.
 */
void r_config_merge(RConfig     *config,
                    const gchar *section_name,
                    GHashTable  *table)
{
  GHashTable *ht;
  RConfigUserData sect;
  
  g_return_if_fail(config != NULL);
  g_return_if_fail(R_IS_CONFIG(config));

  g_assert(section_name != NULL);
  g_assert(table != NULL);
  
  ht = r_config_get_properties(config, section_name);

  memset(&sect, 0, sizeof(sect));
  sect.config = config;
  sect.section_name = g_strdup(section_name);
  
  g_hash_table_foreach(table, r_config_do_merge_foreach, &sect);
  
  g_free(sect.section_name);
}

/**
 * Gets all configuration properties of specified section using
 * current profile. If section was absent add new empty one.
 * @param config a #RConfig.
 * @param section_name a section name where config values should be gotten.
 * @return a hash table with key=>value structure. Key is a string,
 *  value is a #RConfigValue instance.
 */
GHashTable* r_config_get_properties(RConfig     *config,
                                    const gchar *section_name)
{
  GHashTable *ht=NULL;
  gpointer    val;

  g_return_val_if_fail(config != NULL, NULL);
  g_return_val_if_fail(R_IS_CONFIG(config), NULL);

  g_assert(section_name != NULL);

  ht = r_config_util_get_section(config, config->profile, section_name);

  if (ht == NULL)
  {
    /* there wasn't section with that name, add new one */
    gchar   *key;
    GSList  *value;
    gboolean ret;
    
    ht = g_hash_table_new_full(g_str_hash, g_str_equal, g_free,
                               (GDestroyNotify)r_config_value_free);
    g_assert(ht != NULL);
    g_hash_table_insert(ht, (gpointer)g_strdup("__name"),
                        (gpointer)r_config_value_new_with_value(CVT_STRING, section_name));

    /* first, get pointers to old key (profile name) and value (slist of sections) */
    ret = g_hash_table_lookup_extended(config->hashes, config->profile,
                                       (gpointer*)&key, (gpointer*)&value);
    if (ret)
    {
      /* second, remove old key/value from the list and free old key */
      ret = g_hash_table_steal(config->hashes, config->profile);
      g_assert(ret == TRUE);
      g_free(key);
    }
    else
    {
      key   = NULL;
      value = NULL;
    }

    /* third, add new section to the list */
    value = g_slist_append(value, ht);

    /* fourth, insert new section into profile */
    g_hash_table_insert(config->hashes, g_strdup(config->profile),
                        value);
  }
  
  return ht;
}

/**
 * Inserts new value into configuration database.
 * Current profile is NOT changed.
 * @param config a #RConfig.
 * @param profile a profile name to insert value to.
 * @param section_name a section name to insert value to.
 * @param option_name a path to option or group. If value is NULL,
 * then it is a group path, otherwise - option path.
 * @param value a config value to insert or NULL if it is a group.
 */
void r_config_insert_value(RConfig      *config,
                           const gchar  *profile,
                           const gchar  *section_name,
                           const gchar  *option_name,
                           RConfigValue *value)
{
  GHashTable *ht;
  gchar      *oldprofile;

  g_return_if_fail(config != NULL);
  g_return_if_fail(R_IS_CONFIG(config));

  g_assert(profile != NULL);
  g_assert(section_name != NULL);
  g_assert(option_name != NULL);

  oldprofile = g_strdup(r_config_get_profile(config));
  r_config_set_profile(config, profile);
  
  ht = r_config_get_properties(config, section_name);
  g_assert(ht != NULL);
  
  g_hash_table_insert(ht, (gpointer)g_strdup(option_name), (gpointer)value);

  if (oldprofile)
  {
    r_config_set_profile(config, oldprofile);
    g_free(oldprofile);
  }
}

/**
 * Gets integer option from configuration database from specified
 * section.
 * If there were no such option, add new one with specified default
 * value. Current profile is used.
 * @param config a #RConfig.
 * @param section_name a section where option if stored.
 * @param option_name a name of option to get.
 * @param defvalue a default value to store and return if there were
 * no such option.
 * @return option from configuration.
 */
gint r_config_get_int_default(RConfig     *config,
                              const gchar *section_name,
                              const gchar *option_name,
                              gint         defvalue)
{
  gpointer v;

  v = r_config_util_get_value_default(config, section_name,
                                      option_name, GINT_TO_POINTER(defvalue),
                                      CVT_INT);
  return GPOINTER_TO_INT(v);
}

/**
 * Gets boolean option from configuration database from specified
 * section.
 * If there were no such option, add new one with specified default
 * value. Current profile is used.
 * @param config a #RConfig.
 * @param section_name a section where option if stored.
 * @param option_name a name of option to get.
 * @param defvalue a default value to store and return if there were
 * no such option.
 * @return option from configuration.
 */
gboolean r_config_get_bool_default(RConfig     *config,
                                   const gchar *section_name,
                                   const gchar *option_name,
                                   gboolean     defvalue)
{
  gpointer v;

  v = r_config_util_get_value_default(config, section_name,
                                      option_name, GINT_TO_POINTER(defvalue),
                                      CVT_BOOL);
  return GPOINTER_TO_INT(v);
}

/**
 * Gets string option from configuration database from specified
 * section.
 * If there were no such option, add new one with specified default
 * value. Current profile is used.
 * Returned value shouldn't be freed.
 * @param config a #RConfig.
 * @param section_name a section where option if stored.
 * @param option_name a name of option to get.
 * @param defvalue a default value to store and return if there were
 * no such option.
 * @return option from configuration.
 */
gchar* r_config_get_string_default(RConfig     *config,
                                   const gchar *section_name,
                                   const gchar *option_name,
                                   const gchar *defvalue)
{
  gpointer v;

  v = r_config_util_get_value_default(config, section_name,
                                      option_name, (gpointer)defvalue,
                                      CVT_STRING);
  return v;
}

/**
 * Checks whether option with specified path exists.
 * @param section_name search for option in that section.
 * @param option_name path to option (e.g. body::title::p).
 * @return TRUE if option exists.
 */
gboolean r_config_is_value_exists(RConfig     *config,
                                  const gchar *section_name,
                                  const gchar *option_name)
{
  GHashTable *ht;
  gboolean    ret;
  gpointer    key, val;

  g_return_val_if_fail(config != NULL, FALSE);
  g_return_val_if_fail(R_IS_CONFIG(config), FALSE);
  g_return_val_if_fail(section_name != NULL, FALSE);
  g_return_val_if_fail(option_name, FALSE);

  ht = r_config_get_properties(config, section_name);
  ret = g_hash_table_lookup_extended(ht, option_name, &key, &val);

  return (ret && val != NULL);
}

/**
 * Checks whether specified path is group.
 * @param section_name search for path in that section.
 * @param group_name path to group
 * @return TRUE if group_name is path to group, not to option.
 */
gboolean r_config_is_group_exists(RConfig     *config,
                                  const gchar *section_name,
                                  const gchar *group_name)
{
  GHashTable *ht;
  gboolean    ret;
  gpointer    key, val;

  g_return_val_if_fail(config != NULL, FALSE);
  g_return_val_if_fail(R_IS_CONFIG(config), FALSE);
  g_return_val_if_fail(section_name != NULL, FALSE);
  g_return_val_if_fail(group_name, FALSE);

  ht = r_config_get_properties(config, section_name);
  ret = g_hash_table_lookup_extended(ht, group_name, &key, &val);

  return (ret && val == NULL);
}

/**
 * Selects profile. If profile doesn't exist, a new empty profile is
 * created.
 * @param config a RConfig.
 * @param profile a profile name.
 */
void r_config_set_profile(RConfig     *config,
                          const gchar *profile)
{
  g_return_if_fail(config != NULL);
  g_return_if_fail(R_IS_CONFIG(config));

  g_assert(profile != NULL);
  
  if (config->profile)
  {
    if (!g_utf8_collate(config->profile, profile))
      return;
  
    g_free(config->profile);
  }
  
  config->profile = g_strdup(profile);
}

/**
 * Get currect selected profile.
 * @param config a RConfig.
 * @return profile name.
 */
const gchar* r_config_get_profile(RConfig *config)
{
  g_return_val_if_fail(config != NULL, NULL);
  g_return_val_if_fail(R_IS_CONFIG(config), NULL);

  return config->profile;
}


/* utility functions */


/* Called for open tags <foo bar="baz"> */
void r_config_parse_start_element(GMarkupParseContext *context,
                                  const gchar         *element_name,
                                  const gchar        **attribute_names,
                                  const gchar        **attribute_values,
                                  gpointer             user_data,
                                  GError             **error)
{
  RConfigUserData *ud = (RConfigUserData*)user_data;

  g_assert(ud != NULL);

  if (!g_ascii_strcasecmp(element_name, "Reader"))
  {
    ud->in_reader = TRUE;

    if (attribute_names[0] != NULL)
      PARSE_ERROR_1(UNKNOWN_ATTRIBUTE, "Unexpected attribute '%s'", attribute_names[0]);

    if (ud->writefd)
    {
      gchar *buf;
      buf = g_strdup_printf("<%s>", element_name);
      fputs(buf, ud->writefd);
      g_free(buf);

      ud->wrote_profiles_strings = g_string_chunk_new(2);
      g_assert(ud->wrote_profiles_strings != NULL);
    }
    return;
  }
  
  if (!ud->in_reader)
    PARSE_ERROR(INVALID_CONTENT, "'Reader' root element expected");
  
  if (!g_ascii_strcasecmp(element_name, "profile"))
  {
    if (attribute_names[0] == NULL || g_ascii_strcasecmp(attribute_names[0], "name"))
      PARSE_ERROR(UNKNOWN_ATTRIBUTE, "Expected attribute 'name'");
    
    if (attribute_names[1] != NULL)
      PARSE_ERROR_1(UNKNOWN_ATTRIBUTE, "Unexpected attribute '%s'", attribute_names[1]);

    ud->profile_name = g_strdup(attribute_values[0]);
    ud->in_profile   = TRUE;

    r_config_set_profile(ud->config, ud->profile_name);

    if (ud->writefd)
    {
      gchar *buf;
      buf = g_strdup_printf("<%s %s=\"%s\">", element_name, attribute_names[0], attribute_values[0]);
      fputs(buf, ud->writefd);
      g_free(buf);

      // add current profile name to list of written profiles
      ud->wrote_profiles = g_slist_append(
        ud->wrote_profiles,
        g_string_chunk_insert_const(ud->wrote_profiles_strings, ud->profile_name));
      
      ud->wrote_sections_strings = g_string_chunk_new(2);
      g_assert(ud->wrote_sections_strings != NULL);
    }
  }
  else if (!g_ascii_strcasecmp(element_name, "group"))
  {
    if (attribute_names[0] == NULL || g_ascii_strcasecmp(attribute_names[0], "name"))
      PARSE_ERROR(UNKNOWN_ATTRIBUTE, "Expected attribute 'name'");
    
    if (attribute_names[1] != NULL)
      PARSE_ERROR_1(UNKNOWN_ATTRIBUTE, "Unexpected attribute '%s'", attribute_names[1]);

    ud->group_path = g_slist_append(ud->group_path, (gpointer)g_strdup(attribute_values[0]));

    if (ud->writefd)
    {
      gchar *buf;
      buf = g_strdup_printf("<%s %s=\"%s\">", element_name, attribute_names[0], attribute_values[0]);
      fputs(buf, ud->writefd);
      g_free(buf);
    }
  }
  else if (!g_ascii_strcasecmp(element_name, "section"))
  {
    if (attribute_names[0] == NULL || g_ascii_strcasecmp(attribute_names[0], "name"))
      PARSE_ERROR(UNKNOWN_ATTRIBUTE, "Expected attribute 'name'");
    
    if (attribute_names[1] != NULL)
      PARSE_ERROR_1(UNKNOWN_ATTRIBUTE, "Unexpected attribute '%s'", attribute_names[1]);

    ud->section_name = g_strdup(attribute_values[0]);
    ud->in_section = TRUE;

    if (ud->writefd)
    {
      gchar *buf;
      buf = g_strdup_printf("<%s %s=\"%s\">", element_name, attribute_names[0], attribute_values[0]);
      fputs(buf, ud->writefd);
      g_free(buf);


      ud->wrote_options_strings = g_string_chunk_new(2);
      g_assert(ud->wrote_options_strings != NULL);

      // add current section name to list of written sections
      ud->wrote_sections = g_slist_append(
        ud->wrote_sections,
        g_string_chunk_insert_const(ud->wrote_sections_strings, ud->section_name));
    }
  }
  else if (!g_ascii_strcasecmp(element_name, "attr") ||
           !g_ascii_strcasecmp(element_name, "list"))
  {
    if (!g_ascii_strcasecmp(element_name, "attr"))
    {
      if (!ud->in_section)
        PARSE_ERROR(INVALID_CONTENT, "Unexpected 'attr' attribute");
      
      ud->in_attr = TRUE;
    }
    else  /* in 'list' element */
    {
      if (!ud->in_profile || !ud->in_section)
        PARSE_ERROR(INVALID_CONTENT, "Unexpected 'attr' attribute");

      if (ud->in_attr)
        PARSE_ERROR(INVALID_CONTENT, "Syntax error");
    
      ud->in_list = TRUE;
    }

    /* check that only one 'name' and one 'type' attributes */
    if (attribute_names[0] == NULL || attribute_names[1] == NULL || attribute_names[2] != NULL)
      PARSE_ERROR(INVALID_CONTENT, "Need one attribute 'name' and one attribute 'type'");
    
    if ((!(!g_ascii_strcasecmp(attribute_names[0], "name") && !g_ascii_strcasecmp(attribute_names[1], "type")) ||
         (!g_ascii_strcasecmp(attribute_names[1], "name") && !g_ascii_strcasecmp(attribute_names[0], "type"))))
      PARSE_ERROR(UNKNOWN_ATTRIBUTE, "Attributes 'name' and 'type' expected");

    if (!g_ascii_strcasecmp(attribute_names[0], "name"))
    {
      ud->attr_name = g_strdup(attribute_values[0]);
      ud->attr_type = g_strdup(attribute_values[1]);
    }
    else
    {
      ud->attr_name = g_strdup(attribute_values[1]);
      ud->attr_type = g_strdup(attribute_values[0]);
    }

    g_assert(ud->attr_name != NULL);
    g_assert(ud->attr_type != NULL);

    ud->attr_value = NULL;
    ud->attr_value_num = 0;
    
    if (ud->writefd)
    {
      gchar *buf;
      
      buf = g_strdup_printf("<%s %s=\"%s\" %s=\"%s\">", element_name,
                            attribute_names[0],
                            attribute_values[0],
                            attribute_names[1],
                            attribute_values[1]);
      
      fputs(buf, ud->writefd);
      g_free(buf);

      /* add current attribute to list of written options */
      buf = utils_get_option_path(ud->group_path, ud->attr_name);
      ud->wrote_options = g_slist_append(
        ud->wrote_options,
        g_string_chunk_insert_const(ud->wrote_options_strings, buf));
      /* don't need to g_free(buf) because we store 'buf' in StringChunk (?) or should I ? */
    }
  }
  else if (!g_ascii_strcasecmp(element_name, "value"))
  {
    if (!ud->in_section || ud->in_attr || !ud->in_list)
      PARSE_ERROR(INVALID_CONTENT, "Unexpected 'value' attribute");
    
    ud->in_list_value = TRUE;
    ud->attr_value_num++;

    if (attribute_names[0] != NULL)
      PARSE_ERROR_1(INVALID_CONTENT, "Unexpected attribute '%s'", attribute_names[0]);
    
    if (ud->writefd)
    {
      gchar *buf;
      buf = g_strdup_printf("<%s>", element_name);
      fputs(buf, ud->writefd);
      g_free(buf);
    }
  }
  else
    PARSE_ERROR_1(UNKNOWN_ELEMENT, "Unknown element '%s'", element_name);
}

/* Called for close tags </foo> */
void r_config_parse_end_element(GMarkupParseContext *context,
                                const gchar         *element_name,
                                gpointer             user_data,
                                GError             **error)
{
  RConfigUserData *ud = (RConfigUserData*)user_data;

  if (!g_ascii_strcasecmp(element_name, "Reader"))
  {
    ud->in_reader = FALSE;
    
    /* TODO: check if we wrote all available profiles */
  }

  if (!g_ascii_strcasecmp(element_name, "profile"))
  {
    ud->in_profile = FALSE;
    
    if (ud->writefd)
    {
      GSList *it;
      
      // check if we wrote all available sections
      // first, check if we wrote "main" section
      if (g_slist_find_custom(ud->wrote_sections, "main",
                              (GCompareFunc)g_ascii_strcasecmp) == NULL)
      {
        fputs("<section name=\"main\">\n", ud->writefd);
        fputs("</section>\n", ud->writefd);
      }
      // then check all plugins sections
      for (it = plugins; it != NULL; it = g_slist_next(it))
      {
        ReaderPlugin *plugin=NULL;
        
        g_assert(it != NULL && it->data != NULL);

        plugin = READER_PLUGIN(it->data);
        if (g_slist_find_custom(ud->wrote_sections, plugin->info->module_name,
                                (GCompareFunc)g_ascii_strcasecmp) == NULL)
        {
          gchar *buf;

          // mark that there were unwritten sections, so we could re-write section options
          ud->is_unwritten_sections = TRUE;
          
          buf = g_strdup_printf("<section name=\"%s\">\n", plugin->info->module_name);
          fputs(buf, ud->writefd);
          fputs("</section>\n", ud->writefd);
          g_free(buf);
        }
      }
      
      // free memory
      if (ud->wrote_sections)
      {
        g_slist_free(ud->wrote_sections);
        ud->wrote_sections = NULL;
        g_string_chunk_free(ud->wrote_sections_strings);
        ud->wrote_sections_strings = NULL;
      }
    }
    if (ud->profile_name)
      g_free(ud->profile_name);
    ud->profile_name = NULL;
  }
  else if (!g_ascii_strcasecmp(element_name, "group"))
  {
    if (ud->group_path)
    {
      GSList *it;

      /* remove last element from list */
      it = g_slist_last(ud->group_path);
      g_assert(it != NULL && it->data != NULL);
      g_free(it->data);
      ud->group_path = g_slist_delete_link(ud->group_path, it);
    }
  }
  else if (!g_ascii_strcasecmp(element_name, "section")) 
  {
    ud->in_section = FALSE;

    if (ud->writefd)
    {
      // write all other options from option list
      GHashTable *ht;
      GSList *key_list=NULL, *it;
      
      ht = r_config_get_properties(ud->config, ud->section_name);
      g_assert(ht != NULL);
      utils_hash_table_get_keys(ht, &key_list);
      g_assert(key_list != NULL);

      for (it = key_list; it != NULL; it = g_slist_next(it))
      {
        gchar *buffer;
        
        g_assert(it != NULL && it->data != NULL);

        buffer = utils_get_option_path(ud->group_path,
                                       (gchar*)it->data);
        if (g_ascii_strcasecmp((gchar*)it->data, "__name") &&
            g_slist_find_custom(ud->wrote_options, buffer,
                                (GCompareFunc)g_ascii_strcasecmp) == NULL)
        { // if we didn't write this option, write it now.
          gchar *buf;
          GHashTable *ht;
          gpointer val;
          RConfigValue *cv;
          gchar *typestring;
          gchar **valuestring;
          
          ht = r_config_get_properties(ud->config, ud->section_name);
          val = g_hash_table_lookup(ht, (gpointer)it->data);
          g_assert(val != NULL);

          cv = R_CONFIG_VALUE(val);
          typestring = r_config_value_get_string_type(cv);
          g_assert(typestring != NULL);

          valuestring = r_config_value_get_string_value(cv);
          g_assert(valuestring != NULL);
          
          if (r_config_value_get_value_type(cv) & CVT_ARRAY)
          {
            gint i;

            buf = g_strdup_printf("<list name=\"%s\" type=\"%s\">",
                                  (gchar*)it->data,
                                  typestring);
            fputs(buf, ud->writefd);
            g_free(buf);

            i = 0;
            while (valuestring[i] != NULL)
            {
              buf = g_strdup_printf("<value>%s</value>", valuestring[i]);
              fputs(buf, ud->writefd);
              g_free(buf);
              i++;
            }

            fputs("</list>\n", ud->writefd);
          }
          else
          {
            buf = g_strdup_printf("<attr name=\"%s\" type=\"%s\">%s</attr>\n",
                                  (gchar*)it->data,
                                  typestring,
                                  valuestring[0]);
            fputs(buf, ud->writefd);
            g_free(buf);
          }
          g_free(typestring);
          g_strfreev(valuestring);
        }
        g_free(buffer);
      }
    }
    
    // now we can free all allocated strings
    if (ud->wrote_options)
    {
      g_slist_free(ud->wrote_options);
      ud->wrote_options = NULL;
      g_string_chunk_free(ud->wrote_options_strings);
      ud->wrote_options_strings = NULL;
    }
    
    if (ud->section_name)
      g_free(ud->section_name);
    ud->section_name = NULL;
  }
  else if (!g_ascii_strcasecmp(element_name, "attr") ||
           !g_ascii_strcasecmp(element_name, "list"))
  {
    /* save readed config value */
    RConfigValue *cv=NULL;

    if (ud->in_list)
    {
      GSList *lst, *it;
      gpointer *pvals;
      gint i;

      if (ud->attr_value == NULL)
        PARSE_ERROR(INVALID_CONTENT, "Seems like missed 'value' element");
      if (ud->attr_type_cvt == 0)
        PARSE_ERROR(INVALID_CONTENT, "Internal error");

      if (ud->writefd)
      {
        GHashTable *ht;
        gpointer v;
        gchar **strval;
        gchar   *path;
        
        ht = r_config_get_properties(ud->config, ud->section_name);

        path = utils_get_option_path(ud->group_path, ud->attr_name);
        /* make sure there IS option with that path */
        g_assert(r_config_is_value_exists(ud->config, ud->section_name, path) == TRUE);

        v = g_hash_table_lookup(ht, path);
        g_assert(v != NULL);

        g_free(path); path = NULL;

        cv = r_config_value_copy(R_CONFIG_VALUE(v));
        strval = r_config_value_get_string_value(cv);
        g_assert(strval != NULL);

        i = 0;
        while (strval[i] != NULL)
        {
          if (i >= ud->attr_value_num)
          {
            gchar *buf;
            buf = g_strdup_printf("<value>%s</value>", strval[i]);
            fputs(buf, ud->writefd);
            g_free(buf);
          }
          i++;
        }
      }
      else
      {
        lst = (GSList*)ud->attr_value;
      
        i = 0;
        pvals = g_new0(gpointer, g_slist_length(lst)+1);
        for (it = lst; it != NULL; it = g_slist_next(it), i++)
        {
          g_assert(it != NULL && it->data != NULL);
          
          pvals[i] = it->data;
        }
        pvals[i] = NULL;
      
        cv = r_config_value_new_with_value(ud->attr_type_cvt | CVT_ARRAY, pvals);
        g_slist_free(lst);        /* free memory for list elements, RConfigValue copy them */
        if (ud->attr_type_cvt == CVT_STRING)
          g_strfreev((gchar**)pvals);
        else
          g_free(pvals);
        pvals = NULL;
      }
      
      ud->attr_value = NULL;
    }
    else
    {
      if (ud->attr_value == NULL)
      {
        /* this means that xml attribute had no text */
        cv = NULL;
      }
      else
      {
        cv = R_CONFIG_VALUE(ud->attr_value);
      }
    }

    if (cv)
    {
      /* insert option into profile */
      gchar *path;
      path = utils_get_option_path(ud->group_path, ud->attr_name);
      r_config_insert_value(ud->config, ud->profile_name, ud->section_name,
                            path, cv);
      g_free(path);
    }
    
    ud->in_attr = FALSE;
    ud->in_list = FALSE;
    
    if (ud->attr_name)
      g_free(ud->attr_name);
    ud->attr_name = NULL;

    if (ud->attr_type)
      g_free(ud->attr_type);
    ud->attr_type = NULL;
  }
  else if (!g_ascii_strcasecmp(element_name, "value"))
  {
    ud->in_list_value = FALSE;
  }

  if (ud->writefd)
  {
    gchar *buf;
    buf = g_strdup_printf("</%s>", element_name);
    fputs(buf, ud->writefd);
    g_free(buf);
  }
}

/* Called for character data */
/* text is not nul-terminated */
void r_config_parse_text(GMarkupParseContext *context,
                         const gchar         *text,
                         gsize                text_len,  
                         gpointer             user_data,
                         GError             **error)
{
  gint n_line, n_char; 
  RConfigUserData *ud = (RConfigUserData*)user_data;
  gchar *value=NULL;
  RConfigValue *cv=NULL;
  gpointer pval;
  guint    ptype;

  if (ud->writefd)
  {
    if (!ud->in_attr && !ud->in_list_value)
    {
      value = g_strndup(text, text_len);
      fputs(value, ud->writefd);
      g_free(value); 
    }
    else if (ud->in_section && (ud->in_attr || ud->in_list_value))
    {
      GHashTable *ht;
      gpointer v;
      gchar   *path;
      gchar  **strval;
      guint    i;

      ht = r_config_get_properties(ud->config, ud->section_name);

      path = utils_get_option_path(ud->group_path, ud->attr_name);
      v = g_hash_table_lookup(ht, path);
      g_free(path); path = NULL;
      
      g_assert(v != NULL);
      strval = r_config_value_get_string_value(R_CONFIG_VALUE(v));
      g_assert(strval != NULL);

      if (ud->in_list)
      {
        // make sure attr_value_num is correct index
        i=0;
        while (strval[i] != NULL) i++;
        g_assert(ud->attr_value_num <= i);
        
        i = ud->attr_value_num-1;
      }
      else
      {
        i = 0;
      }
      fputs(strval[i], ud->writefd);

      g_strfreev(strval);
    }
  }

  value = g_strndup(text, text_len);
  g_strstrip(value);
  if (!(ud->in_attr || (ud->in_list && ud->in_list_value)) &&
      g_utf8_collate(value, "\n") && value[0] != 0)
    PARSE_ERROR(INVALID_CONTENT, "Unexpected content");
  g_free(value);
  
  if (!(ud->in_attr || (ud->in_list && ud->in_list_value)))
    return;

  // we are reading configuration values
  
  value = g_strndup(text, text_len);

  if (text_len == 0)
  {
    /* option with type 'string' can be empty */
    if (g_ascii_strcasecmp(ud->attr_type, "string"))
      PARSE_ERROR(INVALID_CONTENT, "Content required");
  }

  if (!g_ascii_strcasecmp(ud->attr_type, "int"))
  { /* check that value match its type */
    gchar *endptr=NULL;
    gint   v;
    
    v = strtol(value, &endptr, 10);
    if (endptr != NULL && *endptr != '\0')
      PARSE_ERROR(INVALID_CONTENT, "Content of int field is incorrect");

    pval = GINT_TO_POINTER(v);
    ptype = CVT_INT;
  }
  else if (!g_ascii_strcasecmp(ud->attr_type, "bool"))
  {
    gint v;
    
    if (g_ascii_strcasecmp(value, "true") && g_ascii_strcasecmp(value, "false"))
      PARSE_ERROR(INVALID_CONTENT, "Content of bool field is incorrect");

    v = !g_ascii_strcasecmp(value, "true") ? 1 : 0;

    pval = GINT_TO_POINTER(v);
    ptype = CVT_BOOL;
  }
  else if (!g_ascii_strcasecmp(ud->attr_type, "string"))
  {
    pval = (gpointer)g_strdup(value);
    ptype = CVT_STRING;
  }
  else
    PARSE_ERROR_1(INVALID_CONTENT, "Unknown attr type '%s'", ud->attr_type);

  if (ud->in_list)
  {
    ud->attr_value    = (gpointer)g_slist_append((GSList*)ud->attr_value, pval);
    ud->attr_type_cvt = ptype;
  }
  else
  {
    ud->attr_value = (gpointer)r_config_value_new_with_value(ptype, pval);
    g_assert(ud->attr_value != NULL);
  }

  g_free(value);
}

/* Called for strings that should be re-saved verbatim in this same
 * position, but are not otherwise interpretable.  At the moment
 * this includes comments and processing instructions.
 */
/* text is not nul-terminated. */
void r_config_parse_passthrough(GMarkupParseContext *context,
                                const gchar         *passthrough_text,
                                gsize                text_len,  
                                gpointer             user_data,
                                GError             **error)
{
  RConfigUserData *ud = (RConfigUserData*)user_data;

  if (ud->writefd)
  {
    gchar *buf;
    buf = g_strndup(passthrough_text, text_len);
    fputs(buf, ud->writefd); 
    g_free(buf);
  }
}

/* Called on error, including one set by other
 * methods in the vtable. The GError should not be freed.
 */
void r_config_parse_error(GMarkupParseContext *context,
                          GError              *error,
                          gpointer             user_data)
{
}


GSList* r_config_util_get_profile(RConfig     *config,
                                  const gchar *profile)
{
  g_return_val_if_fail(config != NULL, NULL);
  g_return_val_if_fail(R_IS_CONFIG(config), NULL);

  g_assert(profile != NULL);
  
  return (GSList*)g_hash_table_lookup(config->hashes, profile);
}

GHashTable* r_config_util_get_section(RConfig     *config,
                                      const gchar *profile,
                                      const gchar *section)
{
  GSList *sections;
  
  g_return_val_if_fail(config != NULL, NULL);
  g_return_val_if_fail(R_IS_CONFIG(config), NULL);

  g_assert(profile != NULL);
  g_assert(section != NULL);

  sections = r_config_util_get_profile(config, profile);
  for (; sections != NULL; sections = g_slist_next(sections))
  {
    GHashTable *ht;
    gpointer    val;
    
    g_assert(sections != NULL && sections->data != NULL);

    ht = (GHashTable*)sections->data;
    val = g_hash_table_lookup(ht, "__name");
    g_assert(val != NULL);

    if (!g_utf8_collate(
          r_config_value_get_value_with_type(R_CONFIG_VALUE(val), CVT_STRING),
          section))
    {
      return ht;
    }
  }
  
  return NULL;
}

void r_config_util_free_hash_list(GSList *list)
{
  GSList *it;
  
  for (it = list; it != NULL; it = g_slist_next(it))
  {
    g_assert(it != NULL && it->data != NULL);
    g_hash_table_destroy((GHashTable*)it->data);
  }
  
  g_slist_free(list);
}

/**
 * Gets value of specified option. If there we no such option, creates
 * a new one with specified default value.
 * @config a #RConfig.
 * @section_name a name of section in config file.
 * @option_name a name (or path) of option to get.
 * @defvalue a default value returned if there we no option with
 * specified path.
 * @valuetype a type of option to get (#RConfigValue type).
 * @return value from configuration or NULL if error occured.
 * @see RConfigValue.
 */
gpointer r_config_util_get_value_default(RConfig     *config,
                                         const gchar *section_name,
                                         const gchar *option_name,
                                         gpointer     defvalue,
                                         guint        valuetype)
{
  GHashTable *ht;
  gboolean    ret;
  gpointer    key, val;

  g_return_val_if_fail(config != NULL, NULL);
  g_return_val_if_fail(R_IS_CONFIG(config), NULL);
  g_return_val_if_fail(section_name != NULL, NULL);
  g_return_val_if_fail(option_name != NULL, NULL);
  g_return_val_if_fail(defvalue != NULL, NULL);
                       
  ht = r_config_get_properties(config, section_name);
  ret = g_hash_table_lookup_extended(ht, (gpointer)option_name, &key, &val);
  if (!ret)
  {
    g_hash_table_insert(ht, (gpointer)g_strdup(option_name),
                        r_config_value_new_with_value(valuetype, defvalue));
    val = g_hash_table_lookup(ht, (gpointer)option_name);
    g_assert(val != NULL);
  }
  else if (val == NULL)
  {
    gchar *buf;
    g_warning("Config value (%s::%s::%s) is a group, not option!",
              config->profile, section_name, option_name);
    return defvalue;
  }

  return r_config_value_get_value_with_type(R_CONFIG_VALUE(val), valuetype);
}

void utils_join_list_func(gpointer data,
                          gpointer user_data)
{
  const gchar *str = (const gchar*)data;
  gchar **path = (gchar**)user_data;

  g_assert(path != NULL);
  
  if (*path == NULL)
  {
    *path = g_strdup(str);
  }
  else
  {
    gchar *buf;
    
    buf = g_strdup_printf("%s::%s", *path, str);
    g_free(*path);
    *path = buf;
  }
}

gchar* utils_get_option_path(GSList      *pathes,
                             const gchar *option)
{
  gchar *result=NULL;
  gchar *path=NULL;

  if (pathes)
    g_print("%s\n", pathes->data);
  if (pathes)
    g_slist_foreach(pathes, utils_join_list_func, &path);

  if (path)
    result = g_strdup_printf("%s::%s", path, option);
  else
    result = g_strdup(option); 

  return result;
}


/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
