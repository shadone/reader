#ifndef RCONFIGVALUE_H
#define RCONFIGVALUE_H

#include <gtk/gtk.h>
#include <glib.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define R_CONFIG_VALUE(obj)          GTK_CHECK_CAST(obj, r_config_value_get_type(), RConfigValue)
#define R_CONFIG_VALUE_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, r_config_value_get_type(), RConfigValueClass)
#define R_IS_CONFIG_VALUE(obj)       GTK_CHECK_TYPE(obj, r_config_value_get_type())
  
typedef struct _RConfigValue      RConfigValue;
typedef struct _RConfigValueClass RConfigValueClass;

#define CVT_UNDEF    0x0
#define CVT_INT      0x1
#define CVT_BOOL     0x2
#define CVT_STRING   0x4
#define CVT_POINTER  0x8
#define CVT_ARRAY    0x80       /* NULL-terminated array */

struct _RConfigValue
{
  GObject parent_instance;

  guint        type;            /* one of CVT_ defines */
  gpointer     value;
};

struct _RConfigValueClass
{
  GObjectClass parent_class;
};


RConfigValue* r_config_value_new                (void);
RConfigValue* r_config_value_new_with_value     (guint type,
                                                 gpointer value);
GtkType       r_config_value_get_type           (void);
void          r_config_value_free               (RConfigValue *cv);
RConfigValue* r_config_value_copy               (RConfigValue *cv);
void          r_config_value_set_value          (RConfigValue *cv,
                                                 guint         type,
                                                 gpointer      value);
gpointer      r_config_value_get_value          (RConfigValue *cv);
gpointer      r_config_value_get_value_with_type(RConfigValue *cv,
                                                 guint         type);
guint         r_config_value_get_value_type     (RConfigValue *cv);
gchar**       r_config_value_get_string_value   (RConfigValue *cv);
gchar*        r_config_value_get_string_type    (RConfigValue *cv);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* RCONFIGVALUE_H */

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
