#ifndef UI_H
#define UI_H

#include <gtk/gtk.h>
#include <gtk/gtkwidget.h>
#include <gdk/gdk.h>
#include <glib.h>
#include <libatreader/gtkreaderarea.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef struct _Widgets Widgets;
struct _Widgets
{
  GtkWidget *main_window;
  GtkWidget *menubar;
  GtkWidget *toolbar;
  GtkWidget *scroller;
  GtkItemFactory *popup_item_factory;
  GtkReaderArea  *reader;
  GtkWidget *plugin_prefs;
  GtkWidget *about_dialog;
  GtkWidget *plugin_about_dialog;
};

extern Widgets widgets;

GtkWidget* ui_create_main_menubar ();
void       ui_create_main_window  (void);
void       ui_create_main_popup_menu_item_factory(void);
gboolean   ui_open_file           (const gchar *filename);
void       ui_show_preferences    (gpointer   callback_data,
                                   guint      callback_action,
                                   GtkWidget *widget);
void       ui_show_plugins_preferences(gpointer   callback_data,
                                       guint      callback_action,
                                       GtkWidget *widget);
void       ui_show_plugin_configuration(gchar *module_name);
void       ui_check_item_set_active(gchar   *item,
                                    gboolean value);
  
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* UI_H */

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
