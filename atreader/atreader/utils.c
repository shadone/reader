#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif
#include <glib.h>
#if defined(HAVE_SYS_STAT_H) && !defined(OS_WIN32)
#  include <sys/stat.h>
#endif
#ifdef HAVE_SYS_TYPES_H
#  include <sys/types.h>
#endif
#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif
#include <stdio.h>
#include "defines.h"

/* Converts position in characters to position in bytes
 */
glong utils_utf8_position_to_offset(GString *utf8string,
                                    guint pos)
{
  guint i;
  gchar *text = utf8string->str;
  
  g_return_val_if_fail(utf8string != NULL, 0);

  g_assert(pos < utf8string->len);
  g_return_val_if_fail(g_utf8_validate(utf8string->str, utf8string->len, NULL) == TRUE,
                       pos);
  
  for (i = 0; i < pos; i++)
    text = g_utf8_next_char(text);
  
  return text-utf8string->str;
}

gchar* utils_get_file_extension(const gchar *filename)
{
  gchar *basename;
  gchar *ext, *extension=NULL;
  
  basename = g_path_get_basename(filename);
  g_return_val_if_fail(basename != NULL, NULL);

  ext = g_strrstr(basename, ".");

  if (ext)
    extension = g_strdup(ext);

  g_free(basename);

  return extension;
}

#if defined(OS_WIN32)
#  undef mode_t
#  define mode_t   int
#  define S_IRGRP  0
#  define S_IXGRP  0
#  define S_IRWXU  0
#  define S_IROTH  0
#  define S_IXOTH  0
#endif

int utils_mkdir(const char *pathname, mode_t mode)
{
#ifdef OS_WIN32
  return mkdir(pathname);
#else
  return mkdir(pathname, mode);
#endif
}

void utils_make_reader_dir(void)
{
  gchar *path;

  path = g_build_filename(g_get_home_dir(), DOTREADER_DIR, NULL);
  utils_mkdir(path, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
  
  path = g_build_filename(g_get_home_dir(), DOTREADER_DIR, "plugins", NULL);
  utils_mkdir(path, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);

#ifndef ONE_PLUGIN_DIR
  path = g_build_filename(g_get_home_dir(), DOTREADER_DIR, "plugins", "Input", NULL);
  mkdir(path, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
#endif /* ONE_PLUGIN_DIR */
}

void utils_hash_table_get_keys_func(gpointer key,
                                    gpointer value,
                                    gpointer user_data)
{
  GSList *list;

  g_return_if_fail(key != NULL && value != NULL);
  g_return_if_fail(user_data != NULL);

  list = *(GSList**)user_data;
  list = g_slist_append(list, key);
  *(GSList**)user_data = list;
}

void utils_hash_table_get_keys(GHashTable *ht,
                               GSList **list)
{
  g_return_if_fail(ht != NULL);
  g_return_if_fail(list != NULL);

  *list = NULL;
  g_hash_table_foreach(ht, utils_hash_table_get_keys_func, list);
}

void utils_initialize_config_file(const gchar *filename)
{
  FILE *fd;

  fd = fopen(filename, "w");
  if (fd == NULL)
  {
    g_warning("Can't write to config file '%s'\n", filename);
    return;
  }

  fputs("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", fd);
  fputs("<Reader>\n", fd);
  fputs(" <profile name=\"main\">\n", fd);
  fputs("  <section name=\"main\">\n", fd);
  fputs("  </section>\n", fd);
  fputs(" </profile>\n", fd);
  fputs("</Reader>\n", fd);

  fclose(fd);
}


/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
