#include "rmoduleinfo.h"

/* Forward declarations */

static void r_module_info_class_init               (RModuleInfoClass *klass);
static void r_module_info_init                     (RModuleInfo      *module_info);

GType r_module_info_get_type()
{
  static GType module_info_type = 0;
  
  if (!module_info_type)
  {
    static const GTypeInfo module_info_info =
      {
        sizeof (RModuleInfoClass),
        NULL,
        NULL,
        (GClassInitFunc)r_module_info_class_init,
        NULL,
        NULL,
        sizeof (RModuleInfo),
        0,
        (GInstanceInitFunc)r_module_info_init,
      };
    
    module_info_type = g_type_register_static(G_TYPE_OBJECT, "RModuleInfo", &module_info_info, 0);
  }
  
  return module_info_type;
}

static void r_module_info_class_init(RModuleInfoClass *klass)
{
}

static void r_module_info_init(RModuleInfo *module_info)
{
  module_info->module_name    = NULL;
  module_info->supported_ext  = NULL;
  module_info->copyright_info = NULL;
}

RModuleInfo* r_module_info_new()
{
  RModuleInfo *module_info;

  module_info = g_object_new(r_module_info_get_type(), NULL);

  return module_info;
}

RModuleInfo* r_module_info_new_with_value(gchar *name, gchar **extensions, gchar *copyright)
{
  RModuleInfo *mi;

  /* not sure if using g_return_val_if_fail(..., NULL) is correct in _new constructors */
  g_assert(name != NULL);
  g_assert(extensions != NULL);
  g_assert(copyright != NULL);
  
  mi = r_module_info_new();
  g_assert(mi != NULL);
  
  mi->module_name    = g_strdup(name);
  mi->supported_ext  = g_strdupv(extensions);
  mi->copyright_info = g_strdup(copyright);

  return mi;
}

void r_module_info_free(RModuleInfo *mi)
{
  g_free(mi->module_name);
  g_strfreev(mi->supported_ext);
  g_free(mi->copyright_info);
}

/*
 * Local Variables: ***
 * mode:c ***
 * coding:koi8-r ***
 * End: ***
 */
